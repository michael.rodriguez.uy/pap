package tests;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import datatypes.DtPrograma;
import excepciones.CategoriaRepetidaException;
import excepciones.CursoDeProgramaRepetido;
import excepciones.ProgramaExisteException;
import excepciones.SinEdicionException;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import logica.Curso;
import logica.Programa;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AltaPrograma {
	Fabrica fabrica = Fabrica.getInstancia();
	IControladorInstProg iCip = fabrica.getIControladorIP();

	@Test
	public void a_altaPrograma ()throws ProgramaExisteException {
		iCip.altaPrograma("P1", "desc", null, null, " ");
		boolean ok = iCip.existePrograma("P1");
		assertTrue(ok);
	}
	
	@Test(expected=ProgramaExisteException.class)
	public void b_altaPrograma ()throws ProgramaExisteException {
		iCip.altaPrograma("P1", "desc", null, null, "foto");
	}
	@Test
	public void c_agregarCurso() throws CursoDeProgramaRepetido{
		iCip.agregarCursoPrograma("P1" ,"C1");
		//List<Curso>listarCursosP(String nomPrograma);
		List<Curso> cursos =iCip.listarCursosP("P1");
		assertTrue(cursos.get(0).getNombre().contentEquals("C1"));
	}
	@Test
	public void d_mostrarPrograma () {
		DtPrograma dtp = iCip.mostrarPrograma("P1");
		iCip.cursosDePrograma();
		dtp.getFechaF();
		dtp.getFechaI();
		assertTrue(dtp.getNombre().contentEquals("P1") && dtp.getDescripcion().contentEquals("desc"));
	}
	@Test
	public void e_listar() {
		String[] progs =iCip.listarProgramasCurso("C1");
		assertTrue(progs[0].contentEquals("P1"));
	}
	
	@Test
	public void f_ver() {
		DtPrograma p = iCip.verPrograma("P1");
	}
	
	@Test
	public void g_listarProgramas()  {
		//      String[] listarProgramas()   mostrarEdicion
		String[] progs = iCip.listarProgramas();
		assertTrue(progs[0].contentEquals("P1"));		
	}
	
	@Test
	public void h_programasdeCurso() {
		// public DtPrograma[] programasdeCurso(String nomCurso)
		
		DtPrograma[] progs= iCip.programasdeCurso("C1");
		assertTrue(progs[0].getNombre().contentEquals("P1"));
	}
	
	@Test(expected=CursoDeProgramaRepetido.class)
	public void i_agregarCurso() throws CursoDeProgramaRepetido{
		iCip.agregarCursoPrograma("P1" ,"C1");		
	}
	
	
}
