package tests;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import datatypes.DtCurso;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MostrarCurso {
	Fabrica fabrica = Fabrica.getInstancia();
	IControladorInstProg iCip = fabrica.getIControladorIP();
	@Test
	public void a_mostrarCurso() {
		iCip.listarNombreCursos("UTU");
		DtCurso dtc = iCip.mostrarDatosdeCurso("C2");
		boolean nomb = dtc.getNombre().equals("C2");
		boolean des= dtc.getDescripcion().contentEquals("desc");
		boolean dur=dtc.getDuracion().contentEquals("3 dias");
		boolean hs = dtc.getCantidadHoras()==12;
		boolean cred=dtc.getCantidadCreditos()==22;
		boolean url= dtc.getUrl().contentEquals("www");
		Date d= dtc.getFechaAlta();
		boolean fot = dtc.getFoto().contentEquals("foto");
		boolean ok=nomb&&des&&dur&&hs&&cred&&url&&fot;
		iCip.cancelarConsultaCurso();
		assertTrue(ok);
	}

}
