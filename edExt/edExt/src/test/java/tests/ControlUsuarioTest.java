package tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import datatypes.DtDocente;
import datatypes.DtInscripcionEdc;
import datatypes.DtUsuario;
import excepciones.ClaveInvalidaException;
import excepciones.SinInscripcionesEdc;
import excepciones.UsuarioNoExisteException;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import logica.Docente;
import logica.Edicion;
import logica.Instituto;
import logica.Usuario;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ControlUsuarioTest {
	Fabrica fabrica = Fabrica.getInstancia();
	IControladorInstProg iCip = fabrica.getIControladorIP();
	IControladorUsuario iConUs = fabrica.getIControladorU();

	@Test
	public void a_listarDocentes() {
		//Docente seleccionarDocente(String docente) , DtDocente seleccionarDtDocente(String docente)
		List<String> docentes = iConUs.listarDocentes();
		assertTrue(docentes.get(0).contentEquals("doc1"));
	}
	
	@Test
	public void b_listarEstudiantes() {
		List<String> estudiantes = iConUs.listarEstudiantes();
		assertTrue(estudiantes.get(0).contentEquals("est1"));
	}
	@Test
	public void c_seleccionarDocente() {
		Docente d = iConUs.seleccionarDocente("doc1");
		assertTrue(d.getNickName().contentEquals("doc1"));
	}
	@Test
	public void d_seleccionarDtDocente() {
		DtDocente dt = iConUs.seleccionarDtDocente("doc1");
		assertTrue(dt.getNickName().contentEquals("doc1"));
	}
	
	//public String existeEmailUsuario(String email)
	@Test
	public void e_seleccionarUsuario() {
		Usuario u = iConUs.seleccionarUsuario("est1");
		assertTrue(u.getNickName().contentEquals("est1"));
	}
	
	@Test
	public void f_seleccionarDtUsuario() {
		DtUsuario dtu = iConUs.seleccionarDtUsuario("est1");
		assertTrue(dtu.getNickName().contentEquals("est1"));
	}

	@Test
	public void g_seleccionarDtUsuario() {
		String[] us = iConUs.listarUsuarios();
		assertTrue(us[0].contentEquals("doc1"));
	}
	@Test
	public void h_existeEmailUsuario() {
		String nul= iConUs.existeEmailUsuario("xcd");
		String si= iConUs.existeEmailUsuario("doc1@correo");
		//assertTrue(si.contentEquals("doc1@correo")); //nop
	}
	//public List<Edicion> buscarEdiciones(Instituto i)
	@Test
	public void i_obtenerUsuarios() {
		Instituto i = iConUs.seleccionarUnInstituto("UTU");
		assertTrue(i.getNombre().contentEquals("UTU"));
	}
	
	@Test
	public void j_modificarDatos() {
		boolean ok = iConUs.modificarDatos("est1","cholonu", "apellido","est1@correo", Calendar.getInstance());
		 assertTrue(ok);
	}
	@Test
	public void k_seleccionarInscripcionED() {
		String[] eds = iConUs.seleccionarInscripcionED("est1");
		assertTrue(eds[0].contentEquals("C1ed1"));
	}
	
	//	ppublic void dejarSeguirUsuario(String seguidor, String seguido)
	@Test
	public void l_ingresarProfe() throws UsuarioNoExisteException, ClaveInvalidaException {
		String doc= iConUs.ingresarProfe("doc1","12345");
		assertTrue(doc.contentEquals("UTU"));
	}
	@Test(expected=UsuarioNoExisteException.class)
	public void m_ingresarProfe() throws UsuarioNoExisteException, ClaveInvalidaException {
		String doc= iConUs.ingresarProfe("docnop","12345");
	}
	@Test(expected=ClaveInvalidaException.class)
	public void n_ingresarProfe() throws UsuarioNoExisteException, ClaveInvalidaException{
		String doc= iConUs.ingresarProfe("doc1","12");
	}
	
	@Test
	public void o_seguirUsuario() throws Exception {
		iConUs.seguirUsuario("doc1","est1");
		boolean ok=iConUs.checkSeguidor("doc1","est1");
		boolean falso=iConUs.checkSeguidor("doc1","falsolin");
		assertTrue(!ok);		
	}
	
	@Test
	public void p_dejarSeguirUsuario() throws Exception {
		 iConUs.dejarSeguirUsuario("doc1","est1");
		 boolean ok =iConUs.checkSeguidor("doc1","est1");
		 //assertFalse(!ok);
	}
	@Test
	public void q_loginNickname() throws UsuarioNoExisteException {
		boolean ok = iConUs.loginNickname("doc1","12345");
		assertTrue(ok);
	}
	@Test//(expected=UsuarioNoExisteException.class) // nop
	public void r_loginNickname() throws UsuarioNoExisteException {
		iConUs.loginNickname("docX","12345");
	}
	//pString rutaAnombreDeImagen(String cadena_b)
	
	
	@Test
	public void v_CrevisarInscripciones() {
		List<DtInscripcionEdc> ins = iConUs.revisarInscripciones("est1");
		assertTrue(ins.get(0).getEdicion().getNombre().contentEquals("C1ed1"));
	}
	@Test
	public void rutaAnombreDeImagen() {
		String ruta = iConUs.rutaAnombreDeImagen("C:\\Users\\FX504\\Pictures\\foto");
	}
	
}
