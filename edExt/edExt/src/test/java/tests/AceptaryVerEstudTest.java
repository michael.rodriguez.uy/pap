package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import datatypes.DtEdicion;
import datatypes.DtInscripcionEdc;
import datatypes.DtUsuario;
import datatypes.EstadoInscripcionEdc;
import excepciones.SinEdicionException;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AceptaryVerEstudTest {
	Fabrica fabrica = Fabrica.getInstancia();
	IControladorInstProg iCip = fabrica.getIControladorIP();
	IControladorUsuario iConUs = fabrica.getIControladorU();
	@Test
	public void a_traerUltimaedicion() throws SinEdicionException {
		iCip.seleccionarInstituto("UTU");
		iCip.listarEdicionesdeCurso("C1");
		List<String> ediciones= iCip.listarEdiciones("C1");
		iCip.traerUltimaEdicion("C1","UTU");
		assertTrue(ediciones.get(0).contentEquals("C1ed1"));
		
	}
	@Test(expected=SinEdicionException.class)
	public void b_traerUltimaedicion() throws SinEdicionException{
		iCip.traerUltimaEdicion("C2","UTU");
	}
	@Test
	public void c_listarInscrip() {
		iCip.obtenerInscripciones("C1","UTU","C1ed1",true);
		DtInscripcionEdc[] inscripciones=iCip.obtenerInscripciones("C1","UTU","C1ed1",false);
		assertTrue(inscripciones[0].getEstudiante().getNickName().contentEquals("est1"));
	}
	
	@Test
	public void d_Aceptar() {
		ArrayList<DtInscripcionEdc> listaAceptados= new ArrayList<DtInscripcionEdc>();
		DtUsuario usr = new DtUsuario( "est1","", "", "", null,false, null);
		DtEdicion edc= new DtEdicion("C1ed1", null, null,0,null,null,"");
		DtInscripcionEdc dtedc = new DtInscripcionEdc(edc,usr, new Date(), EstadoInscripcionEdc.Aceptado);
		listaAceptados.add(dtedc);
		iCip.setearInscripciones("UTU","C1","C1ed1", listaAceptados);
		ArrayList <DtInscripcionEdc> acept =iCip.inscrpcionesAceptadas("UTU","C1","C1ed1");
		assertTrue(acept.get(0).getEstado()==EstadoInscripcionEdc.Aceptado);
	}
	
}
