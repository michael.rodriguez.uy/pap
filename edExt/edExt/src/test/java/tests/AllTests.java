package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import logica.InscripcionEdc;

@RunWith(Suite.class)
@SuiteClasses({ InstitutoTest.class,CategoriasTest.class , AltaDocenteTest.class, 
	CursosTest.class,AltaPrograma.class,AltaEdicion.class, MostrarCurso.class, EstudianteTest.class, InscripcionEdcTest.class,
	AceptaryVerEstudTest.class , ControlUsuarioTest.class, RecycleTest.class})
public class AllTests {

}
