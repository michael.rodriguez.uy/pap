package tests;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import interfaces.Fabrica;
import interfaces.IControladorInstProg;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InstitutoTest {
	Fabrica fabrica = Fabrica.getInstancia();
	IControladorInstProg iCip = fabrica.getIControladorIP();
	@Test

	public void a_ingresarNomInst() {
		boolean existe=iCip.ingresarNomInst("UTU");
		System.out.println("C");
		iCip.CancelarAltaInstituto();
		assertFalse(existe);
	}
	@Test
	public void b_ingresarNomInstRepetido() {
		System.out.println("B");
		boolean existe=iCip.ingresarNomInst("UTU");
		assertTrue(existe);
	}
	@Test
	public void c_listarInstitutos() {
		System.out.println("A");
		iCip.listarInstitutosArray();
		String[] institutos=iCip.listarInstitutos();
		
		assertTrue(institutos.length>=0);
	}
}
