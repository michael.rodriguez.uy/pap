package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;

import datatypes.DtCurso;
import datatypes.DtPrograma;
import excepciones.SinEdicionException;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;

public class CursosTest {
	Fabrica fabrica = Fabrica.getInstancia();
	IControladorInstProg iCip = fabrica.getIControladorIP();
	@Test
	public void altaCursoOK() {
		//String[] institutos=iCip.listarInstitutos();
		iCip.seleccionarInstituto("UTU");
		boolean noexiste=iCip.nuevoCurso("C1");
		iCip.setearCurso("C1", "desc", "3 dias", 12, 22, "www", new Date(), "foto");
		iCip.confirmarAltaCurso();
		iCip.cancelarAltaCurso();
		assertTrue(noexiste);
	}
	@Test
	public void b_cursoPrevias() {
		iCip.seleccionarInstituto("UTU");
		assertEquals(iCip.listarPrevias()[0],"C1");
	}
	@Test
	public void c_cursoAltaCatPrev() {
		ArrayList<String> previas=new  ArrayList<>();
		previas.add("C1");
		
		ArrayList<String> categorias=new  ArrayList<>();
		categorias.add("Cat1");
		iCip.seleccionarInstituto("UTU");
		iCip.nuevoCurso("C2");
		iCip.setearCurso("C2", "desc", "3 dias", 12, 22, "www", new Date(), "foto");
		iCip.listarCategorias();
		iCip.seleccionarPrevias(previas);
		iCip.setearCategorias(categorias);
		iCip.confirmarAltaCurso();
		iCip.seleccionarInstituto("UTU");
		boolean nuevo = iCip.nuevoCurso("C2");
		iCip.cancelarAltaCurso();
		assertFalse(nuevo);
		
	}
	@Test
	public void d_cursosDeCategoria() {
		ArrayList<String> curss = iCip.imprimirCursosdeCategoria("Cat1");
	}
	
	@Test
	public void e_seleccionarCurso()  {
		iCip.seleccionarInstituto("UTU");
		DtCurso dc=iCip.seleccionarCurso("C1");
		
		assertTrue(dc.getNombre().contentEquals("C1"));	
	}
	
	@Test
	public void f_mostrarEdicion() {
		//     listarCursos
		String[] curss=iCip.listarCursos();
		assertTrue(curss[0].contentEquals("C1"));
	}
	
	@Test
	public void g_cursosdeCategoria() {
		//     public DtCurso[] cursosdeCategoria(String categoria)
		DtCurso[] cursos = iCip.cursosdeCategoria("Catcualquiera");
		//String[] lista=iCip.listarCategoriasPF("P1");
		//assertTrue(cursos[0].getNombre().contentEquals("C1"));
	}
	
	//public String institutoDeCurso(String nombreCurso)
	@Test
	public void h_institutoDeCurso() {
		String i =iCip.institutoDeCurso("C1");
		assertTrue(i.contentEquals("UTU"));
	}
	

}
