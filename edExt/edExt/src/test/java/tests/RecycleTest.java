package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import logica.Categoria;
import logica.Curso;
import logica.Docente;
import logica.Edicion;
import logica.Estudiante;
import logica.InscripcionPF;
import logica.Instituto;
import logica.ManejadorInstitutos;
import logica.Programa;

public class RecycleTest {

	Fabrica fabrica = Fabrica.getInstancia();
	IControladorInstProg iCip = fabrica.getIControladorIP();
	IControladorUsuario iConUs = fabrica.getIControladorU();

	@Test
	public void a_test() {
		Curso c = new Curso("C2","des", "dur", 12, 22, "www", null, "foto");
		c.getFoto();
		c.getDescripcion();
		c.getDuracion();
		c.getCantidadHoras();
		c.getCantidadCreditos();
		c.getUrl();
		c.getFechaAlta();
		c.setEdiciones(null);
		List<Categoria> categorias = new ArrayList<Categoria>();
		Categoria cat = new Categoria();
		cat.setNombre("CatR");
		categorias.add(cat);
		c.setCategorias(categorias);
		c.getCategoria("CatR");
		c.getCategorias();
		c.existeCategoria("CatR");
		Edicion e1 = new Edicion();
		Edicion e2 = new Edicion();
		
	}
	@Test
	public void b_test() {
		Curso c = new Curso("C3","des", "dur", 12, 22, "www", null, "foto");
		List<Curso> cursos = new ArrayList<Curso>();
		cursos.add(c);
		Programa p = new Programa();
		p.setFoto("foto");
		p.getFoto();
		p.setNombre("Pp");
		p.setDescripcion("des");
		p.getDescripcion();
		p.setFechaF(null);
		p.getFechaF();
		p.setFechaI(null);
		p.getFechaI();
		p.setCursos(cursos);
		p.existePrograma("C3");
		p.getCategorias();
		Instituto i = new Instituto("Fing");
		i.setCursos(cursos);
		
	}
	

	
	@Test
	public void c_test() {
		ManejadorInstitutos mi= ManejadorInstitutos.getInstancia();
		Instituto in = mi.getInstituto("UTU");
		mi.getProgramas();
		mi.existeProgramDeFormacion("P1");
		mi.getNombreCurso(mi.getInstituto("UTU"),"C1");
		mi.agregarCurso("C8", "des", "12", 12, 12, "www", null, null, mi.getInstituto("UTU"));
		
	}
	@Test
	public void d_test() {
		Docente d = new Docente();
		d.setNombreInsituto("UTU");
		Estudiante e = new Estudiante();
		e.setInscripciones(null);
		e.getInscripciones();
		
	}
	
	@Test//public InscripcionPF(Date fechaIns, Programa programa, Estudiante inscripto)
	public void e_test() {
		 InscripcionPF i = new InscripcionPF(null, new Programa(), new Estudiante());
		 i.setFechaIns(null);
		 i.setInscripto(new Estudiante());
		 i.setPrograma(new Programa());
		 i.getFechaIns();
		 i.getInscripto();
		 i.getPrograma();
	}
	
}
