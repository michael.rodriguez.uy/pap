package tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Date;

import org.junit.Test;

import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;

public class AltaDocenteTest {

	Fabrica fabrica = Fabrica.getInstancia();
	IControladorUsuario iConUs = fabrica.getIControladorU();
	@Test
	public void a_altaUsuarioDocente()throws NickNameExistenteException,EmailExistenteException {
		iConUs.altaUsuarioDocente("doc1", "Pepe", "Rodri", "doc1@correo", null, true, "12345", System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "desconocido.jpg" , "UTU");
	}

}
