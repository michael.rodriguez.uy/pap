package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import datatypes.DtCurso;
import excepciones.CategoriaRepetidaException;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import logica.Categoria;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CategoriasTest {
	Fabrica fabrica = Fabrica.getInstancia();
	IControladorInstProg iCip = fabrica.getIControladorIP();
	
	@Test(expected=CategoriaRepetidaException.class)
	public void a_altaCategoria() throws CategoriaRepetidaException {
		iCip.agregarCategoria("Cat1");
		iCip.agregarCategoria("Cat1");

	}
	
	@Test
	public void b_obtenerCategorias() {
		ArrayList<String> categorias = iCip.obtenerCategorias();
		assertEquals(categorias.get(0),"Cat1");
	}
	

}



