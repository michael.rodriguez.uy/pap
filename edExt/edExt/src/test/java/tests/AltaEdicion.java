package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import datatypes.DtEdicion;
import excepciones.SinEdicionException;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import logica.Docente;
import logica.Edicion;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AltaEdicion {
	Fabrica fabrica = Fabrica.getInstancia();
	IControladorInstProg iCip = fabrica.getIControladorIP();
	IControladorUsuario iConUs = fabrica.getIControladorU();
	@Test
	public void a_altaEdicion() {
		iCip.listarNombreCursos("UTU");
		List<Docente> listaDocentes = new ArrayList<Docente>();
		Docente doc = iConUs.seleccionarDocente("doc1");
		listaDocentes.add(doc);
		boolean ok =iCip.altaEdicionCurso("UTU", "C1", "C1ed1", null, null, 12, null, listaDocentes , "foto");
		//iCip.mostrarEdicion("C1ed1");
		assertTrue(ok);
	}
	@Test
	public void b_cancelarAltaEdicion() {
		iCip.listarNombreCursos("UTU");
		boolean ok=iCip.nuevoCurso("C1");
		iCip.cancelarAltaEdicionCurso();
		assertFalse(ok);
	}
	@Test
	public void c_mostrarEdiciones() throws SinEdicionException {
		ArrayList <DtEdicion> edis= iCip.mostrarEdiciones("UTU","C1");
		assertTrue(edis.get(0).getNombre().contentEquals("C1ed1"));
	}
	
	@Test
	public void d_mostrarEdicion() {
		//      String[] listarProgramas()   mostrarEdicion
		iCip.listarNombreCursos("UTU");
		iCip.listarEdicionesdeCurso("C1");
		DtEdicion dte=iCip.mostrarEdicion("C1ed1");
		boolean ok =dte.getNombre().contentEquals("C1ed1");
		iCip.cancelarAltaEdicionCurso();
		assertTrue(ok);
	}
	
	@Test
	public void e_seleccionarEdicion() {
		//public Edicion seleccionarEdicion(String nombreE)
		Edicion e = iCip.seleccionarEdicion("C1ed1");
		assertTrue(e.getNombre().contentEquals("C1ed1"));
	}
	
	@Test
	public void f_mostrarDtEdicionesArray() {
		DtEdicion[] eds= iCip.mostrarDtEdicionesArray("C1");
		assertTrue(eds[0].getNombre().contentEquals("C1ed1"));
	}
}
