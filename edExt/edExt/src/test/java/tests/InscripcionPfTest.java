package tests;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import excepciones.InscripcionEdicionRepetida;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InscripcionPfTest {
	Fabrica fabrica = Fabrica.getInstancia();
	IControladorInstProg iCip = fabrica.getIControladorIP();
	@Test
	public void a_inscripcion() throws InscripcionEdicionRepetida {
		iCip.realizarInscripcionProgramaDeFormacion("P1","est1");
	}

}
