package tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EstudianteTest {
	Fabrica fabrica = Fabrica.getInstancia();
	IControladorUsuario iConUs = fabrica.getIControladorU();
	IControladorInstProg iCip = fabrica.getIControladorIP();
	@Test
	public void a_altaEstudiante() throws NickNameExistenteException, EmailExistenteException {
		iConUs.altaUsuarioEstudiante("est1","Estudi1", "Rodriguez", "est1@correo", null, false, "12345", System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "desconocido.jpg");
	}
	
	@Test(expected=NickNameExistenteException.class)
	public void b_altaEstudiante() throws NickNameExistenteException, EmailExistenteException {
		iConUs.altaUsuarioEstudiante("est1","Estudi1", "Rodriguez", "e@correo", null, false, "12345", System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "desconocido.jpg");
	}
	@Test(expected=EmailExistenteException.class)
	public void c_altaEstudiante() throws NickNameExistenteException, EmailExistenteException {
		iConUs.altaUsuarioEstudiante("est","Estudi1", "Rodriguez", "est1@correo", null, false, "12345", System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "desconocido.jpg");
	}
	
	

}
