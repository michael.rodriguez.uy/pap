package datatypes;

public enum EstadoInscripcionEdc {
	Inscripto,Aceptado,Rechazado;

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		//return super.toString();
		if(this==Inscripto) {
			return "Inscripto";
		}else if(this==Aceptado) {
			return "Aceptado";
		}else {
			return "Rechazado";
		}
	}
	
}
