package datatypes;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
@XmlAccessorType(XmlAccessType.FIELD)
public class DtInscripcionEdc {
	private DtEdicion edicion;
	private DtUsuario estudiante;
	private Date fecha;
	private EstadoInscripcionEdc estado;
	
	public DtInscripcionEdc() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DtInscripcionEdc(DtEdicion edicion, DtUsuario estudiante, Date fecha, EstadoInscripcionEdc estado) {
		super();
		this.edicion = edicion;
		this.estudiante = estudiante;
		this.fecha = fecha;
		this.estado = estado;
	}
	
	public DtEdicion getEdicion() {
		return edicion;
	}

	public DtUsuario getEstudiante() {
		return estudiante;
	}

	public Date getFecha() {
		return fecha;
	}

	public EstadoInscripcionEdc getEstado() {
		return estado;
	}


	
}
