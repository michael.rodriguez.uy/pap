package datatypes;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import logica.Edicion;

//@XmlAccessorType(XmlAccessType.FIELD)
public class DtDocente extends DtUsuario {
	String nombreInsituto;
	
	public DtDocente(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento, Boolean esDocente, String nombreInstituto, String imagen) {
		super (nickName, nombre, apellido, correo, fechaNacimiento, esDocente, imagen);
		this.nombreInsituto= nombreInstituto ;
	}

	public String getNombreInstituto() {
		return this.nombreInsituto;
	}

	@Override
	public String toString() {
		return super.toString() + "\nINSTITUTO = " + nombreInsituto;
	}

	

}