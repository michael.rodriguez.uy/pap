package datatypes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



import logica.Curso;
import logica.Edicion;

@SuppressWarnings("unused")
public class DtCurso {
	private String nombre;
	private String descripcion;
	private String duracion;
	private int cantidadHoras;
	private int cantidadCreditos;	
	private String url;
	private Date fechaAlta;
	private String foto;
	private String[] dtprevias;
	private String[] categorias;
	public String[] getCategorias() {
		return categorias;
	}
	public void setCategorias(String[] categorias) {
		this.categorias = categorias;
	}
	public DtCurso(String nombre, String descripcion, String duracion, int cantidadHoras, int cantidadCreditos,
			String url, Date fechaAlta, String foto) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.duracion = duracion;
		this.cantidadHoras = cantidadHoras;
		this.cantidadCreditos = cantidadCreditos;
		this.url = url;
		this.fechaAlta = fechaAlta;
		this.foto=foto;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public void setDtprevias(String[] dtprevias) {
		this.dtprevias = dtprevias;
	}
	public String[] getDtprevias() {
		return dtprevias;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public int getCantidadHoras() {
		return cantidadHoras;
	}
	public void setCantidadHoras(int cantidadHoras) {
		this.cantidadHoras = cantidadHoras;
	}
	public int getCantidadCreditos() {
		return cantidadCreditos;
	}
	public void setCantidadCreditos(int cantidadCreditos) {
		this.cantidadCreditos = cantidadCreditos;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public DtCurso() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
