package datatypes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import logica.Docente;
import logica.Usuario;

@SuppressWarnings("unused")

public class DtEdicion {
	private String nombre;
	private Calendar fechaI;
	private Calendar fechaF;
	private int cupo;
	private Calendar fechaPub;
	private String imagen;
	private List<Docente> listaDocente = new ArrayList<Docente>();
	private String[] docentes;
	public DtEdicion() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DtEdicion(String nombre, Calendar fechaI, Calendar fechaF, int cupo, Calendar fechaPub, List<Docente> listaDocente, String imagen) {
		super();
		this.nombre = nombre;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
		this.cupo = cupo;
		this.fechaPub = fechaPub;
		this.listaDocente = listaDocente;
		this.imagen = imagen;
	}
	public String getNombre() {
		return nombre;
	}
	public void setDocentes(String[] docentes) {
		this.docentes = docentes;
	}
	public String[] getDocentes() {
		return docentes;
	}
	public Calendar getFechaI() {
		return fechaI;
	}
	public Calendar getFechaF() {
		return fechaF;
	}
	public int getCupo() {
		return cupo;
	}
	public Calendar getFechaPub() {
		return fechaPub;
	}
	public String getImagen() {
		return imagen;
	}
	public List<Docente> getListaDocentes() {
		return listaDocente;
	}	
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setFechaI(Calendar fechaI) {
		this.fechaI = fechaI;
	}
	public void setFechaF(Calendar fechaF) {
		this.fechaF = fechaF;
	}
	public void setCupo(int cupo) {
		this.cupo = cupo;
	}
	public void setFechaPub(Calendar fechaPub) {
		this.fechaPub = fechaPub;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public void setListaDocente(List<Docente> listaDocente) {
		this.listaDocente = listaDocente;
	}
	@Override
	public String toString() {
		return nombre +" " + cupo +"Fi " + fechaI ;
	}

	

}
