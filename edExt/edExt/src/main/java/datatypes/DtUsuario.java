package datatypes;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import logica.InscripcionEdc;


@XmlAccessorType(XmlAccessType.FIELD)
public class DtUsuario {
	private String nickName;
	public DtUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String nombre;
	private String apellido;
	private String correo;
	private Calendar fechaNacimiento;
	private boolean esDocente;
	private String imagen;
	private List<InscripcionEdc> inscripciones = new ArrayList<>();
	private InscripcionEdc[] inscripcionesEdcArray;
	
	
	public DtUsuario(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento2, boolean esDocente, String imagen) {
		super();
		this.nickName = nickName;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
		this.fechaNacimiento = fechaNacimiento2;
		this.esDocente = esDocente;
		this.setImagen(imagen);
	}
	
	
	
	public InscripcionEdc[] getInscripcionesEdcArray() {
		return inscripcionesEdcArray;
	}



	public void setInscripcionesEdcArray(InscripcionEdc[] inscripcionesEdcArray) {
		this.inscripcionesEdcArray = inscripcionesEdcArray;
	}



	public String getNickName() {
		return this.nickName;
	}
	public String getNombre() {
		return this.nombre;
	}
	public String getApellido() {
		return this.apellido;
	}
	public String getCorreo() {
		return this.correo;
	}
	public Calendar getFechaNacimiento() {
		return this.fechaNacimiento;
	}
	public boolean getEsDocente() {
		return this.esDocente;
	}	
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	
	@Override
	public String toString() {
		return "NICKNAME = " + nickName + "\nNOMBRE = " + nombre + "\nAPELLIDO = " + apellido + "\nCORREO = " + correo + "\nFECHA DE NACIMIENTO = " + fechaNacimiento + "\nES DOCENTE = " + esDocente;
	}


	
}
