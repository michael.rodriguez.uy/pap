package datatypes;

import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class DtPrograma {
	private String nombre;
	private String descripcion;
	private Calendar fechaI;
	private Calendar fechaF;
	public DtPrograma(String nombre, String descripcion, Calendar fechaI, Calendar fechaF) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
	}
	public DtPrograma() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getNombre() {
		return nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public Calendar getFechaI() {
		return fechaI;
	}
	public Calendar getFechaF() {
		return fechaF;
	}
}
