package UI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.awt.*;

public class PictureFrame extends JComponent{

    private final Image img;


    public PictureFrame(final String file) throws IOException {
        this(new File(file));
    }

    public PictureFrame(final File file) throws IOException {
        this(ImageIO.read(file));
    }

    public PictureFrame(BufferedImage img){
        this.img = img;
        this.setPreferredSize(new Dimension(img.getWidth(), img.getHeight()));
    }

    public void paintComponent(Graphics g){
        super.paintComponent(g);

        g.drawImage(img, 0, 0, getWidth(), getHeight(), null);
    }
}