package logica;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import datatypes.DtDocente;

@Entity
@DiscriminatorValue("Docente")
@XmlAccessorType(XmlAccessType.FIELD)
public class Docente extends Usuario{

	@Column(name = "nombreInstituto")
	String nombreInsituto;
	//@Column(name = "inst_DondeDictaCurso")
	//private List<Instituto> insitutosDelDocente= new ArrayList<Instituto>();
	
	@ManyToMany(cascade= CascadeType.ALL, mappedBy = "listaDocente")
	private List<Edicion> edicionesInscripto = new ArrayList<Edicion>();
	
	
	public Docente() {
	}
	
	public Docente(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento, Boolean esDocente,  String password, String imagen, String nombreInstituto) {
		super(nickName, nombre, apellido, correo, fechaNacimiento, esDocente, password, imagen);
		this.nombreInsituto= nombreInstituto ;
		}
	
	
	public String getNombreInsituto() {
		return nombreInsituto;
	}

	public void setNombreInsituto(String nombreInsituto) {
		this.nombreInsituto = nombreInsituto;
	}

	public List<Edicion> getEdicionesInscripto() {
		return edicionesInscripto;
	}

	public void setEdicionesInscripto(List<Edicion> edicionesInscripto) {
		this.edicionesInscripto = edicionesInscripto;
	}

	
	
}
 