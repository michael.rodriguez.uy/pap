package logica;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import datatypes.DtPrograma;
import persistencia.Conexion;
@Entity
@Table(name = "PROGRAMA")
public class Programa {
	@Id
	private String nombre;
	private String descripcion;
	private Calendar fechaI;
	private Calendar fechaF;
	private String foto;
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Curso> cursos= new ArrayList<Curso>();
	
	@OneToMany (mappedBy= "programa",cascade= CascadeType.ALL,orphanRemoval=true)
	private List<InscripcionPF> inscripcionesPF = new ArrayList<>();
	
	public Programa(String nombre, String descripcion, Calendar fechaI, Calendar fechaF, String foto) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaI = fechaI;
		this.fechaF = fechaF;
		this.foto= foto;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Calendar getFechaI() {
		return fechaI;
	}
	public void setFechaI(Calendar fechaI) {
		this.fechaI = fechaI;
	}
	public Calendar getFechaF() {
		return fechaF;
	}
	public void setFechaF(Calendar fechaF) {
		this.fechaF = fechaF;
	}
	public List<Curso> getCursos() {
		return cursos;
	}
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
	public boolean existeCurso(String NombreCurso) {
		boolean retorno = false;
		for(Curso cur: cursos) {
			if(cur.getNombre().equals(NombreCurso)) retorno=true;
		}
		return retorno;
	}
	

	DtPrograma getDtPrograma() {
	DtPrograma dtc;
	dtc= new DtPrograma(this.nombre, this.descripcion, this.fechaI, this.getFechaF());
		return 	dtc;
	}
	public Programa() {
		super();
		// TODO Auto-generated constructor stub
	}
	List<String> cursosDePrograma(){
		List<String> scursos =new ArrayList<String>();
		for(Curso c:cursos) {
			scursos.add(c.getNombre());
		}
		return scursos;
	}
	
	public void add(Curso c) {
		cursos.add(c);
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		em.persist(c);		
		em.getTransaction().commit();
	}
	
	

	public void agregarInscripcion(Estudiante estudiante){
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		//em.persist(u);
		Date factual = new Date();
		InscripcionPF i = new InscripcionPF();
		inscripcionesPF.add(i);
		estudiante.getInscripcionesPF().add(i);
		em.getTransaction().commit();
	}
	public boolean existePrograma( String nombre ) {
		boolean retorno = false;
		for(Curso c:cursos) {
			if(c.getNombre().equals(nombre)){
				retorno = true;
			}
		}
		return retorno;
	}
	public String[] getCategorias() {
		List<String> listaux = new ArrayList<String>();
		for(Curso iterador: this.cursos) {
			for(String iterador2 :iterador.getCategoriasString()) {
				if(!listaux.contains(iterador2)) {
					listaux.add(iterador2);
				}
			}
		}
		String [] retorno= new String[listaux.size()];
		int i=0;
		for(String iterador: listaux){
			retorno[i]=iterador;
			i++;
		}
		return retorno;
	}
}