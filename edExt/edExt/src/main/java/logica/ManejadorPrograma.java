package logica;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import persistencia.Conexion;

public class ManejadorPrograma {
	private static ManejadorPrograma instancia = null;
	
	//private List<Programa> programas = new ArrayList<Programa>();
	
	private ManejadorPrograma(){
		///////////PARA//PRUEBAS//////////

		//Programa p1= new Programa("Programa1" ,  "Un programa", new Date(), new Date());
		//Programa p2= new Programa("Programa2" ,  "Un programa", new Date(),new Date());
		//this.addPrograma(p1);
		/////////BORRAR////////////////

	}
	
	public static ManejadorPrograma getInstancia() {
		if (instancia == null)
			instancia = new ManejadorPrograma();
		return instancia;
	}
	// Es un copy paste del manejador de institutos
	public List<String> listarProgramas() {	
		/*
		List<String> salida = new ArrayList<String>();
		for(Programa p: programas) {
			salida.add(p.getNombre());
		} 
		return salida;
		*/
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		Query query = em.createQuery("select p from Programa p");
		@SuppressWarnings("unchecked")
		List<Programa> listPrograma = (List<Programa>) query.getResultList();
		
		ArrayList<String> aRetornar = new ArrayList<>();
		for(Programa p: listPrograma) {
			aRetornar.add(p.getNombre());
		}
		return aRetornar;
	}

	public List<Programa> getProgramas() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		Query query = em.createQuery("select p from Programa p");
		@SuppressWarnings("unchecked")
		List<Programa> listPrograma = (List<Programa>) query.getResultList();
		return listPrograma;
	}
	
	public void addPrograma(Programa p) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		
		em.persist(p);
		
		em.getTransaction().commit();
	}
	
	public Programa find(String nombre) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Programa programa = em.find(Programa.class, nombre);

		return programa;
	}
	/*
	public Usuario existeUsuario(String nickName) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Usuario usuario = em.find(Usuario.class, nickName);
		return usuario;
	}
		
*/
	public Curso obtenerCurso(String nombreCurso) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Curso curso = em.find(Curso.class, nombreCurso);
		
		return curso;
	}
	
	
}
