package logica;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "cat")
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
@Table(name = "CATEGORIA")
public class Categoria {
	@Id
	private String Nombre;
	
	
	public Categoria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Categoria(String nombre) {
		super();
		Nombre = nombre;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

}
