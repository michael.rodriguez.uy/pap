package logica;
import logica.Docente;
import logica.Usuario;
import persistencia.Conexion;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;

import datatypes.DtDocente;





@SuppressWarnings("unused")
public class ManejadorUsuario {
	private static ManejadorUsuario instancia = null;
	private List<Usuario> usuarios = new ArrayList<Usuario>();
	
	public ManejadorUsuario(){}
	
	public static ManejadorUsuario getInstancia() {
		if (instancia == null)
			instancia = new ManejadorUsuario();
		return instancia;
	}
	
	//AgregarUsuairio
	public void add(Usuario u) throws IOException {
		
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		byte[] buffer ;
		//if(u.getImagen()!="") {
		
			File f = new File(u.getImagen());
			InputStream is= new FileInputStream(f);
			buffer = new byte[(int) f.length()];
			int readers = is.read(buffer);
		
		
		em.getTransaction().begin();
		em.persist(u);
		u.setImg(buffer);
		em.getTransaction().commit();
	}
	
	
	//BuscarUsuario
	public Usuario existeUsuario(String nickName) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Usuario usuario = em.find(Usuario.class, nickName);
		return usuario;
	}
		
	
	public String  existeUsuarioString(String nickName) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		String aretornar = null;

		Usuario usuario = em.find(Usuario.class, nickName);
		Docente docente = em.find(Docente.class, nickName);
		if (usuario != null || docente !=null) {
				aretornar=usuario.getCorreo();
			}
		return aretornar;
	}
	
	
	
	
	//BuscarEmail devuelve un string con el mail encontrado, si no encuentra devuelve null
	public String existeEmail(String email) {
		String aretornarE = null;
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		ManejadorUsuario mu=ManejadorUsuario.getInstancia();
		Query query = em.createQuery("select u from usr u");
		List<Usuario> usuarios = query.getResultList();
		for(Usuario us: usuarios) {
			if(us.getCorreo().equals(email)) {
				aretornarE=us.getCorreo();
			}
		}
		return aretornarE;
	}
	
	// devuelve una lista de string de nikname de docentes	
	public List<String> getDocentes() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = em.createQuery("select u from usr u");
		List<Usuario> docentes = query.getResultList();
		List<String> salida = new ArrayList<String>();
		for(Usuario d: docentes) {	
			if (d.getEsDocente()) {
				salida.add(d.getNickName());			
			}				
		} 
		return salida;	
	}
	public List<String> getEstudiantes() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = em.createQuery("select u from usr u");
		List<Usuario> docentes = query.getResultList();
		List<String> salida = new ArrayList<String>();
		for(Usuario d: docentes) {	
			if (!d.getEsDocente()) {
				salida.add(d.getNickName());			
			}				
		} 
		return salida;	
	}
	
	public void altaUsuarioEstudiante(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento, Boolean esDocente,  String password, String imagen) {
		
		Estudiante e = new Estudiante();
		e.setNickName(nickName);
		e.setNombre(nombre);
		e.setApellido(apellido);
		e.setCorreo(correo);
		e.setFechaNacimiento(fechaNacimiento);
		e.setEsDocente(esDocente);
		e.setPassword(password);
		e.setImagen(imagen);
		usuarios.add(e);
	}
	

	public void altaDocente(String nickName, String nombre, String apellido, String email, Calendar fechaNacimiento, Boolean esDocente,  String password, String imagen, String nombreInstituto) throws IOException {
		Docente d = new Docente();
		
		d.setNickName(nickName);
		d.setNombre(nombre);
		d.setApellido(apellido);
		d.setCorreo(email);
		d.setFechaNacimiento(fechaNacimiento);
		d.setEsDocente(esDocente);
		d.setNombreInsituto(nombreInstituto);
		d.setPassword(password);
		d.setImagen(imagen);


	
		
		usuarios.add(d);
		
	}
	
	public Docente getUnDocente(String docente) {
		
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Docente d = em.find(Docente.class, docente);			
		return d;
	}
	
	public Estudiante getUnEstudiante(String nomEstudiante) {
		
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Estudiante e = em.find(Estudiante.class, nomEstudiante);			
		return e;
	}


	//obtiene lista de nombre de usuarios
	public ArrayList<String> obtenerUsuarios(){
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
			
		Query query = em.createQuery("select u from usr u");
		List<Usuario> listUsuario = (List<Usuario>) query.getResultList();
				
		//Inicializo un ArrayList de string.
		ArrayList<String> aRetornar = new ArrayList<>();
		//Recorro la coleccion de Usuarios.
		for(Usuario u: listUsuario) {
			//Se llena la ArrayList con los nombres de los Usuarios.
			aRetornar.add(u.getNickName());
		}
		//Se retorna la lista.
		return aRetornar;
	}
			
	public Usuario obtenerUsuario(String nick) {
		Usuario usuario = null;
			
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = em.createQuery("select u from usr u");
		List<Usuario> listUsuario = (List<Usuario>) query.getResultList();
			
		for(Usuario u: listUsuario) {
			if(u.getNickName().compareTo(nick) == 0) {
				usuario = u;
			}
		}
		return usuario;
	}
	
	public Usuario obtenerUsuarioViaEmail(String email) {
		Usuario usuario = null;
			
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = em.createQuery("select u from usr u");
		List<Usuario> listUsuario = (List<Usuario>) query.getResultList();
			
		for(Usuario u: listUsuario) {
			if(u.getCorreo().compareTo(email) == 0) {
				usuario = u;
			}
		}
		return usuario;
	}
	
	public List<String> obtenerEdicionesDeEstudiante(String nickName){
		List<String> ediciones = new ArrayList<String>();
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = em.createQuery("select e from InscripcionEdc e");
		List<InscripcionEdc> listInscripEdc = (List<InscripcionEdc>) query.getResultList();
		for(InscripcionEdc iedc: listInscripEdc) {
			if(iedc.getUsuario().getNickName().equals(nickName)) {
				ediciones.add(iedc.getEdicion().getNombre());
			}
		}
		return ediciones;
	}
	
	
	public DtDocente obtenerDocente(String nick) {
				
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Docente d = em.find(Docente.class, nick);			
		DtDocente dtDoc= new DtDocente(d.getNickName(), d.getNombre(), d.getApellido(), d.getCorreo(), d.getFechaNacimiento(), d.getEsDocente(), d.getNombreInsituto(), d.getImagen());
		return dtDoc;
	}
	
	
	
	
	
}
	

