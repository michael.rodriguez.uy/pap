package logica;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlType;

import datatypes.DtDocente;
import datatypes.DtEdicion;
import datatypes.DtInscripcionEdc;
import datatypes.EstadoInscripcionEdc;
import excepciones.InscripcionEdicionRepetida;
import persistencia.Conexion;



@Entity
@XmlType
public class Edicion {	
	@Id
	private String nombre;
	private Calendar fechaI;
	private Calendar fechaF;
	private int cupo;
	private Calendar fechaPub;
	private String imagen;
	
	@ManyToMany(cascade= CascadeType.ALL)
	private List<Docente> listaDocente= new ArrayList<Docente>();
	
	
	@OneToMany (mappedBy= "edicion",cascade= CascadeType.ALL,orphanRemoval=true)
	private List<InscripcionEdc> inscripciones = new ArrayList<>();
	
	public Edicion() {
		super();
	}
	
	public Edicion(String nombre, Calendar fechaI, Calendar fechaF, int cupo, Calendar fechaPub, List<Docente> listaDocente, String imagen) {

		System.out.println("Se creo Edicion: " + nombre);
		this.setNombre(nombre);
		this.setFechaI(fechaI);
		this.setFechaF(fechaF);
		this.setCupo(cupo);
		this.setFechaPub(fechaPub);	
		this.setListaDocente(listaDocente);
		this.setImagen(imagen);
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Calendar getFechaI() {
		return fechaI;
	}
	public void setFechaI(Calendar fechaI) {
		this.fechaI = fechaI;
	}
	public Calendar getFechaF() {
		return fechaF;
	} 
	public void setFechaF(Calendar fechaF) {
		this.fechaF = fechaF;
	}
	public int getCupo() {
		return cupo;
	}
	public void setCupo(int cupo) {
		this.cupo = cupo;
	}
	public Calendar getFechaPub() {
		return fechaPub;
	}
	public void setFechaPub(Calendar fechaPub) {
		this.fechaPub = fechaPub;
	}
	public List<Docente> getListaDocente() {
		return listaDocente;
	}
	public void setListaDocente(List<Docente> listaDocente) {
		this.listaDocente = listaDocente;
	}
	
	DtEdicion getDtEdicion() {
		 DtEdicion dte = new DtEdicion(this.nombre, this.fechaI, this.fechaF,this.cupo,this.fechaPub, this.listaDocente, this.imagen);
		 System.out.println("dte en Edicion.java: " + dte.toString());
		 
		 String[] doc = new String[this.listaDocente.size()];
		 int i =0;
		 for(Docente d: this.listaDocente) {
			 doc[i] = d.getNombre() + " "+ d.getApellido();
			 i++;
		 }
		 
		 dte.setDocentes(doc);
		 return dte;
	}
	public void agregarInscripcion(Usuario usuario){
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		//em.persist(u);
		Date factual = new Date();
		InscripcionEdc i = new InscripcionEdc(factual,usuario,this);
		inscripciones.add(i);
		usuario.getInscripciones().add(i);
		em.getTransaction().commit();
	}
	
	public DtInscripcionEdc[] obtenerInscripciones() {
		DtInscripcionEdc[] dtinscripciones = new DtInscripcionEdc[inscripciones.size()];
		        int i=0;
		        for(InscripcionEdc ins:inscripciones) {
		        	dtinscripciones[i]=ins.getDtInscripcionEdc();
		        	i++;
		        }
		        return dtinscripciones;
	}
	
	public void setearInscripciones(List<DtInscripcionEdc> listaAceptados) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		for(DtInscripcionEdc ded:listaAceptados ) {
			System.out.print(ded.getEstudiante().getNickName() + " " + ded.getEstado().toString() + "\n");
			for(InscripcionEdc iedc: inscripciones) {
				if(iedc.getUsuario().getNickName().equals(ded.getEstudiante().getNickName())){
					
					iedc.setEstado(ded.getEstado());
				}
			}
			

		}
		em.getTransaction().commit();	
	}
	
	public boolean fueRechazado(String nickEstudiante) {
		boolean encuentra=false;
		//System.out.println(nickEstudiante + "Fr");
		for(InscripcionEdc iedc: inscripciones){
			if(iedc.getUsuario().getNickName().equals(nickEstudiante)) {
				//System.out.println(iedc.getEstado().toString());
				if(iedc.getEstado()==EstadoInscripcionEdc.Rechazado) encuentra=true;
			}
		}		
			return encuentra;	

	}
	


	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	
	public  ArrayList <DtInscripcionEdc> inscrpcionesAceptadas(){
		ArrayList<DtInscripcionEdc> dtediciones = new ArrayList<>();
		for(InscripcionEdc i: inscripciones) {
			if(i.getEstado()==EstadoInscripcionEdc.Aceptado) {
				dtediciones.add(i.getDtInscripcionEdc());
			}
		}
		return dtediciones;
	}
}
