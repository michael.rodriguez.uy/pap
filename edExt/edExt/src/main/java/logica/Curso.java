package logica;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;

import datatypes.DtCurso;
import datatypes.DtEdicion;
import datatypes.DtInscripcionEdc;
import persistencia.Conexion;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;


@SuppressWarnings("unused")


@Entity
//@Table(name="CURSOS")
public class Curso {
	@Id
	private String nombre;
	private String descripcion;
	private String duracion;
	private int cantidadHoras;
	private int cantidadCreditos;	
	private String url;
	private Date fechaAlta;
	private String foto;
	
	
	
	//@OneToMany(cascade=CascadeType.ALL,orphanRemoval=true)
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(joinColumns = @JoinColumn(referencedColumnName = "nombre", nullable = false))
	private List<Edicion> ediciones = new ArrayList<Edicion>();
	
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Curso> previas = new ArrayList<Curso>(); //el public es para las pruebas
	
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Categoria> categorias = new ArrayList<Categoria>();
	
	public Curso() {
		super();
		// TODO Auto-generated constructor stub
		//System.out.println("Se creo CURSO ");
		//Edicion(String nombre, Date fechaI, Date fechaF, int cupo, Date fechaPub)
		//Date dt = new Date();

	}
	public Curso(String nombre, String descripcion, String duracion, int cantidadHoras, int cantidadCreditos, String url,Date fechaAlta, String foto) {
		super();
		this.setNombre(nombre);
		this.setDescripcion(descripcion);
		this.setDuracion(duracion);
		this.setCantidadHoras(cantidadHoras);
		this.setCantidadCreditos(cantidadCreditos);
		this.setUrl(url);
		this.setFechaAlta(fechaAlta);
		this.setFoto(foto);
		//this.setEdiciones(ediciones);
		
		//System.out.println("Se creo CURSO "+ nombre );
		//Edicion e1= new Edicion("PrimeaEdicion", new Date(),new Date(), 45,new Date());
		//Edicion e2= new Edicion("SegundaEdicion", new Date(),new Date(), 45,new Date());
		//ediciones.add(e1); //ediciones.add(e2);


	}
	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public int getCantidadHoras() {
		return cantidadHoras;
	}
	public void setCantidadHoras(int cantidadHoras) {
		this.cantidadHoras = cantidadHoras;
	}
	public int getCantidadCreditos() {
		return cantidadCreditos;
	}
	public void setCantidadCreditos(int cantidadCreditos) {
		this.cantidadCreditos = cantidadCreditos;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	
	public List<Edicion> getEdiciones() {
		return ediciones;
	}
	public void setEdiciones(List<Edicion> ediciones) {
		this.ediciones = ediciones;
	}
	public void addPRevia(Curso cur) {
		//Conexion conexion = Conexion.getInstancia();
		//EntityManager em = conexion.getEntityManager();
		//em.getTransaction().begin();

		this.getPrevias().add(cur);
		//em.persist(this);
		//em.getTransaction().commit();
	}
	public List<Curso> getPrevias() {
		return previas;
	}
	public DtCurso getDtCurso() {
		DtCurso dtc = new DtCurso();
		dtc.setNombre(this.nombre);;
		dtc.setDescripcion(this.descripcion);
		dtc.setDuracion(this.duracion);
		dtc.setCantidadCreditos(this.cantidadCreditos);
		dtc.setCantidadHoras(this.cantidadHoras);
		dtc.setUrl(this.url);
		dtc.setFechaAlta(this.fechaAlta);
		dtc.setFoto(foto);
		
		Object[] o = this.previas.toArray();
		String[] dprevias= new String[o.length];
		Curso k;
        for (int i = 0; i < o.length; i++) {
        	k=(Curso) o[i];
        	dprevias[i] = k.getNombre();
        }
        dtc.setDtprevias(dprevias);
        Categoria Q;
		Object[] ob = this.categorias.toArray();
		String[] strcategorias= new String[ob.length];
        for (int i = 0; i < ob.length; i++) {
        	Q=(Categoria) ob[i];
        	strcategorias[i] = Q.getNombre();
        }
        dtc.setCategorias(strcategorias);
		return dtc;
	}
	public List<String> obtenerEdiciones(){
		List<String> lista = new ArrayList<String>();
		for(Edicion ed: ediciones) {
			lista.add(ed.getNombre());
		}
		return lista;
	}
	public DtEdicion obtenerUnaEdicion(String nombreEdicion) {
		DtEdicion dte = new DtEdicion();
		for(Edicion ed: ediciones) {
			if(ed.getNombre().equals(nombreEdicion)) {
				dte=ed.getDtEdicion();
			}
		}
		return dte;
	}
	
	public boolean existeEdicion(String nombreEdicion) {
		//System.out.println("evaluo: "+nombreEdicion);
		for(Edicion ed: ediciones) {
			System.out.println("enbase: "+ed.getNombre());
			if(ed.getNombre().equals(nombreEdicion)) {
				return true;
			}
		}	
		return false;
	}
	
	public void addEdicion(Edicion e) {
		ediciones.add(e);		
	}
	public void addCategoria(Categoria cat) {
		//System.out.print(" Agreg Categoria");
		this.categorias.add(cat);
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		em.persist(cat);
		em.getTransaction().commit();
		
	}
	public List<Categoria> getCategorias() {
		return this.categorias;
	}
	public List<String> getCategoriasString() {		
		List<String> retorno = new ArrayList<String>();
		for(Categoria iterador: this.categorias) {
			retorno.add(iterador.getNombre());
		}
		return retorno;
	}
	public Categoria getCategoria(String nomcat) {
		Categoria retorno=null;
		if(categorias.size()>0) {
			for(Categoria iterador:this.categorias) {
				if(iterador.getNombre().compareTo(nomcat)==0) {
					retorno=iterador;
				}
			}
		}
		return retorno;
	}
	
	public DtEdicion traerUltimaEdicion() {
		DtEdicion dte =null; 
		if(!this.ediciones.isEmpty()) {
			// modiificar para edicion vigente fpub < today and ffinal >today
			//construir un arraylist con los que cumplen y mandar el ultimo
			Edicion ultima = this.ediciones.get(this.ediciones.size()-1);
			dte = ultima.getDtEdicion();		
		}
		return dte;

	}

	public DtInscripcionEdc[] obtenerInscripciones(String edicion, boolean prioridad) {
		Edicion enc=null;
		for(Edicion e: ediciones) {
			if(e.getNombre().equals(edicion)) {
				enc=e;
			}
		}
		if(prioridad) {
			return this.OrdenarPrioridad(enc);
		}else {
			return enc.obtenerInscripciones();	
		}
	}
	
	public void setearInscripciones(String edicion, List<DtInscripcionEdc> listaAceptados) {
		Edicion enc=null;
		for(Edicion e: ediciones) {
			if(e.getNombre().equals(edicion)) {
				enc=e;
			}
		}
		enc.setearInscripciones(listaAceptados);	
	}
	
	
	//no esta terminada
	public DtInscripcionEdc[] OrdenarPrioridad(Edicion edicion) {
	DtInscripcionEdc[] dtinscripciones = edicion.obtenerInscripciones();
		 for(int i=0;i<(dtinscripciones.length-1);i++){
			 for(int j=i+1;j<dtinscripciones.length;j++) {
				 if(vecesRechazado(dtinscripciones[i].getEstudiante().getNickName())<vecesRechazado(dtinscripciones[j].getEstudiante().getNickName())) {
					 //Intercambio Valores
					 DtInscripcionEdc aux = dtinscripciones[i];
					 dtinscripciones[i]=dtinscripciones[j];
					 dtinscripciones[j]=aux;
				 }
			 }
		}	
		 /*
		for(DtInscripcionEdc dtedc :dtinscripciones) {
			System.out.println(dtedc.getEstudiante().getNickName()+ " "+ vecesRechazado(dtedc.getEstudiante().getNickName()));
		}*/
	return dtinscripciones;	 
			
	}


	// si quisiera descontar la edicion actual o vigente, deberia pasarla pero no sumaria x def
	public int vecesRechazado(String nickName) {
		int i =0;
		for(Edicion ed: this.ediciones) {
			//System.out.println(" " + ed.fueRechazado(nickName));

			if(ed.fueRechazado(nickName)) {
				i++;
			}
		}
		return i;
	}
	public  ArrayList <DtEdicion> mostrarEdiciones(){
		ArrayList<DtEdicion> dtediciones = new ArrayList<>();
		for(Edicion e: ediciones) {
			dtediciones.add(e.getDtEdicion());
		}
		return dtediciones;
	}
	
	public  ArrayList <DtInscripcionEdc> inscrpcionesAceptadas(String nomEdicion){
		Edicion ed=null;
		for(Edicion e: ediciones) {
			if(e.getNombre().equals(nomEdicion)) {
				ed=e;
			}
		}
		return ed.inscrpcionesAceptadas();
		
	}
	
	public boolean existeCategoria( String nombre ) {
		boolean retorno = false;
		for(Categoria c:categorias) {
			//System.out.println("Listando cursos en noexiste " + c.getNombre());
			if(c.getNombre().equals(nombre)){
				retorno = true;
			}
		}
		return retorno;
	}
	
	
}
