package logica;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import datatypes.DtDocente;
import datatypes.DtInscripcionEdc;
import datatypes.DtUsuario;

import org.hibernate.annotations.Type;


@SuppressWarnings("unused")
@Entity(name = "usr")
@Table(name = "USUARIOS")

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Usuario {

	@Id
	@Column(name = "nickName")
	private String nickName;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "apellido")
	private String apellido;
	@Column(name = "correo")
	private String correo;
	@Column(name = "fNacimiento")
	@Basic
	@Temporal(TemporalType.DATE)
	private Calendar fechaNacimiento;
	@Column(name = "esDocente")
	@Type(type = "yes_no")
	private Boolean esDocente;
	@Lob
	@Column(name = "imagenPath")
	private byte[] img;
	private String password;
	private String imagen;

	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<InscripcionEdc> inscripciones = new ArrayList<>();

	@JoinColumn(nullable = false)
	@ManyToMany
	private List<Usuario> seguidores = new ArrayList<>();

	@ManyToMany(mappedBy = "seguidores")
	private List<Usuario> seguidos = new ArrayList<>();

	public Usuario() {
	}

	public Usuario(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento2,
			Boolean esDocente, String password, String imagen) {
		super();
		this.setNickName(nickName);
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setCorreo(correo);
		this.setFechaNacimiento(fechaNacimiento2);
		this.setEsDocente(esDocente);
		this.setImagen(imagen);
		this.setPassword(password);

	}

	public void setImg(byte[] img) {
		this.img = img;
	}

	public byte[] getImg() {
		return img;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Calendar getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Calendar fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public void setEsDocente(Boolean esDocente) {
		this.esDocente = esDocente;
	}

	public Boolean getEsDocente() {
		return esDocente;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	/// get y set de lista Inscripciones///
	public List<InscripcionEdc> getInscripciones() {
		return inscripciones;
	}

	public void setInscripciones(List<InscripcionEdc> inscripciones) {
		this.inscripciones = inscripciones;
	}
	
	


	public String getImagen() {
		return imagen;
	}
	
	public DtUsuario getDtUsuario() {
		DtUsuario dtu=new DtUsuario(this.nickName,this.nombre,this.apellido,this.correo,this.fechaNacimiento,this.esDocente,this.imagen);
		InscripcionEdc[] inscArray = new InscripcionEdc[inscripciones.size()];
		for(int i=0; i<inscripciones.size(); i++) {
			inscArray[i]=inscripciones.get(i);
		}
		
		dtu.setInscripcionesEdcArray(inscArray);
		return dtu;
	}
	
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public void seguir(Usuario usuario) throws IOException {
		if (!seguidores.add(usuario)) {
			throw new IOException("Ya se encuentra siguiendo a: " + usuario.getNickName());
		}
	}

	public void dejarDeSeguir(Usuario usuario) throws IOException {
		if (!seguidores.remove(usuario)) {
			throw new IOException("No sigues a, " + usuario.getNickName() + " imposible dejar de seguir");
		}
	}
	
	public List<DtInscripcionEdc> getInscripcionesDt(){
		List<DtInscripcionEdc> retorno = new ArrayList<DtInscripcionEdc>();
		for (InscripcionEdc iterador : inscripciones) {
			retorno.add(iterador.getDtInscripcionEdc());
		}
		return retorno;
	}
	
	public List<Usuario> getUsuariosSeguidos() {
		return seguidos;
	}
	
	public void setUsuariosSeguidos(List<Usuario> usuarios) {
		this.seguidos = usuarios;
	}
}
