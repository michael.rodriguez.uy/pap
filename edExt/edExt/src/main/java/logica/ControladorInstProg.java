package logica;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import datatypes.DtCurso;
import datatypes.DtEdicion;
import datatypes.DtInscripcionEdc;
import datatypes.DtPrograma;
import excepciones.CategoriaRepetidaException;
import excepciones.CursoDeProgramaRepetido;
import excepciones.InscripcionEdicionRepetida;
import excepciones.ProgramaExisteException;
import excepciones.SinEdicionException;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import persistencia.Conexion;
public class ControladorInstProg implements IControladorInstProg {
	private  Instituto i;
	private  Curso c;// es una variable para "recordar" el instituto, ya s� es horrible pero no encontre como se hace
	//sacarle el public lo hice para testear
	private Programa p;
	
	public ControladorInstProg() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String[] listarInstitutosArray() { // para el caso de uso Alta Curso
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();	
		//System.out.println("Listando..Nada" );
		List<String> salida = mi.listarInstituto();
	       if (salida.isEmpty())
	            return null;
	        else {
	            Object[] o = salida.toArray();
	            String[] miarray = new String[o.length];
	            for (int i = 0; i < o.length; i++) {
	            	miarray[i] = (String) o[i];
	            }	            
	            return miarray;
	        }
	}
	@Override
	public String[] listarInstitutos() {		
		ManejadorInstitutos mI = ManejadorInstitutos.getInstancia();
		List<String> Institutos =  mI.listarInstituto();
		//for(String s: Institutos) System.out.println("Listando.." + s);
		String[] Institutos_ret = new String[Institutos.size()];
        int i=0;
        for(String s:Institutos) {
        	Institutos_ret[i]=s;
        	i++;
        }
        return Institutos_ret;
	}
	
	public boolean ingresarNomInst(String nom) {
		boolean existe= true;
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		if (!mi.exists(nom)) {
			i = new Instituto();
			i.setNombre(nom);
			mi.add(i);
			//System.out.println("llego " + i.getNombre());
			existe = false;
		}else {
			System.out.println("Ac� seria la excepcion ese instituto ya existe ");
			existe = true;
		}
		return existe;
	}

	public void CancelarAltaInstituto() {
		i=null;
	}
	public boolean nuevoCurso(String nombreCurso ) {
		//i=null;
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		List<Instituto> institutos = mi.getInstitutos();
		boolean noexiste = true;
		for(Instituto ins :institutos) {
			if(!ins.noExisteCurso(nombreCurso)) {
				noexiste=false;
			}
		}
		if(noexiste) {
			//System.out.println("Se Crea el curso Static ");
			i.crearCursoStatic(nombreCurso);
		}
		return noexiste;
	}
	public void seleccionarInstituto(String nombreInst) {
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();	
		i = mi.getInstituto(nombreInst);
	}
	public void seleccionarCursoInstancia(String nombreCurso) {
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();	
		c = mi.getCruso(nombreCurso);
	}
	
		
	public void setearCurso(String nombreCurso, String desc, String duracion, int cantHoras, int cantCreditos, String url, Date fechaAlta, String foto) {		
		i.setearCurso(nombreCurso, desc, duracion, cantHoras, cantCreditos, url, fechaAlta, foto);
	}
	public String[] listarPrevias(){
		List<String> cursos=i.listarNombreCursos();
		//for(String s:cursos ) System.out.println(s + " Institutos.listarPrevvias.List");
		String[] miarray = new String[cursos.size()];
		miarray=cursos.toArray(miarray);
		//for(String s:miarray ) System.out.println(s + " Institutos.listarPrevvias.Array");
		return miarray;
	}
	public void seleccionarPrevias(List<String> previas) {
		i.setearPrevias(previas);
	}
	public void confirmarAltaCurso() {
		i.confirmarAltaCurso();
		//i=null;
	}
	public String[] listarNombreCursos(String nombreInst){ //previamente nombreCursos de mostrar cursos
		List<String> cursos;
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();	
		i = mi.getInstituto(nombreInst);
		cursos = i.listarNombreCursos();
		String[] miarray = new String[cursos.size()];
		miarray=cursos.toArray(miarray);	
		return miarray;
	}
	public DtCurso mostrarDatosdeCurso(String nombreCurso) {
		return i.mostrarDatosdeCurso(nombreCurso);
	}
	
	public boolean altaEdicionCurso(String nombreInstituto, String nombreCurso, String nombreEdicion, Calendar fechaI, Calendar fechaF,
			int cupo, Calendar fechaPub, List<Docente> listaDocentes, String imagen) {
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();	
		i = mi.getInstituto(nombreInstituto);	
		
		//System.out.println("Controlador listaDocentes:" + listaDocentes.size());
		return mi.altaEdicionCurso(i, nombreCurso, nombreEdicion, fechaI, fechaF, cupo, fechaPub, listaDocentes, imagen);
		
	} 
	
	public String[] listarEdicionesdeCurso(String nombreCurso){
		List<String> ediciones;
		ediciones=i.obtenerEdiciones(nombreCurso);
		String[] miarray = new String[ediciones.size()];
		miarray=ediciones.toArray(miarray);	
		return miarray;
	}
	public String[] listarProgramasCurso(String nombreCurso){
		ManejadorPrograma mp = ManejadorPrograma.getInstancia();
		List<String> listast= new ArrayList<String>();
		//System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCCCCCCCCCCCAAAAAAAAAAA");
		List<Programa> listap = mp.getProgramas();
		for(Programa p:listap) {
			if(p.existeCurso(nombreCurso) || nombreCurso=="*") {
				listast.add(p.getNombre());
			}
		}
		String[] miarray = new String[listast.size()];
		miarray=listast.toArray(miarray);
		return miarray;
	}
	public DtPrograma verPrograma(String nombrePrograma) {
		ManejadorPrograma mp = ManejadorPrograma.getInstancia();
		p=mp.find(nombrePrograma);
		return p.getDtPrograma();
	}
	
	public DtPrograma mostrarPrograma(String nombrePrograma) {
		ManejadorPrograma mp = ManejadorPrograma.getInstancia();
		p=mp.find(nombrePrograma);
		return p.getDtPrograma();
	}
	
	public Boolean existePrograma(String nombrePrograma) {
		ManejadorPrograma mp= ManejadorPrograma.getInstancia();
		p=mp.find(nombrePrograma);
		String nom=p.getNombre();
		Boolean retorno=false;
		if(nom!=null) {
			retorno=true;
		}
		return retorno;
		
	}
	
	public String[] cursosDePrograma(){	//funciona con mostrarPrograma que recuerda p
		List<String> programas=p.cursosDePrograma();
		String[] miarray = new String[programas.size()];
		miarray=programas.toArray(miarray);
		return miarray;
	}
	public DtEdicion mostrarEdicion(String nombreEdicion) {
		System.out.println(i);		
		return i.obtenerEdicion(nombreEdicion);
	}
	public void cancelarConsultaCurso() {
		i.borrarC();
		i=null;
		p=null;
	}
	
	public void cancelarAltaCurso() {
		if(i!=null) {
			i.c=null;
			i=null;
		}
	}
	
	public void cancelarAltaEdicionCurso() {
		if(i!=null) {
			i.c=null;
			i=null;
		}
	}

	///////////////////PROGRAMA//////////
	public void altaPrograma(String nombrePrograma, String descripcion, Calendar fechaI, Calendar fechaF, String foto) throws ProgramaExisteException {

		ManejadorPrograma mp = ManejadorPrograma.getInstancia();
		String fotto=  "cursodefault.jpg";
		if(foto == null) {
			foto=fotto;
		}
		Programa programa = mp.find(nombrePrograma);
		if(programa != null)
			 throw new ProgramaExisteException("El programa " + nombrePrograma + " ya esta registrado");
		
		Programa pr;
		if(programa == null ) {
				pr = new Programa(nombrePrograma, descripcion, fechaI, fechaF, foto);
				mp.addPrograma(pr);
			}
	}
	

	public Instituto obtenerInstituto(String nomins){
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		Instituto retorno=null;
		for (Instituto iterador:mi.getInstitutos()) {
			if(iterador.getNombre()==nomins) {
				retorno = iterador;
			}
		}
		return retorno;
	}
	
	public Curso obtenerCurso(Instituto ins, String nomcur) {
		Curso retorno = null;
		for (Curso Iterador: ins.getCursos()) {
			if(nomcur==Iterador.getNombre()) {
				retorno = Iterador;
			}
		}
		return retorno;
	}
	public Edicion obtenerEdicion(Curso cur, String nomedi) {
		Edicion retorno = null;
		for (Edicion Iterador: cur.getEdiciones()) {
			if(nomedi==Iterador.getNombre()) {
				retorno = Iterador;
			}
		}
		return retorno;
	}
	
	public Edicion obtenerEdicion(String nomedi) {
		Edicion retorno = null;
		boolean encontre=false;
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		for(Instituto iterador: mi.getInstitutos()) {
			if(encontre == false) {
				for(Curso iterador2: iterador.getCursos()) {
					if (encontre ==false) {
						for(Edicion iterador3 : iterador2.getEdiciones()) {
							if(iterador3.getNombre().equals(nomedi)) {
								retorno = iterador3;
								encontre = true;
							}						
						}
					}
				}
			}
		}
		return retorno;
	}
	public Edicion obtenerEdicionVigente(Curso objetivo) {
		Edicion retorno = null;
		Calendar fechaactual = new GregorianCalendar() ;
		for(Edicion iterador:objetivo.getEdiciones()) {
			if (iterador.getFechaI().compareTo(fechaactual)>0) {
				retorno=iterador;
			}
		}
		return retorno;
	}
	
	public Programa obtenerPrograma(String nombrePF){
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		Programa retorno=null;
		for (Programa iterador:mi.getProgramas()) {
			if(iterador.getNombre().equals(nombrePF)) {
				retorno = iterador;
			}
		}
		return retorno;
	}
	
	public void realizarInscripcionProgramaDeFormacion(String pf, String estudiante) throws InscripcionEdicionRepetida {
		
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();		
		Programa p = obtenerPrograma(pf);
		Estudiante e = mU.getUnEstudiante(estudiante); 
		for(InscripcionPF ipf:e.getInscripcionesPF()) {
			if(ipf.getInscripto().equals(e)) {
				throw new InscripcionEdicionRepetida(e.getNickName()+" ya esta inscrito en "+p.getNombre());
			}
			p.agregarInscripcion(e);
		}
	}
	
	public void altaInscripcionEdc(String edicion, String estudiante) throws InscripcionEdicionRepetida {	
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Estudiante objetoUsuario = mU.getUnEstudiante(estudiante); 
		Edicion objetoEdicion = obtenerEdicion(edicion);
		for(InscripcionEdc iedc:objetoUsuario.getInscripciones()) {
			if(iedc.getEdicion()==objetoEdicion) {
				throw new InscripcionEdicionRepetida(objetoUsuario.getNickName()+" ya esta inscrito en "+objetoEdicion.getNombre());
				}
		}
		objetoEdicion.agregarInscripcion(objetoUsuario);
	}
	
	
	public String[] listarProgramas() {
		List<String> programas;
		ManejadorPrograma mP = ManejadorPrograma.getInstancia();
		programas = mP.listarProgramas();
		String[] programas_ret = new String[programas.size()];
		int i = 0;
		for(String p:programas) {
			programas_ret[i] = p;
			i++;
		}
		return programas_ret;
	}
	
	public DtCurso seleccionarCurso(String nombreCurso) {
		ManejadorPrograma mP = ManejadorPrograma.getInstancia();
		Curso curso = mP.obtenerCurso(nombreCurso);
		DtCurso dtCurso = curso.getDtCurso();
		return dtCurso;
	}
	
	public List<Curso>listarCursosP(String nomPrograma){
		List<DtCurso> cursos = null;
		ManejadorPrograma mP = ManejadorPrograma.getInstancia();
		Programa p = mP.find(nomPrograma);
		List<Curso> listaCurso = p.getCursos();
		return listaCurso;
	}
	
	public Edicion seleccionarEdicion(String nombreE) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Edicion edicion = em.find(Edicion.class, nombreE);
		
		return edicion;
	}
	
	public List<String> listarEdiciones(String nomCurso) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Curso c = em.find(Curso.class, nomCurso);
		return c.obtenerEdiciones();
	}
	
	public String[] listarCursos(){
		List<String> cursos;
		ManejadorInstitutos mI = ManejadorInstitutos.getInstancia();
		cursos = mI.getCursos();
		String[] cursos_ret = new String[cursos.size()];
		int i = 0;
		for(String c:cursos) {
			cursos_ret[i] = c;
			i++;
		}
		return cursos_ret;
	}
	

	/*
	public DtPrograma seleccionarPrograma(String nomPrograma) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		DtPrograma programa = em.find(DtPrograma.class, nomPrograma);
		return programa;
	}*/
	public void agregarCursoPrograma(String nomProg, String nomCurso) throws CursoDeProgramaRepetido {
		ManejadorPrograma mP = ManejadorPrograma.getInstancia();
		Programa p = mP.find(nomProg);
		ManejadorInstitutos mI = ManejadorInstitutos.getInstancia();
		Curso c = mI.getCurso(nomCurso);
		
		for(Curso cu: p.getCursos()) {
			if(cu.getNombre().equals(nomCurso)) {
				throw new CursoDeProgramaRepetido(nomCurso+" ya esta asociado al Programa "+nomProg);
			}
		}
		
		p.add(c);
		
	}
	public ArrayList<String> obtenerCategorias(){
		ManejadorCategoria mCat = ManejadorCategoria.getInstancia();		
		return mCat.obtenerCategorias();	
	}
	public Categoria buscarCategoria(String id) {
		ManejadorCategoria mCat = ManejadorCategoria.getInstancia();
		return mCat.buscarCategoria(id);
	}
	
	public void agregarCategoria(String nombreCat) throws CategoriaRepetidaException {
		ManejadorCategoria mCat = ManejadorCategoria.getInstancia();
		if(!mCat.existeCategoria(nombreCat)) {
			Categoria cat = new Categoria(nombreCat);
			mCat.agregarCategoria(cat);
		}else {
			throw new CategoriaRepetidaException("Ya existe "+ nombreCat);
			
		}


	}
	
	public DtEdicion traerUltimaEdicion(String curso, String instituto) throws SinEdicionException {
		boolean existe = false;
		ManejadorInstitutos mi= ManejadorInstitutos.getInstancia();
		Instituto in = mi.getInstituto(instituto);
		if(in.traerUltimaEdicion(curso)==null){
			throw new SinEdicionException(instituto + " "+ curso +" "+ "sin edicion " );
		}
		return in.traerUltimaEdicion(curso);
	}
	
	public DtInscripcionEdc[] obtenerInscripciones(String curso, String instituto,String edicion, boolean prioridad){
		ManejadorInstitutos mi= ManejadorInstitutos.getInstancia();
		Instituto in = mi.getInstituto(instituto);
		return in.obtenerInscripciones(curso ,edicion,  prioridad);
	}
	public void setearInscripciones(String instituto, String curso, String edicion, List<DtInscripcionEdc> listaAceptados) {
		ManejadorInstitutos mi= ManejadorInstitutos.getInstancia();
		Instituto in = mi.getInstituto(instituto);
		in.setearInscripciones(curso , edicion,listaAceptados);
	}
	
	public  ArrayList <DtEdicion> mostrarEdiciones(String nomInstituto, String nombreCurso)throws SinEdicionException{
		ManejadorInstitutos mi= ManejadorInstitutos.getInstancia();
		Instituto in = mi.getInstituto(nomInstituto);
		ArrayList dtediciones = in.mostrarEdiciones(nombreCurso);
		if(dtediciones.isEmpty()) {
			throw new SinEdicionException(nomInstituto + " "+ nombreCurso +" "+ "sin edicion " ); 
		}
		return dtediciones;
	}
	public  ArrayList <DtInscripcionEdc> inscrpcionesAceptadas(String nomInstituto, String nombreCurso,String nomEdicion){
		ManejadorInstitutos mi= ManejadorInstitutos.getInstancia();
		Instituto in = mi.getInstituto(nomInstituto);
		return in.inscrpcionesAceptadas(nombreCurso, nomEdicion);
	}
	
	//de alta curso
	public  ArrayList<String> listarCategorias(){
		ArrayList<String> categorias = null;
		ManejadorCategoria mCat = ManejadorCategoria.getInstancia();
		categorias = mCat.obtenerCategorias();
		return categorias;
	}
	
	public List<Curso> obtenerCursosdeCategoria(String nombrecat){
		ManejadorCategoria mCat = ManejadorCategoria.getInstancia();
		Categoria objetivo = mCat.buscarCategoria(nombrecat);
		
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		List<Instituto> listInstitutos= mi.getInstitutos();
		List<Curso> cursosObjetivo = new ArrayList<Curso>();;
		for(Instituto iteradorIns: listInstitutos) {
			if(!iteradorIns.getCursos().isEmpty()) {
				List<Curso> listRevisarcursos = iteradorIns.getCursos();
				for(Curso iteradorCursos:listRevisarcursos) {
					List<Categoria> catsdeestecurso = iteradorCursos.getCategorias();
					if(catsdeestecurso.contains(objetivo)) {
						cursosObjetivo.add(iteradorCursos);
					}
				}
				
			}
		}
		return cursosObjetivo;
	}
	
	public ArrayList<String> imprimirCursosdeCategoria(String nombrecat){
		List<Curso> cursos = obtenerCursosdeCategoria(nombrecat);
		ArrayList<String> nombresdecursos = new ArrayList<String>();
		if(!cursos.isEmpty()) {
			for(Curso iterador:cursos) {
				nombresdecursos.add(iterador.getNombre());
			}
		}
		return nombresdecursos;
	}
	public void setearCategorias(ArrayList<String> categorias) {
		ManejadorCategoria mCat = ManejadorCategoria.getInstancia();		
		ArrayList<Categoria> categclase= new ArrayList<>();
		for(String cat: categorias) {
			Categoria categ= mCat.buscarCategoria(cat);
			categclase.add(categ);
		}
		i.setearCategorias(categclase);
	}
	
	public DtCurso[] cursosdeCategoria(String categoria) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = (Query) em.createQuery("select c from Curso c");
		List<Curso> listadeCursos = (List<Curso>) (query).getResultList();
		ArrayList<DtCurso> listaDtCursos = new ArrayList<DtCurso>();
		for(Curso c: listadeCursos) {
			if(c.existeCategoria(categoria)) {
				listaDtCursos.add(c.getDtCurso());
			}			
		}
		DtCurso[] arrayCursos = new DtCurso[listaDtCursos.size()];
		arrayCursos=listaDtCursos.toArray(arrayCursos);
		return arrayCursos;
	}
	
	public DtCurso datosdeCurso(String nombreCurso) {
		DtCurso dtc=null;
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Query query = (Query) em.createQuery("select c from Curso c");
		List<Curso> listadeCursos = (List<Curso>) (query).getResultList();
		for(Curso c: listadeCursos) {
			if(c.getNombre().equals(nombreCurso)) {
					dtc=c.getDtCurso();
			}			
		}
		return dtc;
	}
	
	public DtPrograma[] programasdeCurso(String nomCurso){
		 
		ManejadorPrograma mProg = ManejadorPrograma.getInstancia();
		List<Programa> programas = mProg.getProgramas();
		ArrayList<DtPrograma> listadtprogramas = new ArrayList<>();

		for(Programa p: programas) {
			if(p.existeCurso(nomCurso)) {
				listadtprogramas.add(p.getDtPrograma());
			}
		}
		DtPrograma[] dtp=new DtPrograma[listadtprogramas.size()];
		int i= 0;
		if(!programas.isEmpty()&&!listadtprogramas.isEmpty()) {
			for(DtPrograma dtpl: listadtprogramas) {
				dtp[i]=dtpl;
				i++;
			}		
		}

		
		return dtp;
	}
	
	public  DtEdicion[] mostrarDtEdicionesArray(String nombreCurso){
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Curso cur = em.find(Curso.class, nombreCurso);
		ArrayList<DtEdicion> dtediciones = cur.mostrarEdiciones();
		DtEdicion[] ediciones=new DtEdicion[dtediciones.size()];
		if(!dtediciones.isEmpty()) {
			int i=0;
			 for(DtEdicion dte:dtediciones) {
				 ediciones[i]=dte;
				 i++;
			 }
		}
		
		return ediciones;
	}

	@Override
	public String[] listarCategoriasPF(String programa) {
		ManejadorPrograma mP = ManejadorPrograma.getInstancia();
		String[] retorno = mP.find(programa).getCategorias();
		return retorno;
	}
	
	public String institutoDeCurso(String nombreCurso) {
		String insti = null;
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		List<Instituto> institutos= mi.getInstitutos();
		for(Instituto i: institutos) {
			if(i.tieneEsteCurso(nombreCurso)) {
				insti=i.getNombre();
			}
		}
		return insti;
	}

}



