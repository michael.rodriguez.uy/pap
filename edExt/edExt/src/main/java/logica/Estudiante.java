package logica;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("Estudiante")
public class Estudiante extends Usuario {
	@OneToMany(mappedBy = "inscripto",cascade = CascadeType.ALL, orphanRemoval=true)
	private List<InscripcionPF> inscripcionesPF = new ArrayList<>();

	public Estudiante() {
		super();
	}
	
	public Estudiante(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento, Boolean esDocente, String password, String imagen) {
		super(nickName, nombre, apellido, correo, fechaNacimiento, esDocente, password, imagen);
	}
	
	public List<InscripcionPF> getInscripcionesPF() {
		return inscripcionesPF;
	}

	public void setInscripcionesPF(List<InscripcionPF> inscripcionesPF) {
		this.inscripcionesPF = inscripcionesPF;
	}


}
