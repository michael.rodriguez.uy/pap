package logica;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import datatypes.DtCurso;
import datatypes.DtDocente;
import persistencia.Conexion;

public class ManejadorInstitutos {
	private static ManejadorInstitutos instancia 	= null;	
	//private List<Instituto> institutos 				= new ArrayList<Instituto>();
	
	public ManejadorInstitutos(){}
	
	public static ManejadorInstitutos getInstancia() {
		if (instancia == null)
			instancia = new ManejadorInstitutos();
		return instancia;
	}
	
/*	public void agregarInstituto(String nombre) {
		Instituto i = new Instituto();
		i.setNombre(nombre);		
		institutos.add(i);

	}
*/	 
	public List<Instituto> getInstitutos() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();		
		Query query = em.createQuery("select i from Instituto i");
		List<Instituto> listInstituto = (List<Instituto>) query.getResultList();
		return listInstituto;
	}

	public void setInstitutos(List<Instituto> institutos) {
		//this.institutos = institutos;
	}

	public void agregarCurso(String nombre, String descripcion, String duracion, int cantidadHoras, int cantidadCreditos, String url,Date fechaAlta,
			List<Edicion> ediciones, Instituto instituto) {
		
		Curso c = new Curso();
		c.setNombre(nombre);
		c.setDescripcion(descripcion);
		c.setDuracion(duracion);
		c.setCantidadHoras(cantidadHoras);
		c.setCantidadCreditos(cantidadCreditos);	
		c.setUrl(url);
		c.setFechaAlta(fechaAlta);
		c.setEdiciones(ediciones);
		
		// a este instituto que le paso por parametro le agrego el curso.
		instituto.add(c);					
		
		System.out.println("Instituto: " + instituto);
		System.out.println("Lista Cursos");		
		List<Curso> cursos = instituto.getCursos();
		for(Curso c1: cursos) {
			System.out.println(c1.getNombre());
		} 
				
	}
	
	
	public Instituto buscarInstituto(String nombre) {

		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		Instituto instituto = em.find(Instituto.class, nombre);
		return instituto;
	}
	
	
	// devuleve todos los nombres de los institutos cargados.		
	public List<String> listarInstituto() {	
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();		
		Query query = em.createQuery("select i from Instituto i");
		List<Instituto> listInstituto = (List<Instituto>) query.getResultList();
		List<String> salida = new ArrayList<String>();
		for(Instituto i: listInstituto) {
			salida.add(i.getNombre());
		} 
		return salida;
	}
	
	public boolean exists (String nombre) {
		boolean retorno=false;
		if(this.getInstituto(nombre)==null) {
			retorno=false;
		}else {
			retorno=true;			
		}
		/*for(Instituto i: institutos) {
			if(i.getNombre().equals(nombre)){
				retorno= true;
			}}
		}*/
		
		return retorno;
	}
	public void add(Instituto i) {
		//institutos.add(i);
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		
		em.persist(i);
		//System.out.print("ACAAAAAAAAAAAAAAAAAA");
		  
		em.getTransaction().commit();
	}
	
	public Instituto getInstituto(String nombreIns) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();		
		Instituto in = em.find(Instituto.class, nombreIns);
		return in;
	}
	
	public Curso getCruso(String nombreCurso) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();		
		Curso c = em.find(Curso.class, nombreCurso);
		return c;
	}
			
	public DtCurso getNombreCurso(Instituto i, String nombreCurso) {
		List<Curso> cursos = i.getCursos();		
		for(Curso c: cursos) {
			if(c.getNombre().equals(nombreCurso)) {
				return c.getDtCurso();
			}			
		}
		return null;
	}
	
	
	
	public boolean altaEdicionCurso(Instituto i, String nombreCurso, String nombreEdicion, Calendar fechaI, Calendar fechaF, int cupo, Calendar fechaPub,
			List<Docente> listaDocentes, String imagen) {
		System.out.println("Manejador listaDocentes:" + listaDocentes.size());
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		// Busco el curso pasado por parametro para la instancia instituto i
		//Curso c = i.obtenerCurso(nombreCurso);
		Curso c = em.find(Curso.class, nombreCurso);
		// si la edicion ya existe devuelvo false y me voy.
		
		//if (c.existeEdicion(nombreEdicion)) {
		if (em.find(Edicion.class, nombreEdicion) != null) {
			return false;
		} else {
			// Doy de alta la edicion:
			List<Docente> auxDoc = obtenerDocentes(listaDocentes, em);
			Edicion e = new Edicion(nombreEdicion, fechaI, fechaF, cupo, fechaPub, auxDoc, imagen);
			for(Docente d : listaDocentes) {
				d.getEdicionesInscripto().add(e);
			}
			c.addEdicion(e);
			List<Docente> doc = e.getListaDocente();
			System.out.println("Manejador listaDocentes en memoria:" + doc.size());
			em.getTransaction().begin();
			//em.persist(e);
			em.persist(c);
			em.getTransaction().commit();
			// salgo con true
			return true;
				
		}
	}
	
	private List<Docente> obtenerDocentes(List<Docente> listaDocente, EntityManager em){
		List<Docente> aux = new ArrayList<Docente>();
		for(Docente d : listaDocente) {
			aux.add(em.find(Docente.class, d.getNickName()));
		}
		return aux;
	}
 
	public List<String> getCursos() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();		
		Query query = em.createQuery("select c from Curso c");
		List<Curso> listCursos = (List<Curso>) query.getResultList();
		//return listCursos;
		List<String> salida = new ArrayList<String>();
		for(Curso c: listCursos) {
			salida.add(c.getNombre());
		} 
		return salida;
	}
	
	public Curso getCurso(String nomCurso) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Curso c = em.find(Curso.class, nomCurso);
		return c;
	}
	
	public boolean existeProgramDeFormacion(String nombre) {
		boolean aretornar = false;
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Programa p = em.find(Programa.class, nombre);
		
		if (nombre.equals(p.getNombre())) {
			aretornar = true;
		}
		return aretornar;
	}

	public List<Programa> getProgramas() {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();		
		Query query = em.createQuery("select p from Instituto p");
		List<Programa> listProgramas = (List<Programa>) query.getResultList();
		return listProgramas;
	}

	
}
