package logica;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.RollbackException;

import datatypes.DtDocente;
import datatypes.DtEdicion;
import datatypes.DtInscripcionEdc;
import datatypes.DtUsuario;
import excepciones.ClaveInvalidaException;
import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;
import excepciones.SinInscripcionesEdc;
import excepciones.UsuarioNoExisteException;
import interfaces.IControladorUsuario;
import persistencia.Conexion;

@SuppressWarnings("unused")
public class ControladorUsuario implements IControladorUsuario {

	private static EntityManagerFactory emf;
	private static EntityManager em;
	
	public ControladorUsuario() {
		super();
	}


	//@SuppressWarnings("unused")
	@Override
	public void altaUsuarioEstudiante(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento, Boolean esDocente, String password, String imagen) throws NickNameExistenteException, EmailExistenteException {
		
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario nick = mU.existeUsuario(nickName);
		String email = mU.existeEmail(correo);
		if(nick != null)
			 throw new NickNameExistenteException("El nickname " + nickName + " ya esta registrado");
		if(email != null)
			throw new EmailExistenteException("El email " + correo + " ya esta registrado");
		
		Usuario us;
		if(nick == null && email == null) {
				us = new Estudiante(nickName, nombre, apellido, correo, fechaNacimiento, esDocente, password, imagen);
				try {
					mU.add(us);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	}
	
	
	public void altaUsuarioDocente(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento,	Boolean esDocente,  String password, String imagen, String nombreInstituto) throws NickNameExistenteException, EmailExistenteException {
		
	
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		String nick = mU.existeUsuarioString(nickName);
		String email = mU.existeEmail(correo);
		if(nick != null)
			 throw new NickNameExistenteException("El nickname " + nickName + " ya esta registrado");
		if(email != null)
			throw new EmailExistenteException("El email " + correo + "ya esta registrado");
		
		Usuario us;
		if(nick == null && email == null) {
			us = new Docente(nickName, nombre, apellido, correo, fechaNacimiento, esDocente, password, imagen, nombreInstituto);
				try {
					mU.add(us);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		
		}
	}
	// Devuelve solo los NICK de los docentes
	public List<String> listarDocentes() {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();		
		return mU.getDocentes();
	}
	public List<String> listarEstudiantes() {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();		
		return mU.getEstudiantes();
	}	
	// devuelve el docente cuyo NICK es pasado por parametro 
	public Docente seleccionarDocente(String docente) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		return mU.getUnDocente(docente);
	}
	
	public DtDocente seleccionarDtDocente(String docente) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		DtDocente dtd= mU.obtenerDocente(docente);
		return dtd;
	}
	
	public Usuario seleccionarUsuario(String nick) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		Usuario u = null;
		u = mU.obtenerUsuario(nick);
		return u;
	}
	
	// funcion para WS
	public DtUsuario seleccionarDtUsuario(String nick) {
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();		
		DtUsuario dtUsuario = mU.obtenerUsuario(nick).getDtUsuario();		
		return dtUsuario;
	}

	
	@Override
	public String[] listarUsuarios() {
		//Inicializo un ArrayList de tipo String.
		ArrayList<String> usuarios;
		//Obtengo una instancia de Manejador.
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		//Obtengo la lista de Usuarios.
		usuarios = mU.obtenerUsuarios();
		
		String[] usuarios_ret = new String[usuarios.size()];
		int i = 0;
		for(String u:usuarios) {
			usuarios_ret[i] = u;
			i++;
		}
		return usuarios_ret;
	}
	
	public String existeEmailUsuario(String email) {
		String aretornarE = null;
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		ManejadorUsuario mu=ManejadorUsuario.getInstancia();
		Usuario usuario = em.find(Usuario.class, email);
		
		if (usuario != null) {
				aretornarE=usuario.getCorreo();
			}
		return aretornarE;
	}
	
	
	
	//obtiene lista de nombre de usuarios
	public ArrayList<String> obtenerUsuarios(){							
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
			
		Query query = em.createQuery("select u from usr u");
		List<Usuario> listUsuario = (List<Usuario>)	query.getResultList();
		
		ArrayList<String> aRetornar = new ArrayList();
		for(Usuario u: listUsuario) {
			aRetornar.add(u.getNickName());
		}
		return aRetornar;
	}
	
	
	public Instituto seleccionarUnInstituto(String nomInstituto) {
		ManejadorInstitutos mi = ManejadorInstitutos.getInstancia();
		Instituto ins = mi.buscarInstituto(nomInstituto);
		return ins;
	}
	
	public List<Edicion> buscarEdiciones(Instituto i){
		List<Edicion> edicionFinal = null;
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		
		List<Curso> cursos = i.getCursos();
		
		for(Curso c: cursos) {
			List<Edicion> ediciones = c.getEdiciones();
			for(Edicion e: ediciones) {
				edicionFinal.add(e);
			}
		}
		return edicionFinal;
	}
	
	public boolean modificarDatos(String nickName, String nombre, String apellido, String Correo, Calendar fechaNac) {
		//ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		//Usuario u = mU.obtenerUsuario(nickName);
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		 
		Usuario usu= (Usuario)em.find(Usuario.class ,nickName);
		
		em.getTransaction().begin();

		usu.setNombre(nombre);
		usu.setApellido(apellido);
		usu.setFechaNacimiento(fechaNac);
		
		em.getTransaction().commit();
		
		return true;
	}

	public String[] seleccionarInscripcionED(String nickName){
		ManejadorUsuario mU = ManejadorUsuario.getInstancia();
		List<String> ediciones;
		ediciones = mU.obtenerEdicionesDeEstudiante(nickName);
		int i = 0;
		String[] miarray = new String[ediciones.size()];
		if(ediciones != null) {
			for(String e: ediciones) {
				miarray[i] = e;
				i++;
			}
		}
		return miarray;
	}
	
	//Verifica que sea profesor y est� en el sistema con su contrase�a
	public String ingresarProfe(String nom , String pass) throws UsuarioNoExisteException, ClaveInvalidaException{ 	
		String retorno = "problemaIngresProfe";
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		 
		Usuario u = (Usuario)em.find(Usuario.class ,nom);
		if(u==null || !u.getEsDocente()) {
			throw new UsuarioNoExisteException("El docente no se encuentra");
		}else if(!u.getPassword().equals(pass)) {
			throw new ClaveInvalidaException("Contrase�a incorrecta");
		}else {
			if (u instanceof Docente) {
				retorno=((Docente) u).getNombreInsituto();
			}else {
				retorno="noDocente";
			}
			/// Quzas ac� toque recordar el usuario
		}
		return retorno;
			
		
	}


	public void seguirUsuario(String seguidor, String seguido) throws Exception {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
	    Usuario uSeguidor = em.find(Usuario.class, seguidor);
	    if (uSeguidor == null) {
	      em.close();
	      throw new Exception("No existe el usuario " + seguidor); // si no existe el seguidor en la lista
	    } else {
	      Usuario uSeguido = em.find(Usuario.class, seguido);
	      if (uSeguido == null) {
	    	  em.close();
	    	  throw new Exception("No existe el usuario " + seguido); //si no exite el seguido en la lista
	      } else {
	        uSeguido.seguir(uSeguidor);
	        try {
	          em.getTransaction().begin();
	          em.persist(uSeguido);
	          //em.persist(uSeguidor);
	          em.getTransaction().commit();
	        } catch (Exception exc) {
	          if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
	            em.getTransaction().rollback();
	            em.close();
	          }
	          throw new Exception("ERROR!");
	        } //finally {
	          //em.close();
	        //}
	      }
	    }
	  }
	
	public String[] usuariosSeguidos(String seguidor)  {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		Usuario uSeguidor = em.find(Usuario.class, seguidor);
		ArrayList<String> resultado = new ArrayList<String>();
		ArrayList<String> usuarios = obtenerUsuarios();
		for(String u: usuarios) {
			if(!u.equals(seguidor)) {
				if(checkSeguidor(seguidor, u)) {
					resultado.add(u);
				}
			}
		}
		String[] usuarios_ret = new String[resultado.size()];
		int i = 0;
		for(String u: resultado) {
			usuarios_ret[i] = u;
			i++;
		}
		
		return usuarios_ret;
	}
	
	public void dejarSeguirUsuario(String seguidor, String seguido) throws Exception {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
	    Usuario uSeguidor = em.find(Usuario.class, seguidor);
	    if (uSeguidor == null) {
	      em.close();
	      throw new Exception("No existe el usuario " + seguidor);//si no existe el seguidor en la lista
	    } else {
	      Usuario uSeguido = em.find(Usuario.class, seguido);
	      if (uSeguido == null) {
	        em.close();
	        throw new Exception("No existe el usuario " + seguido); // ssi no exite el seguido en la lista
	      } else {
	        uSeguido.dejarDeSeguir(uSeguidor);
	        try {
	          em.getTransaction().begin();
	          em.persist(uSeguido);
	          //em.persist(uSeguidor);
	          em.getTransaction().commit();
	        } catch (Exception exc) {
	          if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
	            em.getTransaction().rollback();
	            em.close();
	          }
	          throw new Exception("ERROR!");
	        } //finally {
	          //em.close();
	        //}
	      }
	    }
	  }
	


	  public Boolean loginNickname(String nickname, String password) {
		  Conexion conexion = Conexion.getInstancia();
		  EntityManager em = conexion.getEntityManager();
		  Usuario usuario = em.find(Usuario.class, nickname);
		  boolean ok = false;
		  if(usuario!=null && usuario.getPassword().equals(password)) {
			  ok=true;
		  }else {
			  if(usuario == null) {
				  ok=loginEmail(nickname,password);
			  }
		  }

		return ok;
	  }
	  
	  /*public Boolean loginMix(String nickname, String password) {
		  Conexion conexion = Conexion.getInstancia();
		  EntityManager em = conexion.getEntityManager();
		  Usuario usuario = em.find(Usuario.class, nickname);
		  boolean ok = false;
		  if(usuario!=null && usuario.getPassword().equals(password)) {
			  ok=true;
		  }else {
			  if(usuario == null) {
				  ok=loginEmail(nickname,password);
			  }
		  }

		return ok;
	  }*/
	  
	  @SuppressWarnings("unchecked")
	public Boolean loginEmail(String email, String password) {
		  boolean ok=false;
		  Conexion conexion = Conexion.getInstancia();
		  EntityManager em = conexion.getEntityManager();
		  String nick = emailToNick(email);
		  if(nick != null) {
			  Usuario u= em.find(Usuario.class, nick);
			  if(u.getCorreo().equals(email) && u.getPassword().equals(password)) {
				  ok=true;
		  }
		  
		  }
		  return ok;
	  }
	  

	  
		public String emailToNick(String email) {
			  
			  Conexion conexion = Conexion.getInstancia();
			  EntityManager em = conexion.getEntityManager();
			  Query query = em.createQuery("select u.nickName from usr u where u.correo=?0 ");
			  query.setParameter(0, email);
			  List<String> resultado=query.getResultList();
			  String retorno=null;
			  if( resultado.size()>0) {
				  retorno = query.getSingleResult().toString();
			  }
			  
			  return retorno;
		  }
	  
	  
	  public ArrayList<String> ConsultarEstadoInscripciones(String nickdelusuario)throws SinInscripcionesEdc{
		  // Deuvelve strings de forma : "nombre_edicion estado_inscripcion" separadas por un espacio solamente
		  	ManejadorUsuario mi = ManejadorUsuario.getInstancia();
		  	ArrayList<String> retorno= new ArrayList<String>();
		  	Usuario usuariobjetivo = mi.getUnEstudiante(nickdelusuario);
		  	List<InscripcionEdc> inscripcionesobjetivo = usuariobjetivo.getInscripciones();
		  	if(!inscripcionesobjetivo.isEmpty()) {
		  		for(InscripcionEdc iterador: inscripcionesobjetivo) {
		  			String aux=iterador.getEdicion().getNombre()+" "+iterador.getEstado();
		  			retorno.add(aux);
		  		}
		  	}else { 
		  		throw new SinInscripcionesEdc("Sin Inscripciones");
		  		}
		  	return retorno;
	  }
	  
	  
    public String rutaAnombreDeImagen(String cadena_b) {
		    	String ruta=System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images/";
		    	int largo= ruta.length();
		    	String salida = cadena_b.substring(largo);
		    return salida;
		    }


	@Override
	public List<DtInscripcionEdc> revisarInscripciones(String nickestudiante) {
		ManejadorUsuario mu = ManejadorUsuario.getInstancia();
		Usuario objetivo = mu.getUnEstudiante(nickestudiante);
		
		return objetivo.getInscripcionesDt();
	}

	public boolean checkSeguidor(String nickSeguidor, String nickSeguido) {
		boolean esta = false;
		ManejadorUsuario mu = ManejadorUsuario.getInstancia();
		Usuario u = mu.obtenerUsuario(nickSeguidor);
		List<Usuario> seguidos = u.getUsuariosSeguidos();
		System.out.println(seguidos);
		if(seguidos != null) {
			for(Usuario usu: seguidos) {
				if(nickSeguido.equals(usu.getNickName())) {
					esta = true;
				}
			}
		}
		
		if(esta) {
			return true;
		}
		return false;
	}
}

