package logica;

import javax.persistence.Query;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import persistencia.Conexion;

public class ManejadorCategoria {
	private static ManejadorCategoria instancia = null;
	private ManejadorCategoria() {}
	
	public static ManejadorCategoria getInstancia() {
		if (instancia == null)
			instancia = new ManejadorCategoria();
		return instancia;
	}

	public void agregarCategoria(Categoria objetivo) {
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		em.getTransaction().begin();
		
		em.persist(objetivo);
		
		em.getTransaction().commit();
	}
	
	public Categoria buscarCategoria(String id) { // da error
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
		
		Categoria catobjetivo = em.find(Categoria.class, id);
		return catobjetivo;
	}
	public boolean existeCategoria(String id) {
	boolean existe=false;
	ArrayList<String> categoriastr=this.obtenerCategorias();
	for(String s:categoriastr) {
		if(s.equals(id)) existe=true;
	}
		return existe;
	}
	public ArrayList<String> obtenerCategorias(){
		Conexion conexion = Conexion.getInstancia();
		EntityManager em = conexion.getEntityManager();
	
		Query query = (Query) em.createQuery("select c from cat c");
		
		@SuppressWarnings("unchecked")
		List<Categoria> listadeCategorias = (List<Categoria>) (query).getResultList();
		
		ArrayList<String> aRetornar = new ArrayList<>();
		for(Categoria c: listadeCategorias) {
			aRetornar.add(new String(c.getNombre()));
		}
		return aRetornar;
	}
	

}
