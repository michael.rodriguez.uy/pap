package logica;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import datatypes.DtInscripcionEdc;
import datatypes.EstadoInscripcionEdc;
import persistencia.InscripcionEdcID;
@Entity
@IdClass(InscripcionEdcID.class)
public class InscripcionEdc {
	@Id 
	@ManyToOne
	private Edicion edicion;
	
	@Id
	@ManyToOne
	private Usuario usuario;
	
	@Temporal(TemporalType.DATE)
	private Date fecha;
	
	private EstadoInscripcionEdc estado;
	
	//private Integer contadorRechazos; 
	
	public Edicion getEdicion() {
		return edicion;
	}
	public void setEdicion(Edicion edicion) {
		this.edicion = edicion;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public InscripcionEdc() {
		super();
	}
	public InscripcionEdc(Date fecha, Usuario usuario, Edicion edicion) {
		super();
		this.fecha = fecha;
		this.usuario = usuario;
		this.edicion = edicion;
		this.estado=EstadoInscripcionEdc.Inscripto;
	}
	public EstadoInscripcionEdc getEstado() {
		return estado;
	}
	public void setEstado(EstadoInscripcionEdc estado) {
		this.estado = estado;
	}

	public DtInscripcionEdc getDtInscripcionEdc() {
		return new DtInscripcionEdc(this.edicion.getDtEdicion(),this.usuario.getDtUsuario(),this.fecha,this.estado);
	}
}
