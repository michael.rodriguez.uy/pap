package publicadores;


import configuraciones.WebServiceConfiguracion;
import datatypes.DtCurso;
import datatypes.DtDocente;
import datatypes.DtEdicion;
import datatypes.DtInscripcionEdc;
import datatypes.DtPrograma;
import datatypes.DtUsuario;
import excepciones.EmailExistenteException;
import excepciones.InscripcionEdicionRepetida;
import excepciones.NickNameExistenteException;
import excepciones.ProgramaExisteException;
import excepciones.SinEdicionException;
import excepciones.UsuarioNoExisteException;
import interfaces.Fabrica;
import interfaces.IControladorInstProg;
//import javax.xml.ws.WebServiceFeature;
import interfaces.IControladorUsuario;
import logica.Docente;
import logica.Instituto;
import logica.ManejadorInstitutos;
import logica.Programa;
import logica.Usuario;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class ControladorPublish {
	private Fabrica fabrica;
	private IControladorInstProg iCip;
	private IControladorUsuario iConUs;
	private WebServiceConfiguracion configuracion;
	private Endpoint endpoint; 

	public ControladorPublish() {
		fabrica = Fabrica.getInstancia();
		iCip = fabrica.getIControladorIP();
		iConUs = fabrica.getIControladorU();
		try {
			configuracion = new WebServiceConfiguracion();
		} catch (Exception ex) {
			System.out.println("Excepcion Webservice");
		}
	}
	
	@WebMethod(exclude = true)
	public void publicar() {
		endpoint = Endpoint.publish("http://" + configuracion.getConfigOf("#WS_IP") + ":" + configuracion.getConfigOf("#WS_PORT") + "/controlador", this);
		System.out.println("http://" + configuracion.getConfigOf("#WS_IP") + ":" + configuracion.getConfigOf("#WS_PORT") + "/controlador");
	}
	
	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
        return endpoint;
	}
	//LOS M�TODOS QUE VAMOS A PUBLICAR

	
	@WebMethod
	public Boolean loginNickname(String nickname, String password) throws UsuarioNoExisteException{
	return iConUs.loginNickname(nickname, password);
	}
	
	@WebMethod
	public String[] listarInstitutosArray() {
		return iCip.listarInstitutosArray();
	}
	
	@WebMethod
	//del otro lado parece que no hay nada, la dejo x si alguien la hace andar
	public  ArrayList<String> listarCategoriasList(){ 
		return iCip.listarCategorias();
	}
	
	@WebMethod
	// es la de arriba pero con array
	public  String[] listarCategorias(){ 
		//return iCip.listarCategorias();
		List<String> salida = iCip.listarCategorias();
	       if (salida.isEmpty())
	            return null;
	        else {
	            Object[] o = salida.toArray();
	            String[] miarray = new String[o.length];
	            for (int i = 0; i < o.length; i++) {
	            	miarray[i] = (String) o[i];
	            }	            
	            return miarray;
	        }
	}	
	
	@WebMethod
	public String[] listarDocentes(){
		List<String> salida= iConUs.listarDocentes();
	       if (salida.isEmpty())
	            return null;
	        else {
	            Object[] o = salida.toArray();
	            String[] miarray = new String[o.length];
	            for (int i = 0; i < o.length; i++) {
	            	miarray[i] = (String) o[i];
	            }	            
	            return miarray;
	        }
	}
	
	@WebMethod
	public String[] listarProgramas() {
		return iCip.listarProgramas();
	}
	
	@WebMethod
	public DtUsuario seleccionarDtUsuario(String nick) {
		return iConUs.seleccionarDtUsuario(nick);
	}
	
	@WebMethod
	public String rutaAnombreDeImagen(String cadena_b) {
		return iConUs.rutaAnombreDeImagen(cadena_b);
	}
	
	@WebMethod
	public Boolean loginEmail(String email, String password) {
		return iConUs.loginEmail(email, password);
	}
	
	@WebMethod
	public String emailToNick(String email) {
		return iConUs.emailToNick(email);
	}
	
	@WebMethod
	public String[] listarNombreCursos(String nombreInst) {
		System.out.println("listarNombreCursos en Publish");
		return iCip.listarNombreCursos(nombreInst);
	}
	
	@WebMethod
	public DtCurso mostrarDatosdeCurso(String nombreCurso) {
		DtCurso dtc= iCip.mostrarDatosdeCurso(nombreCurso);
		 return dtc;
	}
	
	@WebMethod
	public  DtEdicion[] mostrarDtEdicionesArray(String nombreCurso) {
		return iCip.mostrarDtEdicionesArray(nombreCurso);
	}
	
	@WebMethod
	public String institutoDeCurso(String nombreCurso) {
		return iCip.institutoDeCurso(nombreCurso);
	}
	
	@WebMethod
	public DtEdicion traerUltimaEdicion(String curso, String instituto) throws SinEdicionException{
		return iCip.traerUltimaEdicion(curso, instituto);
	}
	
	@WebMethod
	public DtInscripcionEdc[] obtenerInscripciones(String curso, String instituto,String edicion, boolean prioridad) {
		return iCip.obtenerInscripciones(curso, instituto, edicion, prioridad);
	}
	
	@WebMethod
	public String[] listarEdicionesdeCurso(String nombreCurso) {
		return iCip.listarEdicionesdeCurso(nombreCurso);
	}
	
	@WebMethod 
	public DtEdicion mostrarEdicion(String nombreEdicion) {		
		return iCip.mostrarEdicion(nombreEdicion);
	}
	
	@WebMethod
	public  DtInscripcionEdc[] inscrpcionesAceptadas(String nomInstituto, String nombreCurso, String nomEdicion){
		ArrayList<DtInscripcionEdc> salida= iCip.inscrpcionesAceptadas(nomInstituto, nombreCurso, nomEdicion);
	       if (salida.isEmpty()) {
	    	   DtInscripcionEdc[] salidaNull= new DtInscripcionEdc[0];
	    	   return salidaNull;
	       }else {
	            Object[] o = salida.toArray();
	            DtInscripcionEdc[] miarray = new DtInscripcionEdc[o.length];
	            for (int i = 0; i < o.length; i++) {
	            	miarray[i] = (DtInscripcionEdc) o[i];
	            }	            
            return miarray;
	        }
	}
	@WebMethod
	public void altaInscripcionEdc(String nombreEdicion,String nickEstudiante) throws InscripcionEdicionRepetida {
		iCip.altaInscripcionEdc( nombreEdicion, nickEstudiante);
	}
	
	@WebMethod
	public DtPrograma[] programasdeCurso(String nomCurso) {
		return iCip.programasdeCurso(nomCurso);
	}
	
	@WebMethod
	public DtCurso[] cursosdeCategoria(String categoria) {
		return iCip.cursosdeCategoria(categoria);
	}
	
	@WebMethod
	public void setearInscripciones(String instituto, String curso, String edicion, DtInscripcionEdc[] listaAceptados) {
		//ArrayList<DtInscripcionEdc> lista = (ArrayList<DtInscripcionEdc>) Arrays.asList(listaAceptados);
		ArrayList<DtInscripcionEdc> lista = new ArrayList<>();
		for(DtInscripcionEdc dted:listaAceptados) {
			lista.add(dted);
		}
		iCip.setearInscripciones(instituto, curso, edicion, lista);
	}
	
	@WebMethod
	public void seleccionarInstituto(String nombreInst) {
		//System.out.println("En ContPublish Instimod "+nombreInst);
		iCip.seleccionarInstituto(nombreInst);
	}
	@WebMethod
	public void seleccionarCursoInstancia(String nombreCurso) {
		iCip.seleccionarCursoInstancia(nombreCurso);
	}
	
	@WebMethod
	public boolean nuevoCurso(String nombreCurso ) {
		//System.out.println("En ContPublish nuevoCurso "+nombreCurso);
		return iCip.nuevoCurso(nombreCurso);
	}
	
	@WebMethod
	public void setearCurso(String nombreCurso, String desc, String duracion, int cantHoras, int cantCreditos, String url, Calendar fechaAlta, String foto) {
		//System.out.println("En ContPublish StCur "+nombreCurso);
		iCip.setearCurso(nombreCurso, desc, duracion, cantHoras, cantCreditos, url, fechaAlta.getTime(), foto);
	}
	
	@WebMethod
	public void  seleccionarPrevias(String[] previas){ //(List<String> previas)
		ArrayList<String> prev=new ArrayList<>();
		for(String p:previas) {
			prev.add(p);
		}		
		iCip.seleccionarPrevias(prev);
	}
	
	@WebMethod
	//public void setearCategorias(ArrayList<String> categorias)
	public void setearCategorias(String[] categorias) {
		ArrayList<String> cats=new ArrayList<>();
		for(String c:categorias) {
			cats.add(c);
		}
		iCip.setearCategorias(cats);
	}
	
	@WebMethod
	public void confirmarAltaCurso() {
		//System.out.println("confirmarAltaCurso ");
		iCip.confirmarAltaCurso();
	}

	@WebMethod
	public Usuario seleccionarUsuario(String nick) {
		Usuario aux = iConUs.seleccionarUsuario(nick);
		//DtUsuario dtretorno= aux.getDtUsuario();
		return aux;
	}
	
	@WebMethod
	public void pruebaDate(Date d) {
		System.out.println(d);
	}
	
	@WebMethod
	public void altaUsuarioEstudiante(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento, Boolean esDocente, String password, String imagen) throws NickNameExistenteException, EmailExistenteException {
		iConUs.altaUsuarioEstudiante(nickName, nombre, apellido, correo, fechaNacimiento, esDocente, password, imagen);
	}
	
	@WebMethod
	public void altaUsuarioDocente(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento,	Boolean esDocente,  String password, String imagen, String nombreInstituto) throws NickNameExistenteException, EmailExistenteException {
		iConUs.altaUsuarioDocente(nickName, nombre, apellido, correo, fechaNacimiento, esDocente, password, imagen, nombreInstituto);
	}
	
	@WebMethod
	public Boolean altaEdicionCurso(String nombreInstituto, String nombreCurso, String nombreEdicion, Calendar fechaI, Calendar fechaF, int cupo, Calendar fechaPub, Docente[] listaDocentes, String imagen) {
		ArrayList<Docente> listaDoc = new ArrayList<>();
		for(Docente doc:listaDocentes) {
			listaDoc.add(doc);
		}
		return iCip.altaEdicionCurso(nombreInstituto, nombreCurso, nombreEdicion, fechaI, fechaF, cupo, fechaPub, listaDoc, imagen);
		
	}
	
	@WebMethod
	public DtDocente seleccionarDtDocente(String docente75) {
		return iConUs.seleccionarDtDocente(docente75);
	}
	
	@WebMethod
	public String[] listarUsuarios() {
		return iConUs.listarUsuarios();
	}
	

	/*
	@WebMethod
	public DtInstituto seleccionarUnInstituto(String nomInstituto) {
		return iConUs.seleccionarUnDtInstituto(nomInstituto);
	}*/
	
	@WebMethod
	public String[] seleccionarInscripcionED(String NickName) {
		return iConUs.seleccionarInscripcionED(NickName);
	}
	
	@WebMethod
	public boolean modificarDatos(String nickName, String nombre, String apellido, String Correo, Calendar fechaNac) {
		return iConUs.modificarDatos(nickName, nombre, apellido, Correo, fechaNac);
	}
	
	@WebMethod
	public void dejarSeguirUsuario(String seguidor, String seguido)throws Exception {
		iConUs.dejarSeguirUsuario(seguidor, seguido);
	}
	
	@WebMethod
	public void seguirUsuario(String seguidor, String seguido) throws Exception{
		iConUs.seguirUsuario(seguidor, seguido);
	}
	
	@WebMethod
	public boolean checkSeguidor(String nickSeguidor, String nickSeguido) {
		return iConUs.checkSeguidor(nickSeguidor, nickSeguido);
	}
	
	@WebMethod
	public String[] usuariosSeguidos(String seguidor) throws IOException{
		return iConUs.usuariosSeguidos(seguidor);
	}
	
	@WebMethod
	public void altaPrograma(String nombrePrograma, String descripcion, Calendar fechaI, Calendar fechaF, String foto) throws IOException{
		try {
			iCip.altaPrograma(nombrePrograma, descripcion, fechaI, fechaF, foto);
		} catch (ProgramaExisteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@WebMethod
	public DtPrograma verPrograma(String nombrePrograma) {
		return iCip.verPrograma(nombrePrograma);
	}
	
	@WebMethod
	public String[] cursosDePrograma() {
		return iCip.cursosDePrograma();
	}
	
	@WebMethod
	public String[] listarCategoriasPF(String programa) {
		return iCip.listarCategoriasPF(programa);
	}
	
	@WebMethod
	public DtCurso seleccionarCurso(String nombreCurso) {
		return iCip.seleccionarCurso(nombreCurso);
	}
}
