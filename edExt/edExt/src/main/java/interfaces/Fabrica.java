package interfaces;

import logica.ControladorInstProg;
import logica.ControladorUsuario;

public class Fabrica {
	private static Fabrica instancia = null;
	
	private Fabrica(){}
	
	public static Fabrica getInstancia() {
		if (instancia == null)
			instancia = new Fabrica();
		return instancia;
	}
	
	public IControladorInstProg getIControladorIP() {
		return new ControladorInstProg();
	}
	
	public IControladorUsuario getIControladorU() {
		return new ControladorUsuario();
	}
}
