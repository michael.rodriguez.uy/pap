package interfaces;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import datatypes.DtDocente;
import datatypes.DtInscripcionEdc;
import datatypes.DtUsuario;
import excepciones.ClaveInvalidaException;
import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;
import excepciones.UsuarioNoExisteException;
import logica.Docente;
import logica.Edicion;
import logica.Instituto;
import logica.Usuario;

@SuppressWarnings("unused")
public interface IControladorUsuario{
	public void altaUsuarioDocente(String nickName, String nombre, String apellido, String correo, Calendar fechaN, Boolean esDocente,  String password, String imagen, String nombreInstituto) throws NickNameExistenteException, EmailExistenteException;
	public void altaUsuarioEstudiante(String nickName, String nombre, String apellido, String correo, Calendar fechaNacimiento, Boolean esDocente,  String password, String imagen) throws NickNameExistenteException, EmailExistenteException;
	public List<String>	listarEstudiantes();
	public List<String> listarDocentes();
	//	public Usuario existeUsuario(String nickName);
	//	public String existeEmail(String email);
	public Docente seleccionarDocente(String docente);
	public String[] listarUsuarios();
	public Usuario seleccionarUsuario(String nick);
	public DtUsuario seleccionarDtUsuario(String nick);
	
	public Instituto seleccionarUnInstituto(String nomInstituto);
	
	public List<Edicion> buscarEdiciones(Instituto i);
	public boolean modificarDatos(String nickName, String nombre, String apellido, String Correo, Calendar fechaNac);
	public String[] seleccionarInscripcionED(String NickName);
	
	public String ingresarProfe(String nom , String pass) throws UsuarioNoExisteException, ClaveInvalidaException;
	public void seguirUsuario(String seguidor, String seguido) throws Exception;
	public void dejarSeguirUsuario(String seguidor, String seguido) throws Exception;
	
	public Boolean loginNickname(String nickname, String password) throws UsuarioNoExisteException;
	public Boolean loginEmail(String email, String password); 
	public String emailToNick(String email);
	public String rutaAnombreDeImagen(String cadena_b);
	public List<DtInscripcionEdc> revisarInscripciones(String nickestudiante);
	public String existeEmailUsuario(String email);
	public boolean checkSeguidor(String nickSeguidor, String nickSeguido);
	public DtDocente seleccionarDtDocente(String docente);
	public String[] usuariosSeguidos(String seguidor);

}


 
