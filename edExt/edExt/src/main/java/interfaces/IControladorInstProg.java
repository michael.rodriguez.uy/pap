package interfaces;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import datatypes.DtCurso;
import datatypes.DtEdicion;
import datatypes.DtInscripcionEdc;
import datatypes.DtPrograma;
import excepciones.CategoriaRepetidaException;
import excepciones.CursoDeProgramaRepetido;
import excepciones.InscripcionEdicionRepetida;
import excepciones.ProgramaExisteException;
import excepciones.SinEdicionException;
import logica.Curso;
import logica.Docente;
import logica.Edicion;
import logica.Instituto;
import logica.Programa;
import logica.Usuario;


@SuppressWarnings("unused")
public interface IControladorInstProg {
	//Alta Instituto
	public boolean ingresarNomInst(String nom);
	public void CancelarAltaInstituto();
	//Alta Curso
	public String[] listarInstitutosArray();
	public void seleccionarInstituto(String nombreInst);
	public void seleccionarCursoInstancia(String nombreCurso);	
	public boolean nuevoCurso(String nombreCurso );
	public void setearCurso(String nombreCurso, String desc, String duracion, int cantHoras, int cantCreditos, String url, Date fechaAlta, String foto );
	public String[] listarPrevias();
	public void seleccionarPrevias(List<String> previas);
	public void setearCategorias(ArrayList<String> categorias);
	public void confirmarAltaCurso();
	public void cancelarAltaCurso();
	public  ArrayList<String> listarCategorias(); // fase 2 nueva
	// Consulta Curso 
	//Reutilizo>>listarInstitutosArray() >> eleccionarInstituto(String nombreInst)
	String[]  listarNombreCursos(String nombreInst);
	public DtCurso mostrarDatosdeCurso(String nombreCurso);
	public String[] listarEdicionesdeCurso(String nombreCurso);
	String[] listarProgramasCurso(String nombreCurso);
	public DtPrograma verPrograma(String nombrePrograma);
	public DtPrograma mostrarPrograma(String nombrePrograma);
	public String[] cursosDePrograma(); // esta la agregu� pq el dt no los trae falta el DC
	public DtEdicion mostrarEdicion(String nombreEdicion);
	public void cancelarConsultaCurso();
	public DtCurso[] cursosdeCategoria(String categoria);//fase 2
	public DtCurso datosdeCurso(String nombreCurso);//sin memoria para la fase2
	public DtPrograma[] programasdeCurso(String nomCurso);//fase 2
	public  DtEdicion[] mostrarDtEdicionesArray(String nombreCurso);//fase 2
	public String institutoDeCurso(String nombreCurso);//fase2

	//Franco 
	// Alta Edicion Curso
	//
	//public String[] listarInstitutosArray();
	//public void seleccionarInstituto(String nombreInst);
	//List<String> listarNombreCursos(String nombreInst);
	public boolean altaEdicionCurso(String nombreInst, String nombreCurso, String nombreEdicion, Calendar fechaI,
			Calendar fechaF, int cupo, Calendar fechaPub, List<Docente> listaDocentes, String imagen);
	public void cancelarAltaEdicionCurso();
	public String[] listarInstitutos();
	
	
	// DatosEdicionCurso
	//public String[] listarInstitutosArray();
	//public void seleccionarInstituto(String nombreInst);
	//List<String> listarNombreCursos(String nombreInst);
	//public List<String> listarEdicionesdeCurso(String nombreCurso);
	//public DtEdicion mostrarEdicion(String nombreEdicion);
	
	//AltaPrograma
	public void altaPrograma(String nombrePrograma, String descripcion, Calendar fechaI, Calendar fechaF, String foto) throws ProgramaExisteException;
	public Instituto obtenerInstituto(String nomins);
	public Curso obtenerCurso(Instituto ins, String nomcur);
	public Edicion obtenerEdicionVigente(Curso objetivo);
	public void altaInscripcionEdc(String edicion, String estudiante) throws InscripcionEdicionRepetida ;
	//Consulta Programa
	public String[] listarProgramas();
	public DtCurso seleccionarCurso(String nombreCurso);
	public List<Curso>listarCursosP(String nomPrograma);
	
	
	public Edicion seleccionarEdicion(String nombreE);
	public List<String> listarEdiciones(String nomCurso);
	
	public String[] listarCursos();
	public void agregarCursoPrograma(String nomProg, String nomCurso) throws CursoDeProgramaRepetido;
	public String[] listarCategoriasPF(String programa);
	public ArrayList<String> obtenerCategorias();
	public void agregarCategoria(String nombreCat)  throws CategoriaRepetidaException;
	//Seleccionar estudiantes para una edici�n de curso
	public DtEdicion traerUltimaEdicion(String curso, String instituto) throws SinEdicionException;
	public DtInscripcionEdc[] obtenerInscripciones(String curso, String instituto,String edicion, boolean prioridad);
	public void setearInscripciones(String instituto, String curso,String eicion, List<DtInscripcionEdc> listaAceptados);
	
	//Aceptados a Edicion
	public  ArrayList <DtEdicion> mostrarEdiciones(String nomInstituto, String nombreCurso)throws SinEdicionException;
	public  ArrayList <DtInscripcionEdc> inscrpcionesAceptadas(String nomInstituto, String nombreCurso, String nomEdicion);
	
	//Alta Prog Form
	public void realizarInscripcionProgramaDeFormacion(String pf, String estudiante) throws InscripcionEdicionRepetida ;
	public ArrayList<String> imprimirCursosdeCategoria(String nombrecat);
	public Boolean existePrograma(String nombrePrograma);
	

}
