package persistencia;

import java.io.Serializable;

public class InscripcionPFID implements Serializable{
	private static final long serialVersionUID = 1L;
	private String inscripto;
	private String programa;

	public InscripcionPFID() {
		super();
	}

	public String getEstudiante() {
		return inscripto;
	}

	public void setEstudiante(String estudiante) {
		this.inscripto = estudiante;
	}

	public String getPrograma() {
		return programa;
	}

	public void setPrograma(String programa) {
		this.programa = programa;
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((inscripto == null) ? 0 : inscripto.hashCode());
		result = prime * result + ((programa == null) ? 0 : programa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InscripcionPFID other = (InscripcionPFID) obj;
		if (inscripto == null) {
			if (other.inscripto != null)
				return false;
		} else if (!inscripto.equals(other.inscripto))
			return false;
		if (programa == null) {
			if (other.programa != null)
				return false;
		} else if (!programa.equals(other.programa))
			return false;
		return true;
	}
	
	

}
