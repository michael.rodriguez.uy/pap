package excepciones;

public class EmailExistenteException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public EmailExistenteException(String email){
		super(email);
	}
}
