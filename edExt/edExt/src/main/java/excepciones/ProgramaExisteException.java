package excepciones;

public class ProgramaExisteException extends Exception{
	private static final long serialVersionUID = 1L;
	public ProgramaExisteException(String nombrePrograma){
		super(nombrePrograma);
	}

}
