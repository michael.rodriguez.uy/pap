package excepciones;

public class CursoDeProgramaRepetido extends Exception{
private static final long serialVersionUID = 1L;
	
	public CursoDeProgramaRepetido(String curso){
		super(curso);	
		
	}
}
