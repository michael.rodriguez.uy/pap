package excepciones;

public class SinInscripcionesException extends Exception{
	private static final long serialVersionUID = 1L;
	public SinInscripcionesException(String nombreEdicion){
		super(nombreEdicion);
	}	
}
