package excepciones;

public class NickNameExistenteException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public NickNameExistenteException(String nickName){
		super(nickName);
	}
}
