package excepciones;

public class SinInscripcionesEdc extends Exception{
	private static final long serialVersionUID = 1L;
	public SinInscripcionesEdc(String mensaje){
		super(mensaje);
	}
}
