package excepciones;

public class SinEdicionException extends Exception{
	private static final long serialVersionUID = 1L;
	public SinEdicionException(String nombreCurso){
		super(nombreCurso);
	}
}
