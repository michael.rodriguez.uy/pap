package excepciones;

public class InscripcionEdicionRepetida extends Exception{
	private static final long serialVersionUID = 1L;
	public InscripcionEdicionRepetida(String edicion){
		super(edicion);
	}

}
