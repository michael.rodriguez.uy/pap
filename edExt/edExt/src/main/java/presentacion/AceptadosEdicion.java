package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;

import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import datatypes.DtEdicion;
import datatypes.DtInscripcionEdc;
import excepciones.ClaveInvalidaException;
import excepciones.SinEdicionException;

import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;


public class AceptadosEdicion extends JInternalFrame {
	private String[] listaCursos;
	private String instituto;
	private String curso;

	public AceptadosEdicion(IControladorUsuario iconUs, IControladorInstProg iconIP) {
		setBounds(100, 100, 450, 300);
		this.setTitle("Aceptados a Edicion");
		this.setClosable(true);
		getContentPane().setLayout(null);
		
		JComboBox comboInstitutos = new JComboBox();
		comboInstitutos.setBounds(71, 27, 73, 21);
		getContentPane().add(comboInstitutos);
		
		JComboBox comboCursos = new JComboBox();
		comboCursos.setBounds(71, 80, 75, 21);
		getContentPane().add(comboCursos);
		
		JComboBox comboEdiciones = new JComboBox();
		comboEdiciones.setBounds(41, 198, 103, 21);
		getContentPane().add(comboEdiciones);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(321, 25, 203, 139);
		getContentPane().add(textArea);
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setBounds(321, 196, 166, 171);
		getContentPane().add(textArea_1);
		
		JButton btnRecIns = new JButton("R");
		btnRecIns.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboInstitutos.setEnabled(true);
				comboInstitutos.removeAllItems();
				String[] lista = iconIP.listarInstitutosArray();
				if(lista != null) {
					for(String i : lista){
						//System.out.println(i);
						comboInstitutos.addItem(i);					
					}			
					
				} else {
					comboInstitutos.addItem("Sin Datos");	
					comboInstitutos.setEnabled(false);
				}
			}
		});
		btnRecIns.setIcon(new ImageIcon(AceptadosEdicion.class.getResource("/img/Recargar.png")));
		btnRecIns.setBounds(10, 27, 41, 21);
		getContentPane().add(btnRecIns);
		
		JLabel lblNewLabel = new JLabel("Institutos");
		lblNewLabel.setBounds(71, 10, 73, 13);
		getContentPane().add(lblNewLabel);
		
		JButton btnSelInstit = new JButton("Sel Instituto");
		btnSelInstit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboCursos.removeAllItems();
				String s = comboInstitutos.getSelectedItem().toString();
				instituto=s;
				iconIP.seleccionarInstituto(s);
				comboInstitutos.setEnabled(false);
				 listaCursos = iconIP.listarNombreCursos((String) comboInstitutos.getSelectedItem());
				if(listaCursos.length!=0 && listaCursos!=null) {
					comboCursos.setEnabled(true);
					comboCursos.removeAllItems();
					for(String i : listaCursos){
						//System.out.println(i);
						comboCursos.addItem(i);					
					}
				}else {
					comboCursos.addItem("Sin Datos");	
				}
				comboInstitutos.setEnabled(false);
			}
		});
		btnSelInstit.setBounds(154, 27, 85, 21);
		getContentPane().add(btnSelInstit);
		
		JButton btnSelCurso = new JButton("Sel Curso");
		btnSelCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
				try {
					curso =(String)comboCursos.getSelectedItem();
					ArrayList<DtEdicion> dtediciones = iconIP.mostrarEdiciones(instituto , (String)comboCursos.getSelectedItem());
					comboEdiciones.removeAllItems();
					DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
					for(DtEdicion dte: dtediciones) {
						comboEdiciones.addItem(dte.getNombre());
						textArea.append(dte.getNombre()+ " ");
						textArea.append("FechaPub: " + dateFormat.format(dte.getFechaPub()) + "\n");
						//if(dte.get)
					}
					
				}catch(SinEdicionException e2) {
					JOptionPane.showMessageDialog(null, e2.getMessage(), "Aceptados Edicion ",JOptionPane.ERROR_MESSAGE);

				}
			}
		});
		btnSelCurso.setBounds(154, 80, 85, 21);
		getContentPane().add(btnSelCurso);
		

		
		JButton btnSelEdicion = new JButton("VerAceptados");
		btnSelEdicion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea_1.setText("");
				ArrayList <DtInscripcionEdc> dtinscacept = iconIP.inscrpcionesAceptadas(instituto, curso, (String)comboEdiciones.getSelectedItem());
				if(!dtinscacept.isEmpty()) {
					for(DtInscripcionEdc dti: dtinscacept)
					textArea_1.append("EstudianteAcept " +dti.getEstudiante().getNickName() + "\n");
				}
					
			}
		});
		btnSelEdicion.setBounds(162, 198, 103, 21);
		getContentPane().add(btnSelEdicion);
		
		
		JLabel lblNewLabel_1 = new JLabel("Ediciones");
		lblNewLabel_1.setBounds(71, 175, 58, 13);
		getContentPane().add(lblNewLabel_1);
		
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				textArea_1.setText("");
				textArea.setText("");
				comboCursos.removeAllItems();
				comboInstitutos.removeAllItems();
				comboEdiciones.removeAllItems();
			}
		});
		
	}
}
