package presentacion;

import javax.swing.JInternalFrame;

import interfaces.IControladorUsuario;
import logica.Usuario;

import javax.swing.border.CompoundBorder;

import UI.ImagenLogin;
import excepciones.UsuarioNoExisteException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.beans.PropertyVetoException;
import java.io.IOException;

import javax.swing.JTextField;
import javax.swing.RootPaneContainer;
import javax.swing.JPasswordField;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InciarSesion extends JInternalFrame {

	private IControladorUsuario iConUs;
	private JTextField nickNametextField;
	private JPasswordField passwordField;
	private JPanel panelImg;

	public InciarSesion(IControladorUsuario iConUs) {
		setTitle("Iniciar sesion");
		try {
			setIcon(true);
		} catch (PropertyVetoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setFrameIcon(new ImageIcon(InciarSesion.class.getResource("/img/icons8-spider-man-head-30.png")));
		setBorder(new CompoundBorder());
		this.iConUs = iConUs;
		getContentPane().setLayout(null);
		
		JLabel lblNickName = new JLabel("Nickname");
		lblNickName.setBounds(243, 33, 113, 28);
		getContentPane().add(lblNickName);
		
		nickNametextField = new JTextField();
		nickNametextField.setBounds(323, 33, 128, 28);
		getContentPane().add(nickNametextField);
		nickNametextField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(243, 84, 79, 20);
		getContentPane().add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(323, 79, 128, 28);
		getContentPane().add(passwordField);
		
		JButton btnIniciarSesion = new JButton("Iniciar Sesion");
		btnIniciarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					try {
						iniciarSesion(arg0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}});
		btnIniciarSesion.setBounds(255, 159, 113, 23);
		getContentPane().add(btnIniciarSesion);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nickNametextField.setText("");
				passwordField.setText("");
				
				InciarSesion.this.setVisible(false);				
			}
		});
		btnCancelar.setBounds(395, 159, 89, 23);
		getContentPane().add(btnCancelar);
		
		panelImg = new JPanel();
		panelImg.setBounds(10, 11, 214, 228);
		getContentPane().add(panelImg);
		JLabel label = new JLabel(new ImageIcon(getClass().getResource("/img/login.png")));
		label.setBounds(10,10,50,50);
		panelImg.add(label);
		//panelImg.revalidate();
		//panelImg.repaint();

		/*
		ImagenLogin ImagenL = new ImagenLogin();
		ImagenL.setBounds(0, 0, panelImg.getBounds().width, panelImg.getBounds().height);
		((JPanel) panelImg).getRootPane().add(ImagenL);
		panelImg.repaint();
		*/
		
		
	}
	
	
	public void iniciarSesion(ActionEvent arg0) throws   Exception {
		String stringLogin = this.nickNametextField.getText();
		char passArray1[] = passwordField.getPassword();
		String pass1 = new String(passArray1);
		if(iConUs.loginNickname(stringLogin, pass1)){
			JOptionPane.showMessageDialog(this, "Ok inicio", "Login Ususario",	JOptionPane.INFORMATION_MESSAGE);
		}else if(iConUs.loginEmail(stringLogin, pass1)) {
				JOptionPane.showMessageDialog(this, "Ok inicio", "Login Ususario", JOptionPane.INFORMATION_MESSAGE);
			}
			else{
				JOptionPane.showMessageDialog(this, "usuario inexistente o contraseņa invalida", "Login Ususario",	JOptionPane.INFORMATION_MESSAGE);	
				}	
			}
	

}
