package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.SwingConstants;

@SuppressWarnings({ "unused", "serial" })
public class AltaInstituto extends JInternalFrame {
	private JTextField textField;

	/**
	 * Launch the application.
	 */

	public AltaInstituto(IControladorUsuario iconUs, final IControladorInstProg iconIP) {
		getContentPane().setForeground(Color.GRAY);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		this.setTitle("Agregar Instituto");
		this.setClosable(true);
		JLabel lblNewLabel = new JLabel("Ingrese el nombre del Instituto");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setBounds(42, 38, 349, 13);
		getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(42, 55, 349, 25);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Agregar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agregar( iconIP );
			}
		});
		btnNewButton.setBounds(306, 87, 85, 21);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cancelar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelarAltaInstituto(iconIP);
			}
		});
		btnNewButton_1.setBounds(42, 87, 85, 21);
		getContentPane().add(btnNewButton_1);

	}
	protected void agregar(IControladorInstProg iconIP ) {
		String s= textField.getText();
		boolean existe = iconIP.ingresarNomInst(s);
    	if(!existe) {
    		textField.setText("");
    		JOptionPane.showMessageDialog(this, "Se Creo el Instituto","Alta Instituto", JOptionPane.DEFAULT_OPTION);
    		}else {
        		JOptionPane.showMessageDialog(this, "El Instituto ingresado ya existe","Alta Instituto", JOptionPane.ERROR_MESSAGE);    		    	
    	}
    	
    	//setVisible(false); 
    	
	}
	protected void cancelarAltaInstituto(IControladorInstProg iconIP) {
		iconIP.cancelarAltaCurso();
		//this.setVisible(false);
		this.dispose();
	}
}

