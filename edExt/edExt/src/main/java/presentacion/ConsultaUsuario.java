package presentacion;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.SystemColor;

import javax.swing.JInternalFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import com.toedter.calendar.JDateChooser;

import datatypes.DtDocente;
import datatypes.DtEdicion;
import datatypes.DtPrograma;
import datatypes.DtUsuario;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import logica.Curso;
import logica.Docente;
import logica.Edicion;
import logica.Estudiante;
import logica.InscripcionEdc;
import logica.InscripcionPF;
import logica.Instituto;
import logica.Programa;
import logica.Usuario;
import rojerusan.RSFotoSquareResize;

import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class ConsultaUsuario extends JInternalFrame {
	
	private static final long serialVersionUID = 1L;
	
	private IControladorUsuario iConUsu;
	private IControladorInstProg iConIns;
	
	private JComboBox<String> comboBoxUsuario; 
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtCorreo;
	private JDateChooser dateChooserFN;
	private JTextField textFieldMensaje;
	private JComboBox<String> comboBoxEdiciones;
	private JComboBox<String> comboBoxProgramas;
	private JTextField textFieldTipo;
	private JLabel txtProgramas;
	private JTextArea textAreaInformacion;
	private JLabel lblImagenUsu;



	
	/**
	 * Launch the application.
	 *//*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaUsuario frame = new ConsultaUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 * @param iConUsu 
	 */
	public ConsultaUsuario(final IControladorUsuario iConUsu, final IControladorInstProg iConIns) {
		this.iConUsu = iConUsu;
		this.iConIns = iConIns;
		setTitle("Consulta de Usuario");
		setClosable(true);
		setBounds(100, 100, 450, 320);
		getContentPane().setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(12, 23, 56, 16);
		getContentPane().add(lblUsuario);
		
		comboBoxUsuario = new JComboBox<String>();
		comboBoxUsuario.setEnabled(true);
		comboBoxUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rellenarDatos();
			}
		});
		comboBoxUsuario.setBounds(75, 20, 149, 25);
		getContentPane().add(comboBoxUsuario);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 52, 56, 16);
		getContentPane().add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(12, 81, 56, 16);
		getContentPane().add(lblApellido);
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setBounds(12, 110, 56, 16);
		getContentPane().add(lblCorreo);
		
		txtNombre = new JTextField();
		txtNombre.setEditable(false);
		txtNombre.setBounds(75, 49, 149, 25);
		getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setEditable(false);
		txtApellido.setBounds(75, 78, 149, 25);
		getContentPane().add(txtApellido);
		txtApellido.setColumns(10);
		
		txtCorreo = new JTextField();
		txtCorreo.setEditable(false);
		txtCorreo.setBounds(75, 107, 149, 25);
		getContentPane().add(txtCorreo);
		txtCorreo.setColumns(10);
		
		JLabel lblFechaDeNacimiento = new JLabel("FechanNac.");
		lblFechaDeNacimiento.setBounds(12, 147, 127, 15);
		getContentPane().add(lblFechaDeNacimiento);
		
		dateChooserFN = new JDateChooser();
		dateChooserFN.setBounds(85, 142, 139, 25);
		getContentPane().add(dateChooserFN);
		
		
		textFieldMensaje = new JTextField();
		textFieldMensaje.setEnabled(false);
		textFieldMensaje.setEditable(false);
		textFieldMensaje.setBounds(0, 242, 240, 25);
		getContentPane().add(textFieldMensaje);
		textFieldMensaje.setColumns(10);
		textFieldMensaje.setDisabledTextColor(Color.BLACK);
		
		comboBoxEdiciones = new JComboBox();
		comboBoxEdiciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listarComboBoxEdicion();
			}
		});
		comboBoxEdiciones.setBounds(85, 175, 139, 25);
		getContentPane().add(comboBoxEdiciones);
		
		comboBoxProgramas = new JComboBox();
		comboBoxProgramas.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				listarProgramas();
			}
		});
		comboBoxProgramas.setBounds(85, 210, 139, 25);
		getContentPane().add(comboBoxProgramas);
		
		JLabel txtEdiciones = new JLabel("Ediciones");
		txtEdiciones.setBounds(12, 178, 56, 16);
		getContentPane().add(txtEdiciones);
		
		txtProgramas = new JLabel("Programas");
		txtProgramas.setBounds(12, 213, 71, 16);
		getContentPane().add(txtProgramas);
		
		textFieldTipo = new JTextField();
		textFieldTipo.setEditable(false);
		textFieldTipo.setBounds(293, 20, 116, 25);
		getContentPane().add(textFieldTipo);
		textFieldTipo.setColumns(10);
		
		textAreaInformacion = new JTextArea();
		textAreaInformacion.setBounds(236, 49, 173, 202);
		getContentPane().add(textAreaInformacion);
		
		lblImagenUsu = new JLabel("");
		lblImagenUsu.setBounds(421, 52, 158, 172);
		getContentPane().add(lblImagenUsu);
	}
	
	public void inicializarComboBoxes() {
		DefaultComboBoxModel<String> nickUsuarios = new DefaultComboBoxModel<String>(iConUsu.listarUsuarios());
		comboBoxUsuario.setModel(nickUsuarios);
	}
	
	public void rellenarDatos() {
		DtUsuario u = iConUsu.seleccionarDtUsuario(this.comboBoxUsuario.getSelectedItem().toString());
		textAreaInformacion.setText("");
		txtNombre.setText(u.getNombre());
		txtApellido.setText(u.getApellido());
		txtCorreo.setText(u.getCorreo());
		
		Date date = u.getFechaNacimiento().getTime();  
		dateChooserFN.setDate(date);
		dateChooserFN.setEnabled(false);
		
		try {
			ImageIcon icon = new ImageIcon(u.getImagen().toString());
			Icon icono = new ImageIcon(icon.getImage().getScaledInstance(lblImagenUsu.getWidth(), lblImagenUsu.getHeight(), Image.SCALE_AREA_AVERAGING));
			lblImagenUsu.setText(null);
			lblImagenUsu.setIcon( icono );
		}
		catch(Exception e) {
			lblImagenUsu.setText("No hay foto asociada.");
		}
		
		if(u.getEsDocente()) {
				textFieldTipo.setText("DOCENTE");
				comboBoxProgramas.setVisible(false);
				txtProgramas.setVisible(false);
				List<String> edicionFin = new ArrayList<String>();
				Instituto i = iConUsu.seleccionarUnInstituto(((DtDocente)u).getNombreInstituto());
				List<Curso> cursos = i.getCursos();
				if(cursos != null) {
					for(Curso c: cursos) {
						List<Edicion> ediciones = c.getEdiciones();
						if(ediciones != null) {
							for(Edicion e: ediciones)
							{
								List<Docente> docentes = e.getListaDocente();
								if(docentes != null) {
									for(Usuario d: docentes) {
										if(d.getNickName().equals(u.getNickName())) {
											edicionFin.add(e.getNombre());
										}
									}
								}
							}
						}
					}
				}
				int j = 0;
				String[] edicionC = new String[edicionFin.size()];
				for(String e: edicionFin) {
					edicionC[j] = e;
					j++;
				}
				DefaultComboBoxModel<String> nickUsuarios = new DefaultComboBoxModel<String>(edicionC);
				comboBoxEdiciones.setModel(nickUsuarios);
			}
			else {
				comboBoxProgramas.setVisible(true);
				txtProgramas.setVisible(true);
				//Mostrar ediciones
				textFieldTipo.setText("ESTUDIANTE");
				//List<String> programaFin = new ArrayList<String>();
				List<String> inscripciones = new ArrayList<String>();
				String[] institutos = iConIns.listarInstitutos();
				if(!institutos.toString().isEmpty()) {
					for(String ins: institutos) {
						String[] cursos = iConIns.listarNombreCursos(ins);
						if(!cursos.toString().isEmpty()) {
							for(String cur: cursos) {
								String[] ediciones = iConIns.listarEdicionesdeCurso(cur);
								if(!ediciones.toString().isEmpty()) {
									for(String ed: ediciones) {
										if(inscripciones.isEmpty()) {
											String[] aux = iConUsu.seleccionarInscripcionED(u.getNickName());
											for(String i:aux) {
												inscripciones.add(i);
											}
										}									
									}
								}
								else {
									textFieldMensaje.setText("No hay ediciones.");
								}
							}
						}
						else {
							textFieldMensaje.setText("No hay cursos.");
						}
					}
				}
				else {
					textFieldMensaje.setText("No hay institutos.");
				}
				int i = 0;
				String[] edicionC = new String[inscripciones.size()];
				for(String e: inscripciones) {
					edicionC[i] = e;
					i++;
				}
				DefaultComboBoxModel<String> nomEdicion = new DefaultComboBoxModel<String>(edicionC);
				comboBoxEdiciones.setModel(nomEdicion);
			
				//Mostrar Programas
				
				
				/*
				List<InscripcionPF> inscripcionesPF = ((Estudiante)u).getInscripcionesPF();
				if(inscripcionesPF != null) {
					List<String> programa = new ArrayList<String>();
					for(InscripcionPF ins: inscripcionesPF) {
						programa.add(ins.getPrograma().getNombre());
					}
					int j = 0;
					String[] prog = new String[programa.size()];
					for(String p: programa) {
						prog[j] = p;
						j++;
					}
					DefaultComboBoxModel<String> nomPrograma = new DefaultComboBoxModel<String>(prog);
					comboBoxProgramas.setModel(nomPrograma);
				}*/
			}
	}
	
	
	public void listarComboBoxEdicion() {
		textAreaInformacion.setText("");
		Usuario u = iConUsu.seleccionarUsuario(comboBoxUsuario.getSelectedItem().toString());
		List<InscripcionEdc> ediciones = u.getInscripciones();
		if(ediciones.isEmpty()) {
			textAreaInformacion.setText("No hay datos.\n");
		}
		else {
			textAreaInformacion.setText("Datos de Ediciones de Curso.\n");
			Edicion edicion = iConIns.seleccionarEdicion((String)comboBoxEdiciones.getSelectedItem());
			textAreaInformacion.append("Nombre: " + edicion.getNombre() + "\n");
			textAreaInformacion.append("Fecha de Inicio: " + edicion.getFechaI() + "\n");
			textAreaInformacion.append("Fecha de Finalizacion: " + edicion.getFechaF() + "\n");
			textAreaInformacion.append("Cupo: " + edicion.getCupo() + "\n") ;
			textAreaInformacion.append("Fecha de Publicacion: " + edicion.getFechaPub() + "\n");
			textAreaInformacion.append("\nLista de Docentes:\n");
		
			List<Docente> lista = edicion.getListaDocente();
			System.out.println("Cantidad de Docentes: " + lista.size());
			for (Docente l : lista) {
				System.out.println(l.getNickName());	
				textAreaInformacion.append(l.getNickName() + "\n");
			}
		}
	}
	
	public void listarProgramas(){
		textAreaInformacion.setText("");
		try {
			DtPrograma p = iConIns.mostrarPrograma(this.comboBoxProgramas.getSelectedItem().toString());
			textAreaInformacion.setText("Datos de Programa.\n");
			textAreaInformacion.append("Nombre: " + p.getNombre() + "\n");
			textAreaInformacion.append("Descripcion: " + p.getDescripcion() + "\n");
			textAreaInformacion.append("Fecha inicio: " + p.getFechaI() + "\n");
			textAreaInformacion.append("Fecha fin: " + p.getFechaF() + "\n");
			String[] listCursos = iConIns.cursosDePrograma();
			if(!listCursos.toString().isEmpty()) {
				int i = 1;
				for(String c: listCursos) {
					textAreaInformacion.append("Curso " + i + ": " + c.toString() + "\n");
					i++;
				}
			}
			else {
				textAreaInformacion.append("No hay Cursos asociados al usuario.");
			}
		}
		catch(Exception e) {
			textAreaInformacion.append("No hay programas que mostrar.");
		}
	}
}	