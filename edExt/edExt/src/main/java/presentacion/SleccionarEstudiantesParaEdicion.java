package presentacion;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Panel;

import javax.swing.JInternalFrame;
import javax.swing.JTextField;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import datatypes.DtEdicion;
import datatypes.DtInscripcionEdc;
import datatypes.DtUsuario;
import datatypes.EstadoInscripcionEdc;
import excepciones.ClaveInvalidaException;
import excepciones.SinEdicionException;
import excepciones.UsuarioNoExisteException;
import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import logica.Docente;

import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.JSlider;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

public class SleccionarEstudiantesParaEdicion extends JInternalFrame {
	private JTextField textField;
	private JPasswordField passwordField;
	private boolean docenteOK;
	private String[] listaCursos;
	private String instituto;
	private String edicion;
	private String curso;

	private boolean ordenada = false;
	private DtInscripcionEdc[] inscripciones;
	/**
	 * @wbp.nonvisual location=-33,214
	 */
	private final JTree tree = new JTree();
	private final JButton btnEnviar = new JButton("Enviar");

	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SleccionarEstudiantesParaEdicion frame = new SleccionarEstudiantesParaEdicion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	 */
	public SleccionarEstudiantesParaEdicion(IControladorUsuario iconUs, IControladorInstProg iconIP) {
		ArrayList<DtInscripcionEdc> listaAceptados = new ArrayList<>();
		docenteOK=false;
		setBounds(100, 100, 450, 600);
		getContentPane().setLayout(null);
		//getContentPane().setForeground(Color.GRAY);
		
		textField = new JTextField();
		textField.setBounds(50, 49, 96, 26);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(50, 110, 96, 26);
		getContentPane().add(passwordField);
		
		JLabel lblNewLabel = new JLabel("Nick");
		lblNewLabel.setBounds(50, 24, 45, 13);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Contrase\u00F1a");
		lblNewLabel_1.setBounds(50, 93, 65, 13);
		getContentPane().add(lblNewLabel_1);
		
		JComboBox comboInstitutos = new JComboBox();
		comboInstitutos.setBounds(28, 215, 87, 21);
		getContentPane().add(comboInstitutos);
		
		JComboBox comboCursos = new JComboBox();
		comboCursos.setBounds(208, 51, 78, 21);
		getContentPane().add(comboCursos);
		
		JLabel lblNewLabel_3 = new JLabel("Cursos");
		lblNewLabel_3.setBounds(217, 34, 45, 13);
		getContentPane().add(lblNewLabel_3);
		
		JRadioButton rdbtnPrior = new JRadioButton("Prioridad");
		rdbtnPrior.setBounds(209, 89, 77, 21);
		getContentPane().add(rdbtnPrior);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(318, 10, 220, 226);
		getContentPane().add(textArea);
		
		
		JComboBox comboEstudiantes = new JComboBox();
		comboEstudiantes.setBounds(162, 280, 96, 21);
		getContentPane().add(comboEstudiantes);
		
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setBounds(378, 290, 160, 166);
		getContentPane().add(textArea_1);
		

		JLabel lblNewLabel_4 = new JLabel("Estudiante");
		lblNewLabel_4.setBounds(181, 257, 65, 13);
		getContentPane().add(lblNewLabel_4);
		this.setTitle("Admitir Estudiantes");
		this.setClosable(true);
		
		ButtonGroup grupo1 = new ButtonGroup();
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Aceptar");
		rdbtnNewRadioButton.setBounds(284, 280, 77, 21);
		getContentPane().add(rdbtnNewRadioButton);
		grupo1.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Rechazar");
		rdbtnNewRadioButton_1.setBounds(284, 303, 77, 21);
		getContentPane().add(rdbtnNewRadioButton_1);
		grupo1.add(rdbtnNewRadioButton_1);
		
		JButton btnNewButton_1 = new JButton("Agregar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DtUsuario usr = new DtUsuario( (String)comboEstudiantes.getSelectedItem(),"", "", "", null,false, null);
				DtEdicion edc= new DtEdicion(edicion, null, null,0,null,null,"");
				EstadoInscripcionEdc estado;
				String est;
				if(rdbtnNewRadioButton.isSelected()) {
					estado=EstadoInscripcionEdc.Aceptado;
					est="A";
				}else {
					estado=EstadoInscripcionEdc.Rechazado;
					est="R";
				}
				DtInscripcionEdc dtedc = new DtInscripcionEdc(edc, usr, new Date(), estado);
				textArea_1.append((String)comboEstudiantes.getSelectedItem() + " " + est + "\n");
				listaAceptados.add(dtedc);
			}
		});
		btnNewButton_1.setBounds(274, 330, 94, 21);
		getContentPane().add(btnNewButton_1);
		
		JButton btnListarinsed = new JButton("Sel Curso");
		btnListarinsed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
				comboEstudiantes.removeAllItems();
				curso = (String) comboCursos.getSelectedItem();
				//System.out.println(curso);
				try {
					DtEdicion dte = iconIP.traerUltimaEdicion(curso, instituto);
					edicion=dte.getNombre();
					textArea.append("Nombre:  " +dte.getNombre() + " ");
					textArea.append("Cupo:  " + dte.getCupo() + "\n");
					Date fI = dte.getFechaI().getTime();
					Date ff = dte.getFechaF().getTime();
					Date fp = dte.getFechaPub().getTime();
					
					
					DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd"); 
					//textArea.append("Fecha Inicio: " + dateFormat.format(fI) + "\n");
					//textArea.append("Fecha Final: " + dateFormat.format(ff) + "\n");
					textArea.append("Fecha Publi: " + dateFormat.format(fp) + "\n");
					
					inscripciones = iconIP.obtenerInscripciones(curso, instituto, dte.getNombre(), rdbtnPrior.isSelected()); // DtInscripcionEdc[] 
					comboEstudiantes.removeAllItems();
					if(inscripciones.length >= 0) {
						for(DtInscripcionEdc dti:inscripciones ) {
							comboEstudiantes.addItem(dti.getEstudiante().getNickName());
							//textArea_1.append(dti.getEstudiante().getNombre() + "\n");
							textArea.append("Nom:  " +dti.getEstudiante().getNombre() + "  ");
							textArea.append("Fec: " + dateFormat.format(dti.getFecha())+ "\n" );
							textArea.append(dti.getEstado().toString() +"\n" );
						}
					}else {
						JOptionPane.showMessageDialog(null,"No hay inscripciones","Aceptar Estudiante ",JOptionPane.INFORMATION_MESSAGE );
					}
					
				}catch(SinEdicionException e1){
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Aceptar Estudiante ",JOptionPane.WARNING_MESSAGE);
				}
				
			}
		});
		btnListarinsed.setBounds(212, 125, 85, 21);
		getContentPane().add(btnListarinsed);

		JButton btnNewButton = new JButton("Ingresar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				String nom= textField.getText();
				char passArray1[] = passwordField.getPassword();
				String pass = new String(passArray1);
				//System.out.println("Nombre " + nom +" Contrase�a "+ pass);
				try {
					String InstDocente =iconUs.ingresarProfe(nom,pass);
					//instituto=InstDocente;
					comboInstitutos.removeAllItems();
					String[] lista = iconIP.listarInstitutosArray();
					if(lista != null) {
						String primerItem = lista[0];
						for(String i : lista){
							//System.out.println(i);
							comboInstitutos.addItem(i);					
						}									
					} else {
						comboInstitutos.addItem("Sin Datos");	
						comboInstitutos.setEnabled(false);
					}
					JOptionPane.showMessageDialog(null, "Aceptado", "Aceptar Estudiante ",JOptionPane.INFORMATION_MESSAGE);	

					docenteOK=true;
					
					//iconUs.listarInstitutosdeDocente
					
				}catch(UsuarioNoExisteException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Aceptar Estudiante ",JOptionPane.ERROR_MESSAGE);
				}catch(ClaveInvalidaException e2) {
					JOptionPane.showMessageDialog(null, e2.getMessage(), "Aceptar Estudiante ",JOptionPane.ERROR_MESSAGE);
				}finally {

				}
			}
		});
		btnNewButton.setBounds(50, 163, 85, 21);
		getContentPane().add(btnNewButton);
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iconIP.setearInscripciones(instituto, curso, edicion, listaAceptados);
				//public void setearInscripciones(String instituto, String curso,String eicion, List<DtInscripcionEdc> listaAceptados);
			}
		});
		btnEnviar.setBounds(263, 466, 128, 26);
		getContentPane().add(btnEnviar);
		
		
		JButton btnListInstitutos = new JButton("Seleccionar Institutos");
		btnListInstitutos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboEstudiantes.removeAllItems();
				instituto= (String)comboInstitutos.getSelectedItem();
				listaCursos=iconIP.listarNombreCursos(instituto);
				comboCursos.removeAllItems();
				for(String s:listaCursos) comboCursos.addItem(s);
			}
			
		});
		btnListInstitutos.setBounds(132, 215, 85, 21);
		getContentPane().add(btnListInstitutos);
		

		////
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				docenteOK=false;
				comboCursos.removeAllItems();
				comboEstudiantes.removeAllItems();
				comboInstitutos.removeAllItems();
				textArea.setText("");
				listaCursos=null;
				instituto ="";
				edicion= "";
				ordenada = false;
				inscripciones=null;
				listaAceptados.clear();
				textArea_1.setText("");
				
			}
		});
	}
}
