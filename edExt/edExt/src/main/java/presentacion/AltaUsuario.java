package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import excepciones.EmailExistenteException;
import excepciones.NickNameExistenteException;

import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import interfaces.IControladorUsuario;
import logica.Instituto;
import interfaces.IControladorInstProg;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.awt.event.ActionEvent;
import javax.swing.event.ChangeListener;

import com.toedter.calendar.JDateChooser;

import javax.swing.event.ChangeEvent;
import javax.swing.JPasswordField;
import rojerusan.RSFotoCircle;
import rojerusan.RSFotoSquare;
import rojerusan.RSFotoSquareResize;
import java.awt.SystemColor;

@SuppressWarnings("unused")
public class AltaUsuario extends JInternalFrame {

	private static final long serialVersionUID = 1L;

	private IControladorUsuario iconUs;
	private IControladorInstProg iconIP;

	public boolean esDocente = false;

	private JTextField textFieldNickname;
	private JTextField textFieldEmail;
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JComboBox<String> comboBoxListarInstitutos;
	private JCheckBox chckbxEsDocente;
	private JDateChooser dateChooserFN;
	private JPasswordField passwordField;
	private JPasswordField passwordRepetir;
	private RSFotoSquareResize fotoSquareResize;
	private BufferedImage img;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 * 
	 * @param iconIP
	 */
	public AltaUsuario(IControladorUsuario iconUs, IControladorInstProg iconIP) {

		this.iconUs = iconUs;
		this.iconIP = iconIP;

		setTitle("edEXT");
		setClosable(true);
		setBounds(100, 100, 450, 427);
		getContentPane().setLayout(null);

		JLabel LabelNickname = new JLabel("Nickname");
		LabelNickname.setBounds(30, 34, 61, 14);
		getContentPane().add(LabelNickname);

		JLabel LabelEmail = new JLabel("Email");
		LabelEmail.setBounds(30, 65, 46, 14);
		getContentPane().add(LabelEmail);

		textFieldNickname = new JTextField();
		textFieldNickname.setBounds(91, 28, 121, 25);
		getContentPane().add(textFieldNickname);
		textFieldNickname.setColumns(10);

		textFieldEmail = new JTextField();
		textFieldEmail.setColumns(10);
		textFieldEmail.setBounds(91, 59, 121, 25);
		getContentPane().add(textFieldEmail);

		JLabel LabelNombre = new JLabel("Nombre");
		LabelNombre.setBounds(30, 155, 46, 14);
		getContentPane().add(LabelNombre);

		JLabel LabelApellido = new JLabel("Apellido");
		LabelApellido.setBounds(215, 155, 46, 14);
		getContentPane().add(LabelApellido);

		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);
		textFieldNombre.setBounds(86, 151, 101, 25);
		getContentPane().add(textFieldNombre);

		textFieldApellido = new JTextField();
		textFieldApellido.setColumns(10);
		textFieldApellido.setBounds(276, 150, 121, 25);
		getContentPane().add(textFieldApellido);

		JLabel SeleccioneInstitutoLabel = new JLabel("Seleccione Instituto");
		SeleccioneInstitutoLabel.setBounds(215, 260, 148, 14);
		getContentPane().add(SeleccioneInstitutoLabel);
		comboBoxListarInstitutos = new JComboBox<String>();
		comboBoxListarInstitutos.setToolTipText("Seleccione instituto");
		comboBoxListarInstitutos.setBounds(215, 285, 182, 25);
		getContentPane().add(comboBoxListarInstitutos);
		comboBoxListarInstitutos.setEnabled(false);

		JLabel lblFechaDeNacimiento = new JLabel("Fecha de nacimiento");
		lblFechaDeNacimiento.setBounds(77, 223, 135, 15);
		getContentPane().add(lblFechaDeNacimiento);
		dateChooserFN = new JDateChooser();
		dateChooserFN.setBounds(215, 213, 182, 25);
		getContentPane().add(dateChooserFN);

		JButton btnConfirmarAltaUsuario = new JButton("Confirmar");
		btnConfirmarAltaUsuario.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
			}
		});
		btnConfirmarAltaUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					AltaUsuarioAceptarActionPerformed(e);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnConfirmarAltaUsuario.setBounds(181, 349, 117, 23);
		getContentPane().add(btnConfirmarAltaUsuario);

		JButton btnCancelarAltaUsuario = new JButton("Cancelar");
		btnCancelarAltaUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AltaUsuario.this.setVisible(false);
				limpiarFormulario();
			}
		});
		btnCancelarAltaUsuario.setBounds(308, 349, 89, 23);
		getContentPane().add(btnCancelarAltaUsuario);

		chckbxEsDocente = new JCheckBox("Es Docente");
		chckbxEsDocente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxListarInstitutos.setEnabled(true);
			}
		});
		chckbxEsDocente.setBounds(51, 286, 121, 23);
		getContentPane().add(chckbxEsDocente);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(30, 122, 46, 14);
		getContentPane().add(lblPassword);

		passwordField = new JPasswordField();
		passwordField.setBounds(91, 119, 89, 25);
		getContentPane().add(passwordField);

		JLabel lblRepPassword = new JLabel("Repetir Password");
		lblRepPassword.setBounds(190, 122, 102, 14);
		getContentPane().add(lblRepPassword);

		passwordRepetir = new JPasswordField();
		passwordRepetir.setBounds(295, 119, 102, 25);
		getContentPane().add(passwordRepetir);

		// Foto de perfil
		fotoSquareResize = new RSFotoSquareResize();
		fotoSquareResize.setColorBorde(SystemColor.control);

		fotoSquareResize.setBounds(242, 0, 121, 111);
		getContentPane().add(fotoSquareResize);

	}

	@SuppressWarnings("unused")
	protected void AltaUsuarioAceptarActionPerformed(ActionEvent arg0) throws IOException, URISyntaxException {
		String nickName = this.textFieldNickname.getText();
		String email = this.textFieldEmail.getText();
		String nombre = this.textFieldNombre.getText();
		String apellido = this.textFieldApellido.getText();
		int anioN = dateChooserFN.getCalendar().get(Calendar.YEAR);
		int mesN = dateChooserFN.getCalendar().get(Calendar.MONTH) + 1;
		int diaN = dateChooserFN.getCalendar().get(Calendar.DAY_OF_MONTH);
		String fechaNString = diaN + "-" + mesN + "-" + anioN;
		char passArray1[] = passwordField.getPassword();
		String pass1 = new String(passArray1);
		char passArray2[] = passwordRepetir.getPassword();
		String pass2 = new String(passArray2);
		
		
		DateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
		Date fNac = null;
		String fotoName;
		String foto = fotoSquareResize.getRutaImagen();
		
		if (foto.isEmpty()) {
			foto= System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "desconocido.jpg";
			System.out.println(foto);
			fotoName= "desconocido.jpg";
		}else {
			fotoName = fotoSquareResize.getNombreImagen();
		}
		 


		File outputfile = new File(foto); 
		img = ImageIO.read(outputfile);
		File oute = new File(System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + fotoName);
		ImageIO.write(img, "jpg", oute );

		String rutaSubida= System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + fotoName;
		if (fotoSquareResize.getRutaImagen().isEmpty())
			rutaSubida = System.getProperty("user.home") + File.separator + "edExt"+ File.separator +"images"  + File.separator + "desconocido.jpg";

		try {
			fNac = formatoFecha.parse(fechaNString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Calendar fNacG= Calendar.getInstance();
		fNacG.setTime(fNac);

		boolean esDocente = false;
		if (chckbxEsDocente.isSelected()) {
			esDocente = true;
		}

		if (esDocente) {
			String nombreInstituto = this.comboBoxListarInstitutos.getSelectedItem().toString();
			System.out.println(nombreInstituto);

			if (checkFormulario()) {
				if (checkContrasena(pass1, pass2)) {
					try {
						iconUs.altaUsuarioDocente(nickName, nombre, apellido, email, fNacG, esDocente, pass1, rutaSubida,
								nombreInstituto);
						JOptionPane.showMessageDialog(this, "El usuario ha sido dado de alta", "Alta Ususario",
								JOptionPane.INFORMATION_MESSAGE);
						limpiarFormulario();
					} catch (NickNameExistenteException | EmailExistenteException e) {
						JOptionPane.showMessageDialog(this, e.getMessage(), "Alta de usuario",
								JOptionPane.ERROR_MESSAGE);
					}
					setVisible(true);

				} else {
					passArray1 = null;
					passArray2 = null;
					pass1 = null;
					pass2 = null;
					passwordField.setText("");
					passwordRepetir.setText("");
				}

			}

		} else {
			if (checkFormulario()) {
				if (checkContrasena(pass1, pass2)) {
					try {
						iconUs.altaUsuarioEstudiante(nickName, nombre, apellido, email, fNacG, esDocente, pass1, rutaSubida);
						String rr= iconUs.rutaAnombreDeImagen(rutaSubida);
						System.out.println("ruta: " + rr);
						JOptionPane.showMessageDialog(this, "El usuario ha sido dado de alta", "Alta Ususario",
								JOptionPane.INFORMATION_MESSAGE);
						limpiarFormulario();
					} catch (NickNameExistenteException | EmailExistenteException e) {
						JOptionPane.showMessageDialog(this, e.getMessage(), "Alta de usuario",
								JOptionPane.ERROR_MESSAGE);
					}

					setVisible(true);
				} else {
					passArray1 = null;
					passArray2 = null;
					pass1 = null;
					pass2 = null;
					passwordField.setText("");
					passwordRepetir.setText("");
				}
			}

		}
		passArray1 = null;
		passArray2 = null;
		pass1 = null;
		pass2 = null;

	}

	private boolean checkFormulario() {
		String nickName = this.textFieldNickname.getText();
		String email = this.textFieldEmail.getText();
		char passArray1[] = passwordField.getPassword();
		String pass1 = new String(passArray1);
		char passArray2[] = passwordRepetir.getPassword();
		String pass2 = new String(passArray1);
		if (nickName.isEmpty() || email.isEmpty()) {
			if (nickName.isEmpty()) {
				JOptionPane.showMessageDialog(this, "Debe introducir un nick name", "Alta Usuario",
						JOptionPane.ERROR_MESSAGE);
				return false;
			} else {
				if (email.isEmpty()) {
					JOptionPane.showMessageDialog(this, "Debe introducir un email", "Alta Usuario",
							JOptionPane.ERROR_MESSAGE);
					return false;
				}

			}
		}
		return true;
	}

	private void limpiarFormulario() {
		textFieldNickname.setText("");
		textFieldNombre.setText("");
		textFieldApellido.setText("");
		textFieldEmail.setText("");
		chckbxEsDocente.setSelected(false);
		comboBoxListarInstitutos.setEnabled(false);
		passwordField.setText("");
		passwordRepetir.setText("");

	}

	public boolean iniciarlizarComboBoxes() {
		if (iconIP.listarInstitutos().length == 0) {
			JOptionPane.showMessageDialog(this, "No existen insitutos para dar de alta usuairos", "alta usuario",
					JOptionPane.WARNING_MESSAGE);

			return false;
		} else {

			DefaultComboBoxModel<String> modelInstitutos = new DefaultComboBoxModel<String>(
					iconIP.listarInstitutosArray());
			comboBoxListarInstitutos.setModel(modelInstitutos);
			return true;
		}

	}

	public boolean checkContrasena(String pass1, String pass2) {
		boolean ok = true;
		;
		if (pass1.isEmpty() || pass2.isEmpty()) {
			ok = false;
			JOptionPane.showMessageDialog(this, "Debe introducir una contrase�a", "Alta Usuario",
					JOptionPane.ERROR_MESSAGE);
		} else {
			if (!pass1.equals(pass2)) {
				ok = false;
				JOptionPane.showMessageDialog(this, "Las contrase�as no coinciden, corrija e intente de nuevo",
						"Alta Usuario", JOptionPane.ERROR_MESSAGE);
			}
		}
		return ok;
	}
}
