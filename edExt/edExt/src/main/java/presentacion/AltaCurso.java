package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextField;

import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

@SuppressWarnings({ "unused", "serial", "rawtypes" })
public class AltaCurso extends JInternalFrame {
	private JTextField textFieldNombre;
	private JTextField textFielDesc;
	private JTextField textFielDuracion;
	private JTextField textFieldHoras;
	private JTextField textFieldCreditos;
	private JTextField textFieldUrl;
	private JTextField textFieldFoto;

	private String[] listaCursos;
	private List<String> previas = new ArrayList<>();
	private List<String> categorias = new ArrayList<>();
	private ArrayList<String> categoriasdelcurso = new ArrayList<>();
	private boolean institutoSeleccionado = false;
	//private JComboBox combocursos;


	public AltaCurso(IControladorUsuario iconUs, final IControladorInstProg iconIP) {
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				textFieldNombre.setText("");
				textFielDesc.setText("");
				textFielDuracion.setText("");
				textFieldHoras.setText("");
				textFieldCreditos.setText("");
				textFieldUrl.setText("");	
			}
		});

		//System.out.println("Se crea Alta Curso");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		this.setTitle("alta de Curso");
		this.setClosable(true);
		//DefaultListModel DLM= new DefaultListModel();

		JComboBox comboBoxListarInstitutos = new JComboBox();
		comboBoxListarInstitutos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//String s = comboBoxListarInstitutos.getSelectedItem().toString();
				//iconIP.seleccionarInstituto(s);
				//DLM.addElement("Curso");
			}
		});
		comboBoxListarInstitutos.setBounds(10, 33, 123, 25);
		getContentPane().add(comboBoxListarInstitutos);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(400, 52, 150, 220);
		getContentPane().add(textArea);
		
		JLabel lblNewLabel = new JLabel("Previas");
		lblNewLabel.setBounds(288, 67, 55, 13);
		getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel8 = new JLabel("Categorias");
		lblNewLabel8.setBounds(288, 167, 80, 13);
		getContentPane().add(lblNewLabel8);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(20, 64, 96, 25);
		getContentPane().add(textFieldNombre);
		textFieldNombre.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setBounds(126, 67, 68, 13);
		getContentPane().add(lblNewLabel_1);
		
		textFielDesc = new JTextField();
		textFielDesc.setBounds(20, 93, 96, 25);
		getContentPane().add(textFielDesc);
		textFielDesc.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Descripcion");
		lblNewLabel_2.setBounds(126, 96, 90, 13);
		getContentPane().add(lblNewLabel_2);
		
		textFielDuracion = new JTextField();
		textFielDuracion.setBounds(20, 123, 96, 25);
		getContentPane().add(textFielDuracion);
		textFielDuracion.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Duracion");
		lblNewLabel_3.setBounds(126, 126, 103, 13);
		getContentPane().add(lblNewLabel_3);
		
		textFieldHoras = new JTextField();
		textFieldHoras.setBounds(20, 152, 96, 25);
		getContentPane().add(textFieldHoras);
		textFieldHoras.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Horas Int");
		lblNewLabel_4.setBounds(126, 155, 90, 13);
		getContentPane().add(lblNewLabel_4);
		
		textFieldCreditos = new JTextField();
		textFieldCreditos.setBounds(20, 181, 96, 25);
		getContentPane().add(textFieldCreditos);
		textFieldCreditos.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Cr\u00E9ditos Int");
		lblNewLabel_5.setBounds(126, 184, 90, 13);
		getContentPane().add(lblNewLabel_5);
		
		textFieldUrl = new JTextField();
		textFieldUrl.setBounds(20, 210, 96, 25);
		getContentPane().add(textFieldUrl);
		textFieldUrl.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("URL");
		lblNewLabel_6.setBounds(126, 213, 68, 13);
		getContentPane().add(lblNewLabel_6);
		
		textFieldFoto = new JTextField();
		textFieldFoto.setBounds(20, 240, 96, 25);
		getContentPane().add(textFieldFoto);
		textFieldUrl.setColumns(10);
		textFieldFoto.setText("cursodefault.jpg");
		
		JLabel lblNewLabel_7 = new JLabel("Foto");
		lblNewLabel_7.setBounds(126, 243, 68, 13);
		getContentPane().add(lblNewLabel_7);
		
		JComboBox combocategorias; // lo pas� pq lo preciso ac�
		/*JComboBox*/ combocategorias = new JComboBox();
		combocategorias.setBounds(256, 192, 124, 25);
		getContentPane().add(combocategorias);
		
		JComboBox combocursos; // lo pas� pq lo preciso ac�
		/*JComboBox*/ combocursos = new JComboBox();
		combocursos.setBounds(256, 92, 124, 25);
		getContentPane().add(combocursos);
		combocursos.setEnabled(false);
		//String[] lista;
		JButton btnCrear = new JButton("Crear");
		btnCrear.addActionListener(new ActionListener() {
			//@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				if(!combocursos.isEnabled() && crear(iconIP, textFieldNombre )){
					combocursos.setEnabled(true);
					combocursos.removeAllItems();
					previas.clear();
					listaCursos = iconIP.listarPrevias();
					for(String s:listaCursos ) System.out.println(s + " SIACA");
					
					if(listaCursos != null && listaCursos.length>0) {
						String primerItem = listaCursos[0];
						for(String i : listaCursos){
							//System.out.println(i);
							combocursos.addItem(i);					
						}			
						combocursos.setSelectedItem(primerItem);
						getContentPane().add(combocursos);
						
					} else {
						combocursos.addItem("Sin Datos");	
						//comboBoxListarInstitutos.setEnabled(false);
					}
					
					categorias = iconIP.listarCategorias();
					if(categorias != null && !categorias.isEmpty()) {
						//String primerItem = listaCursos[0];
						for(String i : categorias){
							//System.out.println(i);
							combocategorias.addItem(i);					
						}			
						
					} else {
						combocursos.addItem("Sin Datos");	
						//comboBoxListarInstitutos.setEnabled(false);
					}
				}

			}
		});
		btnCrear.setBounds(41, 330, 85, 21);
		getContentPane().add(btnCrear);
		
		JButton btnSelInst = new JButton("Seleccionar Instituto");
		btnSelInst.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				combocursos.removeAllItems();
				String s = comboBoxListarInstitutos.getSelectedItem().toString();
				iconIP.seleccionarInstituto(s);
				comboBoxListarInstitutos.setEnabled(false);
				institutoSeleccionado=true;
			}
			
		});
		btnSelInst.setBounds(156, 33, 164, 21);
		getContentPane().add(btnSelInst);
		
		//aca iba combocursos
		
		JButton btnAniadir = new JButton("A\u00F1adir");
		btnAniadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s = combocursos.getSelectedItem().toString();
				boolean pertenece = false;
				for(int i=0; i<previas.size(); i++) {
					if(s==previas.get(i)) pertenece=true;
				}
				if(!pertenece) {
					textArea.append(s + "\n");
					//System.out.println(s+"ACA");
					previas.add(s);
				}
				
			}
		});
		btnAniadir.setBounds(281, 122, 85, 21);
		getContentPane().add(btnAniadir);
		
		JButton btnAgreCateg = new JButton("AggregarCat");
		btnAgreCateg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cat = (String)combocategorias.getSelectedItem();
				textArea.append(cat);
				categoriasdelcurso.add(cat);				
			}
		});
		btnAgreCateg.setBounds(281, 222, 95, 21);
		getContentPane().add(btnAgreCateg);
		
		JButton btnFin = new JButton("Confirmar");
		btnFin.setHorizontalAlignment(SwingConstants.LEFT);
		btnFin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textFieldNombre.getText().compareTo("")!=0) {
				for(String s: previas)System.out.println(s);
				iconIP.seleccionarPrevias(previas);
				// public void setearCategorias(ArrayList<String> categorias); 
				iconIP.setearCategorias(categoriasdelcurso);
				iconIP.confirmarAltaCurso();
				JOptionPane.showMessageDialog(null, "Se ha creado el curso!","Alta curso", JOptionPane.INFORMATION_MESSAGE);
				textFieldNombre.setText("");
				textFielDesc.setText("");
				textFielDuracion.setText("");
				textFieldHoras.setText("");
				textFieldCreditos.setText("");
				textFieldUrl.setText("");
				previas.clear();
				combocursos.removeAllItems();
				combocursos.setEnabled(false);
				institutoSeleccionado=false;
				comboBoxListarInstitutos.removeAllItems();
				comboBoxListarInstitutos.setEnabled(false);
			
				
				//iconIP.cancelarAltaCurso();  
				combocursos.removeAllItems();
				previas.clear();
				combocursos.setEnabled(false);
				textArea.setText("");
				}else {JOptionPane.showMessageDialog(null, "�Nombre del Curso?","Alta curso", JOptionPane.QUESTION_MESSAGE);}
			}
		});
		btnFin.setBounds(321, 330, 96, 21);
		getContentPane().add(btnFin);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iconIP.cancelarAltaCurso();
				previas.clear();
				combocursos.removeAllItems();
				combocursos.setEnabled(false);
				textArea.setText("");
				aVerSiMeDeja();
			}
		});
		btnCancelar.setBounds(197, 330, 85, 21);
		getContentPane().add(btnCancelar);
		
		comboBoxListarInstitutos.setEnabled(false);
		JButton btnRefresh = new JButton("Cargar Institutos");
		btnRefresh.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) { 
				if(comboBoxListarInstitutos.isEnabled()==false) {
					// Inicialiar Comobo Box
					comboBoxListarInstitutos.setEnabled(true);
					comboBoxListarInstitutos.removeAllItems();
					String[] lista = iconIP.listarInstitutosArray();
					// Si hay registros 
					if(lista != null) {
						String primerItem = lista[0];
						for(String i : lista){
							//System.out.println(i);
							comboBoxListarInstitutos.addItem(i);					
						}			
						comboBoxListarInstitutos.setSelectedItem(primerItem);
						//getContentPane().add(comboBoxListarInstitutos);
						institutoSeleccionado=false;
						
					} else {
						comboBoxListarInstitutos.addItem("Sin Datos");	
						comboBoxListarInstitutos.setEnabled(false);
					}
				}
				
				
			}
		});
		btnRefresh.setBounds(10, 6, 164, 21);
		getContentPane().add(btnRefresh);
		
		// Inicialiar Comobo Box
		/*String[] lista = iconIP.listarInstitutosArray();
		// Si hay registros 
		if(lista != null) {
			String primerItem = lista[0];
			for(String i : lista){
				System.out.println(i);
				comboBoxListarInstitutos.addItem(i);					
			}			
			comboBoxListarInstitutos.setSelectedItem(primerItem);
			getContentPane().add(comboBoxListarInstitutos);
			
		} else {
			comboBoxListarInstitutos.addItem("Sin Datos");	
			comboBoxListarInstitutos.setEnabled(false);
		}*/
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				textArea.setText("");
				combocursos.removeAllItems();
				previas.clear();
				combocursos.setEnabled(false);
				institutoSeleccionado=false;
				comboBoxListarInstitutos.removeAllItems();
				comboBoxListarInstitutos.setEnabled(false);
				
			}
		});
		
	}
	//setearCurso(String nombreCurso, String desc, String duracion, int cantHoras, int cantCreditos, String url, Date fechaAlta);

	protected boolean crear(IControladorInstProg iconIP, JTextField textFieldNombre) {
		String nom = textFieldNombre.getText();
		boolean noExiste = false;
			
			if(institutoSeleccionado) {
				noExiste =  iconIP.nuevoCurso(nom);
				if(noExiste) {
					String desc = textFielDesc.getText();
					String duracion = textFielDuracion.getText();
					int canthoras = Integer.parseInt(textFieldHoras.getText());
					int creditos= Integer.parseInt(textFieldCreditos.getText());
					String url = textFieldUrl.getText();
					Date d = new Date();
					String foto = textFieldFoto.getText();
					iconIP.setearCurso(nom, desc, duracion, canthoras,  creditos,  url, d,foto);
					

					//combocursos
					
				}else {
					JOptionPane.showMessageDialog(null, "Ese Curso ya Existe"," Alta Curso ", JOptionPane.ERROR_MESSAGE);
					textFieldNombre.setText("");
		    		//JOptionPane.showMessageDialog(this, "Ese ya est� ingresado, fijate bien!","Swing Tester", JOptionPane.ERROR_MESSAGE);

				}				
			}else {
				JOptionPane.showMessageDialog(this, "Seleccionar Instituto","Alta Curso", JOptionPane.ERROR_MESSAGE);
			}
			


		
		return noExiste;

	}
	
	public void aVerSiMeDeja() {
		this.dispose();
		
		
	}
}


