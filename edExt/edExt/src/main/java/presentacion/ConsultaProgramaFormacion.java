package presentacion;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import datatypes.DtCurso;
import datatypes.DtEdicion;
import datatypes.DtPrograma;
import interfaces.IControladorInstProg;
import logica.ControladorInstProg;
import logica.Curso;
import logica.Edicion;
import logica.Programa;

import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;

public class ConsultaProgramaFormacion extends JInternalFrame {
	private IControladorInstProg iConIns;
	
	private JTextField textFieldDescripcion;
	private JComboBox<String> comboBoxPF;
	private JDateChooser dateChooserFIni;
	private JDateChooser dateChooserFFin;
	private JComboBox<String> comboBoxCursos;
	private JTextArea textAreaCurso;
	
	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultaProgramaFormacion frame = new ConsultaProgramaFormacion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public ConsultaProgramaFormacion(final IControladorInstProg iConIns) {
		this.iConIns = iConIns;
		
		setTitle("Consulta Programa de Formacion");
		setClosable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblPrograma = new JLabel("Programa de formacion");
		lblPrograma.setBounds(12, 13, 147, 16);
		getContentPane().add(lblPrograma);
		
		comboBoxPF = new JComboBox();
		comboBoxPF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textAreaCurso.setText("");
				rellenarDatos();
			}
		});
		comboBoxPF.setBounds(165, 10, 142, 25);
		getContentPane().add(comboBoxPF);
		
		JLabel lbldescripcionPF = new JLabel("Descripcion");
		lbldescripcionPF.setBounds(12, 42, 71, 16);
		getContentPane().add(lbldescripcionPF);
		
		JLabel lblFechaIPF = new JLabel("Fecha Inicial");
		lblFechaIPF.setBounds(12, 71, 83, 16);
		getContentPane().add(lblFechaIPF);
		
		JLabel lblFFinalPF = new JLabel("Fecha Final");
		lblFFinalPF.setBounds(12, 100, 71, 16);
		getContentPane().add(lblFFinalPF);
		
		textFieldDescripcion = new JTextField();
		textFieldDescripcion.setBounds(95, 39, 162, 25);
		getContentPane().add(textFieldDescripcion);
		textFieldDescripcion.setColumns(10);
		textFieldDescripcion.setDisabledTextColor(Color.BLACK);
		
		dateChooserFIni = new JDateChooser();
		dateChooserFIni.setBounds(95, 71, 162, 25);
		getContentPane().add(dateChooserFIni);
		
		dateChooserFFin = new JDateChooser();
		dateChooserFFin.setBounds(95, 100, 162, 25);
		getContentPane().add(dateChooserFFin);
		
		JLabel lblCursos = new JLabel("Cursos");
		lblCursos.setBounds(12, 129, 56, 16);
		getContentPane().add(lblCursos);
		
		comboBoxCursos = new JComboBox();
		comboBoxCursos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mostrarDatosCurso();
			}
		});
		comboBoxCursos.setBounds(95, 132, 142, 25);
		getContentPane().add(comboBoxCursos);
		
		textAreaCurso = new JTextArea();
		textAreaCurso.setBounds(269, 39, 165, 225);
		getContentPane().add(textAreaCurso);
	}
	
	public void inicializarComboBoxPF(){
		DefaultComboBoxModel<String> nomProg = new DefaultComboBoxModel<String>(iConIns.listarProgramas());
		comboBoxPF.setModel(nomProg);
	}
	
	public void rellenarDatos() {
		DtPrograma p = iConIns.mostrarPrograma(this.comboBoxPF.getSelectedItem().toString());
		textFieldDescripcion.setText(p.getDescripcion());
		textFieldDescripcion.setEnabled(false);
		dateChooserFIni.setCalendar(p.getFechaI());
		dateChooserFIni.setEnabled(false);
		dateChooserFFin.setCalendar(p.getFechaF());
		dateChooserFFin.setEnabled(false);
		
		String[] listCursos = iConIns.cursosDePrograma();
		if(listCursos.toString() != null) {
			DefaultComboBoxModel<String> nomCurso = new DefaultComboBoxModel<String>(iConIns.cursosDePrograma());
			comboBoxCursos.setModel(nomCurso);
		}
	}
	
	
	public void mostrarDatosCurso(){
		try {
			DtCurso dt = iConIns.seleccionarCurso(comboBoxCursos.getSelectedItem().toString());
			textAreaCurso.setText("Datos de Curso\n");
			//DtCurso dt = p.getDtCurso((String)comboBoxCursos.getSelectedItem());
			textAreaCurso.append("Nombre: " +dt.getNombre()+ "\n");
			textAreaCurso.append("Descripcion: " + dt.getDescripcion() + "\n");
			textAreaCurso.append("Duracion: " + dt.getDuracion() + "\n");
			textAreaCurso.append("Cantidad de Horas: " + String.valueOf(dt.getCantidadHoras())+ "\n");
			textAreaCurso.append("Creditos: " + String.valueOf(dt.getCantidadCreditos())+ "\n");
			textAreaCurso.append("Url: " + dt.getUrl()+ "\n");
			List<String> ediciones = iConIns.listarEdiciones(dt.getNombre());
			//String[] listaediciones=iConIns.listarEdiciones((String)comboBoxCursos.getSelectedItem());
			textAreaCurso.append("Ediciones:\n");
			for(String e: ediciones){
				textAreaCurso.append(e.toString() + "\n");
			}
			
			Date fechaAlta = dt.getFechaAlta();
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd"); //Quitar horas y segundos
			//dateFormat.format(date)
			textAreaCurso.append("Fecha Alta: " + dateFormat.format(fechaAlta));
		}
		catch(Exception e) {
			textAreaCurso.setText("No hay cursos que mostrar.");
			//JOptionPane.showMessageDialog(this, "No hay cursos", "Consulta Programa Formacion",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	 private Calendar dateToCalendar(Date date) {

	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        return calendar;

	    }
}

