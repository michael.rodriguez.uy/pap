package presentacion;

import java.awt.EventQueue;

import interfaces.Fabrica;

import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import publicadores.ControladorPublish;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

import UI.Imagen;
import UI.ImagenLogin;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.awt.event.InputEvent;
import java.awt.Toolkit;
import java.awt.SystemColor;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;

@SuppressWarnings("unused")
public class Principal {

	private JFrame frmEdextSuperheroAcademy;	
	private AltaUsuario altaUsuarioInternalFrame;
	private ConsultaEdicionCurso consultaEdicionCurso;
	private AltaInstituto altaInstitutoIFrame;
	private AltaCurso altaCursoIFrame;
	private ConsultaCurso consultaCursoIFrame;
	private AltaEdicionCurso altaEdicionCursoIFrame;
	private InscripcionEdicion inscripcionEdicionIFrame;
	private AltaPrograma altaProgramaIFrame;
	private ConsultaUsuario consultaUsuarioIFrame;
	private ModificarUsuario modificarUsuarioIFrame;
	private ConsultaProgramaFormacion consultaProgramaFormacionIFrame;
	private AgregarCursoAProgramaF agregarCursoPFIFrame;
	private SleccionarEstudiantesParaEdicion seleccionarEstudianteIFrame;
	private AceptadosEdicion AceptadosEdicionIFrame;
	private CategoriaAlt CategoriaAltIFrame;

	private InciarSesion iniciarSesionIFrame;
	private static EntityManagerFactory emf;
	private static EntityManager em;


	public static void main(String[] args) {
		// Esto creo que no lo comente yo (Wueisman), cualquier cosa descomentenlo
		
		emf = Persistence.createEntityManagerFactory("edExt");
		em = emf.createEntityManager();

		  
		new PantallaCargandoMain();
		System.out.println("Se abrio el programa ");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
					       
					Principal window = new Principal();  
					window.frmEdextSuperheroAcademy.setVisible(true);
					//seteo background
					Imagen Imagen = new Imagen();
					Imagen.setBounds(0, 0, window.frmEdextSuperheroAcademy.getBounds().width, window.frmEdextSuperheroAcademy.getBounds().height);
					window.frmEdextSuperheroAcademy.getContentPane().add(Imagen);
					window.frmEdextSuperheroAcademy.repaint();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	

	/**
	 * Create the application.
	 * @throws IOException 
	 */
	@SuppressWarnings("unused")
	public Principal() throws IOException {
		ControladorPublish cp = new ControladorPublish();
		cp.publicar();
		initialize();
		
		Fabrica fabrica = Fabrica.getInstancia();
		IControladorInstProg iCip = fabrica.getIControladorIP();
		IControladorUsuario iConUs = fabrica.getIControladorU();
		 
		
		Dimension desktopSize = frmEdextSuperheroAcademy.getSize();
		frmEdextSuperheroAcademy.setLocationRelativeTo(null);
		Dimension jInternalFrameSize;

		

		//internalFrame AltaUsuario
		altaUsuarioInternalFrame = new AltaUsuario(iConUs, iCip);
		altaUsuarioInternalFrame.setSize(609, 421);
		jInternalFrameSize = altaUsuarioInternalFrame.getSize();
		altaUsuarioInternalFrame.setLocation(0,0);
		altaUsuarioInternalFrame.setVisible(false);
		frmEdextSuperheroAcademy.getContentPane().add(altaUsuarioInternalFrame);
		Imagen ImagenAU = new Imagen();
		ImagenAU.setBounds(0, 0, altaUsuarioInternalFrame.getBounds().width, altaUsuarioInternalFrame.getBounds().height);
		altaUsuarioInternalFrame.getContentPane().add(ImagenAU);
		altaUsuarioInternalFrame.repaint();

		//internalFrame ConsultaEdicionCurso
		consultaEdicionCurso = new ConsultaEdicionCurso(iCip,iConUs);
		consultaEdicionCurso.setSize(609,421);
		jInternalFrameSize = consultaEdicionCurso.getSize();
		consultaEdicionCurso.setLocation(0,0);
		consultaEdicionCurso.setVisible(false);
		frmEdextSuperheroAcademy.getContentPane().add(consultaEdicionCurso);
		Imagen ImagenCE = new Imagen();
		ImagenCE.setBounds(0, 0, consultaEdicionCurso.getBounds().width, consultaEdicionCurso.getBounds().height);
		consultaEdicionCurso.getContentPane().add(ImagenCE);
		consultaEdicionCurso.repaint();
		
		
		// internalFrame AltaInstituto
		altaInstitutoIFrame= new AltaInstituto(iConUs, iCip);
		altaInstitutoIFrame.setSize(609,421);
		jInternalFrameSize=altaInstitutoIFrame.getSize();
		altaInstitutoIFrame.setLocation(0, 0);
		frmEdextSuperheroAcademy.getContentPane().add(altaInstitutoIFrame);
		Imagen ImagenAI = new Imagen();
		ImagenAI.setBounds(0, 0, altaInstitutoIFrame.getBounds().width, altaInstitutoIFrame.getBounds().height);
		altaInstitutoIFrame.getContentPane().add(ImagenAI);
		altaInstitutoIFrame.repaint();
		
		//internalFrame AltaCurso
		altaCursoIFrame = new AltaCurso(iConUs, iCip);
		altaCursoIFrame.setSize(609,421);
		jInternalFrameSize=altaCursoIFrame.getSize();
		altaCursoIFrame.setLocation(0,0);
		frmEdextSuperheroAcademy.getContentPane().add(altaCursoIFrame);
		Imagen ImagenAC = new Imagen();
		ImagenAC.setBounds(0, 0, altaCursoIFrame.getBounds().width, altaCursoIFrame.getBounds().height);
		altaCursoIFrame.getContentPane().add(ImagenAC);
		altaCursoIFrame.repaint();
		
		//internalFrame ConsultaCurso
		consultaCursoIFrame = new ConsultaCurso(iCip);
		consultaCursoIFrame.setSize(609,421);
		jInternalFrameSize = consultaCursoIFrame.getSize();
		consultaCursoIFrame.setLocation(0,0);
		frmEdextSuperheroAcademy.getContentPane().add(consultaCursoIFrame);
		Imagen ImagenCC = new Imagen();
		ImagenCC.setBounds(0, 0, consultaCursoIFrame.getBounds().width, consultaCursoIFrame.getBounds().height);
		consultaCursoIFrame.getContentPane().add(ImagenCC);
		consultaCursoIFrame.repaint();
		
		//internalFrame AltaEdicionCurso
		altaEdicionCursoIFrame = new AltaEdicionCurso(iCip, iConUs);
		altaEdicionCursoIFrame.setSize(630,500);
		jInternalFrameSize = altaEdicionCursoIFrame.getSize();
		altaEdicionCursoIFrame.setLocation(0,0);
		altaEdicionCursoIFrame.setVisible(false);
		frmEdextSuperheroAcademy.getContentPane().add(altaEdicionCursoIFrame);
		Imagen ImagenEC = new Imagen();
		ImagenEC.setBounds(0, 0, altaEdicionCursoIFrame.getBounds().width, altaEdicionCursoIFrame.getBounds().height);
		altaEdicionCursoIFrame.getContentPane().add(ImagenEC);
		altaEdicionCursoIFrame.repaint();
		
		//internalFrame InscripcionEdicion
		inscripcionEdicionIFrame = new InscripcionEdicion(iCip, iConUs);
		inscripcionEdicionIFrame.setSize(630,500);
		jInternalFrameSize = inscripcionEdicionIFrame.getSize();
		inscripcionEdicionIFrame.setLocation(0,0);
		inscripcionEdicionIFrame.setVisible(false);
		frmEdextSuperheroAcademy.getContentPane().add(inscripcionEdicionIFrame);
		Imagen ImagenIEC = new Imagen();
		ImagenIEC.setBounds(0, 0, 		inscripcionEdicionIFrame.getBounds().width, 		inscripcionEdicionIFrame.getBounds().height);
		inscripcionEdicionIFrame.getContentPane().add(ImagenIEC);
		inscripcionEdicionIFrame.repaint();

		//internalFrame AltaPrograma
		altaProgramaIFrame = new AltaPrograma(iCip);
		altaProgramaIFrame.setSize(630,500);
		jInternalFrameSize = altaProgramaIFrame.getSize();
		altaProgramaIFrame.setLocation(0,0);
		altaEdicionCursoIFrame.setVisible(false);
		frmEdextSuperheroAcademy.getContentPane().add(altaProgramaIFrame);
		Imagen ImagenAP = new Imagen();
		ImagenAP.setBounds(0, 0, altaProgramaIFrame.getBounds().width, altaProgramaIFrame.getBounds().height);
		altaProgramaIFrame.getContentPane().add(ImagenAP);
		altaProgramaIFrame.repaint();
		
		//internalFrame ConsultaUsuario
		consultaUsuarioIFrame = new ConsultaUsuario(iConUs, iCip);
		consultaUsuarioIFrame.setSize(609, 421);
		jInternalFrameSize = consultaUsuarioIFrame.getSize();
		consultaUsuarioIFrame.setLocation(0,0);
		consultaUsuarioIFrame.setVisible(false);
		frmEdextSuperheroAcademy.getContentPane().add(consultaUsuarioIFrame);
		Imagen ImagenCU = new Imagen();
		ImagenCU.setBounds(0, 0, consultaUsuarioIFrame.getBounds().width, consultaUsuarioIFrame.getBounds().height);
		consultaUsuarioIFrame.getContentPane().add(ImagenCU);
		consultaUsuarioIFrame.repaint();
		
		//internalFrame ModificarUsuario
		modificarUsuarioIFrame = new ModificarUsuario(iConUs);
		modificarUsuarioIFrame.setSize(609, 421);
		jInternalFrameSize = modificarUsuarioIFrame.getSize();
		modificarUsuarioIFrame.setLocation(0,0);
		modificarUsuarioIFrame.setVisible(false);
		frmEdextSuperheroAcademy.getContentPane().add(modificarUsuarioIFrame);
		Imagen ImagenMU = new Imagen();
		ImagenMU.setBounds(0, 0, modificarUsuarioIFrame.getBounds().width, modificarUsuarioIFrame.getBounds().height);
		modificarUsuarioIFrame.getContentPane().add(ImagenMU);
		modificarUsuarioIFrame.repaint();
		

		frmEdextSuperheroAcademy.getContentPane().add(modificarUsuarioIFrame);
		
		//internalFrame ComsultaProgramaFormacion
		//internalFrame ConsultaProgramaFormacion
		consultaProgramaFormacionIFrame = new ConsultaProgramaFormacion(iCip);
		consultaProgramaFormacionIFrame.setSize(609, 421);
		jInternalFrameSize = consultaProgramaFormacionIFrame.getSize();
		consultaProgramaFormacionIFrame.setLocation(0,0);
		consultaProgramaFormacionIFrame.setVisible(false);
		frmEdextSuperheroAcademy.getContentPane().add(consultaProgramaFormacionIFrame);
		Imagen ImagenCPF = new Imagen();
		ImagenCPF.setBounds(0, 0, consultaProgramaFormacionIFrame.getBounds().width, consultaProgramaFormacionIFrame.getBounds().height);
		consultaProgramaFormacionIFrame.getContentPane().add(ImagenCPF);
		consultaProgramaFormacionIFrame.repaint();
			

		frmEdextSuperheroAcademy.getContentPane().add(consultaProgramaFormacionIFrame);
		
		//internalFrame AgregarCursoAProgramaF
		agregarCursoPFIFrame = new AgregarCursoAProgramaF(iCip);
		agregarCursoPFIFrame.setSize(609, 421);
		jInternalFrameSize = agregarCursoPFIFrame.getSize();
		agregarCursoPFIFrame.setLocation(0,0);
		agregarCursoPFIFrame.setVisible(false);
		frmEdextSuperheroAcademy.getContentPane().add(agregarCursoPFIFrame);
		Imagen ImagenACP = new Imagen();
		ImagenACP.setBounds(0, 0, agregarCursoPFIFrame.getBounds().width, agregarCursoPFIFrame.getBounds().height);
		agregarCursoPFIFrame.getContentPane().add(ImagenACP);
		agregarCursoPFIFrame.repaint();
		
		//internalFrame SleccionarEstudiantesParaEdicion
		seleccionarEstudianteIFrame=new SleccionarEstudiantesParaEdicion(iConUs, iCip);
		seleccionarEstudianteIFrame.setSize(609,621);
		jInternalFrameSize=seleccionarEstudianteIFrame.getSize();
		seleccionarEstudianteIFrame.setVisible(false);
		seleccionarEstudianteIFrame.setLocation(20,100);
		frmEdextSuperheroAcademy.getContentPane().add(seleccionarEstudianteIFrame);
		
		//iniciar sesion
		iniciarSesionIFrame = new InciarSesion(iConUs);
		iniciarSesionIFrame.setBounds(70, 200 , 550, 300);
		frmEdextSuperheroAcademy.getContentPane().add(iniciarSesionIFrame);
		iniciarSesionIFrame.setVisible(true);
		
		//Aceptados Edicion
		AceptadosEdicionIFrame= new AceptadosEdicion(iConUs, iCip);
		AceptadosEdicionIFrame.setBounds(20, 20 , 550, 450);
		frmEdextSuperheroAcademy.getContentPane().add(AceptadosEdicionIFrame);
		//AceptadosEdicionIFrame.setVisible(true); 
		
		//AltaCategoria CategoriaAlt CategoriaAltIFrame
		CategoriaAltIFrame= new CategoriaAlt(iConUs, iCip);
		CategoriaAltIFrame.setSize(609,421);
		jInternalFrameSize=CategoriaAltIFrame.getSize();
		CategoriaAltIFrame.setLocation(0, 0);
		frmEdextSuperheroAcademy.getContentPane().add(CategoriaAltIFrame);
		
		
		
			}



	private void initialize() throws IOException {
		
		frmEdextSuperheroAcademy = new JFrame();
		frmEdextSuperheroAcademy.setResizable(false);
		frmEdextSuperheroAcademy.getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		frmEdextSuperheroAcademy.getContentPane().setBackground(Color.LIGHT_GRAY);
		frmEdextSuperheroAcademy.setForeground(Color.GRAY);
		frmEdextSuperheroAcademy.setTitle("edEXT");
		frmEdextSuperheroAcademy.setIconImage(Toolkit.getDefaultToolkit().getImage(Principal.class.getResource("/img/icons8-spider-man-head-30.png")));
		frmEdextSuperheroAcademy.setBackground(Color.LIGHT_GRAY);
		frmEdextSuperheroAcademy.setBounds(100, 100, 666, 779);
		frmEdextSuperheroAcademy.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frmEdextSuperheroAcademy.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Usuarios");
		mnNewMenu.setBackground(Color.GRAY);
		mnNewMenu.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmAltaUsuario = new JMenuItem("Alta");
		mntmAltaUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				altaUsuarioInternalFrame.setVisible(true);
				altaUsuarioInternalFrame.iniciarlizarComboBoxes();
			}
		});
		mntmAltaUsuario.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		mnNewMenu.add(mntmAltaUsuario);
		
		JMenuItem mntmNewMenuModifUsuario = new JMenuItem("Modificar");
		mntmNewMenuModifUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				modificarUsuarioIFrame.setVisible(true);
				modificarUsuarioIFrame.inicializarComboBoxes();
			}
		});
		mnNewMenu.add(mntmNewMenuModifUsuario);
		
		JMenuItem mntmConsultUsuario = new JMenuItem("Consultar");
		mntmConsultUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				consultaUsuarioIFrame.inicializarComboBoxes();
				consultaUsuarioIFrame.setVisible(true);
			}
		});
		mntmConsultUsuario.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		mnNewMenu.add(mntmConsultUsuario);
		
		JMenuItem mntmNewMenuSelEstud = new JMenuItem("Admitir Estudiante");
		mntmNewMenuSelEstud.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				seleccionarEstudianteIFrame.setVisible(true);
				//modificarUsuarioIFrame.inicializarComboBoxes();
			}
		});
		
		mnNewMenu.add(mntmNewMenuSelEstud);

		JMenu mnNewMenu_2 = new JMenu("Instituto");
		mnNewMenu_2.setBackground(Color.GRAY);
		mnNewMenu_2.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmAltaInstituto = new JMenuItem("Alta");
		mntmAltaInstituto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				altaInstitutoIFrame.setVisible(true);
			}
		} );
		mnNewMenu_2.add(mntmAltaInstituto);
		
		//AltaCategoria CategoriaAlt CategoriaAltIFrame

		JMenuItem mntmAltaCategoria = new JMenuItem("Alta Categoria");
		mntmAltaCategoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CategoriaAltIFrame.setVisible(true);
			}
		} );
		mnNewMenu_2.add(mntmAltaCategoria);
		
		
		JMenu mnNewMenu_1 = new JMenu("Curso");
		mnNewMenu_1.setBackground(Color.GRAY);
		mnNewMenu_1.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		mnNewMenu_1.setForeground(Color.BLACK);
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmAltaCurso = new JMenuItem("Alta");
		mnNewMenu_1.add(mntmAltaCurso);
		mntmAltaCurso.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mntmAltaCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				altaCursoIFrame.setVisible(true);
				//altaCursoIFrame.iniciarlizarComboBoxes();
			}			
		});
		
		JMenuItem mntmConsultaCurso = new JMenuItem("Consulta");
		mnNewMenu_1.add(mntmConsultaCurso);
		mntmConsultaCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				consultaCursoIFrame.setVisible(true);
				//altaCursoIFrame.iniciarlizarComboBoxes();
			}	
		});
		
		JMenuItem mntmAltaEdicionCurso = new JMenuItem("Alta edicion de curso");
		mntmAltaEdicionCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				altaEdicionCursoIFrame.setVisible(true);
				//altaEdicionCursoIFrame.iniciarlizarComboBoxes();
			}
		});
		mnNewMenu_1.add(mntmAltaEdicionCurso);
		mntmAltaEdicionCurso.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		
		JMenuItem mntmConsultaEdicCurso = new JMenuItem("Consulta edicion de curso");
		mntmConsultaEdicCurso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consultaEdicionCurso.setVisible(true);
			}
		});
		mnNewMenu_1.add(mntmConsultaEdicCurso);
		
		//AceptadosEdicionIFrame
		JMenuItem mntmConsultarAceptadosAEdicion = new JMenuItem("Aceptados a Edicion");
		mntmConsultarAceptadosAEdicion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AceptadosEdicionIFrame.setVisible(true);
			}
		});
		mnNewMenu_1.add(mntmConsultarAceptadosAEdicion);
		
		JMenu mnNewMenu_3 = new JMenu("Prog. de formaci\u00F3n");
		mnNewMenu_3.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnNewMenu_3);
		
		JMenuItem mntmCrearProgDeFormacion = new JMenuItem("Crear nuevo");
		mntmCrearProgDeFormacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				altaProgramaIFrame.setVisible(true);
			}
		});
		mnNewMenu_3.add(mntmCrearProgDeFormacion);
		
		JMenuItem mntmAgregarCursoAprogDeFormacion = new JMenuItem("Agregar cursos");
		mntmAgregarCursoAprogDeFormacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarCursoPFIFrame.setVisible(true);
				agregarCursoPFIFrame.inicializarComboBoxPF();
			}
		});
		mnNewMenu_3.add(mntmAgregarCursoAprogDeFormacion);
		
		JMenuItem mntmConsultarProgFormacion = new JMenuItem("Consultar");
		mntmConsultarProgFormacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consultaProgramaFormacionIFrame.setVisible(true);
				consultaProgramaFormacionIFrame.inicializarComboBoxPF();
			}
		});
		mnNewMenu_3.add(mntmConsultarProgFormacion);
		frmEdextSuperheroAcademy.getContentPane().setLayout(null);
		
		////////////////////////////MENU DE INSCRIPCIONES///////////////////////
		JMenu mnInscripciones = new JMenu("Inscripciones");
		mnInscripciones.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		menuBar.add(mnInscripciones);
		
		JMenuItem mntmNuevaInscripcion = new JMenuItem("Nueva inscripcion");
		mntmNuevaInscripcion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inscripcionEdicionIFrame.setVisible(true);
			}
		});
		mnInscripciones.add(mntmNuevaInscripcion);
				
		JMenuItem mntmConsultaInscripcion = new JMenuItem("ConsultarInscripcion");
		mnInscripciones.add(mntmConsultaInscripcion);
		mntmConsultaInscripcion.setEnabled(false);
		
		JButton btnIniciarSesion = new JButton("Iniciar Sesion");
		btnIniciarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iniciarSesionIFrame.setVisible(true);
			}
		});
		menuBar.add(btnIniciarSesion);
		frmEdextSuperheroAcademy.getContentPane().setLayout(null);
	}
	}

