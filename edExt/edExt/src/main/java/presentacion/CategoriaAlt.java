package presentacion;

import java.awt.Color;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import interfaces.IControladorInstProg;
import interfaces.IControladorUsuario;
import javax.swing.JTextField;

import excepciones.CategoriaRepetidaException;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class CategoriaAlt extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	private JTextField textField;


	public CategoriaAlt(IControladorUsuario iconUs, final IControladorInstProg iconIP) {
		getContentPane().setForeground(Color.GRAY);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		this.setTitle("Agregar Categoria");
		this.setClosable(true);
		
		textField = new JTextField();
		textField.setBounds(28, 52, 214, 31);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(28, 143, 378, 86);
		getContentPane().add(textArea);
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				llenartextbox(comboBox,textArea,iconIP);
			}
		});
		comboBox.setBounds(28, 94, 214, 38);
		getContentPane().add(comboBox);
		
		
		
		JButton btnNewButton = new JButton("Agregar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nuevoNombre=null;
				try {
					if(!textField.getText().equals("")) {
						iconIP.agregarCategoria(textField.getText());
						JOptionPane.showMessageDialog(null, "Se ha creado la categoria!","Alta Categoria", JOptionPane.INFORMATION_MESSAGE);
					}else {
						JOptionPane.showMessageDialog(null, "La categoria nueva requiere un nombre"," Alta Categoria ", JOptionPane.WARNING_MESSAGE);
						}
				}catch(CategoriaRepetidaException exe) {
					JOptionPane.showMessageDialog(null, "Esa Categoria ya Existe"," Alta Categoria ", JOptionPane.ERROR_MESSAGE);
					textField.setText("");
				}
					System.out.println(nuevoNombre);										
				
				comboBox.removeAllItems();
			}
		});
		btnNewButton.setBounds(262, 51, 119, 32);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Refresh");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				llenarcombobox(comboBox,iconIP);
				//llenartextbox(comboBox,textArea,iconIP);
			}
		});
		btnNewButton_1.setBounds(262, 102, 119, 30);
		getContentPane().add(btnNewButton_1);
	}
	void llenarcombobox(JComboBox<String> este, IControladorInstProg icip ) {
		este.removeAllItems();
		ArrayList<String> categorias = icip.obtenerCategorias();
		if(categorias!=null && categorias.size()>0) {
			for(String iterador: categorias) {
				este.addItem(iterador);
			}
		}
	}
	void llenartextbox(JComboBox<String> este,JTextArea texto, IControladorInstProg icip ) {
		texto.setText("Cursos Asociados\n");
		String target= (String) este.getSelectedItem();
		if(target!=null && !target.equals("")) {
		ArrayList<String> cursosobjetivo = icip.imprimirCursosdeCategoria(target);
		if(!cursosobjetivo.isEmpty()) {
			for(String iterador:cursosobjetivo){
				texto.append(" Curso: " +iterador+ "\n"); 
			}
		}}
		
	}
}
