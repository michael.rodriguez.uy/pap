package presentacion;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

import datatypes.DtCurso;
import interfaces.IControladorInstProg;
import javax.swing.JButton;

public class AgregarCursoAProgramaF extends JInternalFrame {
	private IControladorInstProg iConIns;
	private JComboBox<String> comboBoxPF;
	private JComboBox<String> comboBoxCursos;
	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgregarCursoAProgramaF frame = new AgregarCursoAProgramaF(iConIns);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public AgregarCursoAProgramaF(final IControladorInstProg iConIns) {
		this.iConIns = iConIns;
		
		setTitle("Agregar Curso a Programa de Formacion");
		setClosable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblPrograma = new JLabel("Programa de formacion");
		lblPrograma.setBounds(12, 44, 147, 16);
		getContentPane().add(lblPrograma);
		
		comboBoxPF = new JComboBox();
		comboBoxPF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listarCursos();
			}
		});
		comboBoxPF.setBounds(183, 41, 142, 25);
		getContentPane().add(comboBoxPF);
		
		JLabel lblCursos = new JLabel("Cursos");
		lblCursos.setBounds(12, 93, 56, 16);
		getContentPane().add(lblCursos);
		
		comboBoxCursos = new JComboBox();
		comboBoxCursos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		comboBoxCursos.setBounds(183, 90, 142, 25);
		getContentPane().add(comboBoxCursos);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agregarCursoProgF();
			}
		});
		btnAceptar.setBounds(228, 197, 97, 25);
		getContentPane().add(btnAceptar);
	}
	
	public void inicializarComboBoxPF(){
		DefaultComboBoxModel<String> nomProg = new DefaultComboBoxModel<String>(iConIns.listarProgramas());
		comboBoxPF.setModel(nomProg);
	}
	
	public void listarCursos() {
		DefaultComboBoxModel<String> nomProg = new DefaultComboBoxModel<String>(iConIns.listarCursos());
		comboBoxCursos.setModel(nomProg);
	}
	
	public void agregarCursoProgF() {
		String nomProg = "";
		String nomCurso = "";
		try {
		nomProg = (String) this.comboBoxPF.getSelectedItem().toString();
		nomCurso = (String) this.comboBoxCursos.getSelectedItem().toString();
			try {
				iConIns.agregarCursoPrograma(nomProg, nomCurso);
				JOptionPane.showMessageDialog(null, "Se ha creado el curso.","Agregar Curso a Programa de Formacion", JOptionPane.INFORMATION_MESSAGE);
			}
			catch(Exception e) {
				JOptionPane.showMessageDialog(this, "El Curso "+nomCurso+" ya esta asociado al Programa "+nomProg, "Agregar Curso a Programa de Formacion", JOptionPane.ERROR_MESSAGE);
			}
		}
		catch(Exception e) {
			JOptionPane.showMessageDialog(this, "Faltan datos por seleccionar.", "Agregar Curso a Programa de Formacion", JOptionPane.ERROR_MESSAGE);
		}
	}
}
