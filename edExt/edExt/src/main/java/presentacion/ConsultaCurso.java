package presentacion;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import interfaces.IControladorInstProg;
import logica.Docente;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import datatypes.DtCurso;
import datatypes.DtEdicion;
import datatypes.DtPrograma;


@SuppressWarnings({ "unused", "serial" })
public class ConsultaCurso extends JInternalFrame {

	private String[] listaCursos;
	private List<Docente> listaDocente = new ArrayList<Docente>();
	/**
	 * Create the frame.
	 */
	@SuppressWarnings("rawtypes")
	public ConsultaCurso(final IControladorInstProg iconIP) {
		setBounds(100, 100, 550, 370);
		getContentPane().setLayout(null);
        setResizable(true);
        setMaximizable(true);
        //setBounds(30, 30, 400, 280);


		
		@SuppressWarnings("rawtypes")
		JComboBox comboInstitutos = new JComboBox();
		comboInstitutos.setBounds(57, 45, 104, 25);
		getContentPane().add(comboInstitutos);
		
		JComboBox combocursos = new JComboBox();
		combocursos.setBounds(57, 104, 90, 25);
		getContentPane().add(combocursos);
		
		JComboBox combocurprog = new JComboBox();
		combocurprog.setBounds(21, 257, 126, 25);
		getContentPane().add(combocurprog);
		
		
		ButtonGroup grupo1 = new ButtonGroup(); // Eso es para agruparlos y que sea solo uno el que quede select
		 
		JRadioButton rdbtnEdiciones = new JRadioButton("Ediciones");
		rdbtnEdiciones.setBounds(35, 158, 103, 21);
		getContentPane().add(rdbtnEdiciones);
		grupo1.add(rdbtnEdiciones);
		
		JRadioButton rdbtnprogramas = new JRadioButton("Programas");
		rdbtnprogramas.setBounds(35, 184, 103, 21);
		getContentPane().add(rdbtnprogramas);
		grupo1.add(rdbtnprogramas);
		
		comboInstitutos.setEnabled(false);
		JButton btnInstitutos = new JButton(",");
		btnInstitutos.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				if(comboInstitutos.isEnabled()==false) {
					// Inicialiar Comobo Box
					comboInstitutos.setEnabled(true);
					comboInstitutos.removeAllItems();
					String[] lista = iconIP.listarInstitutosArray();
					// Si hay registros 
					if(lista != null) {
						String primerItem = lista[0];
						for(String i : lista){
							System.out.println(i);
							comboInstitutos.addItem(i);					
						}			
						comboInstitutos.setSelectedItem(primerItem);
						getContentPane().add(comboInstitutos);
						
					} else {
						comboInstitutos.addItem("Sin Datos");	
						comboInstitutos.setEnabled(false);
					}
				}
			}
		});
		btnInstitutos.setForeground(Color.WHITE);
		btnInstitutos.setIcon(new ImageIcon(ConsultaCurso.class.getResource("/img/Recargar.png")));
		btnInstitutos.setBounds(10, 42, 37, 27);
		getContentPane().add(btnInstitutos);
		
		JButton btnlistarcursos = new JButton("Sel Instituto");
		combocursos.setEnabled(false);
		btnlistarcursos.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				 listaCursos = iconIP.listarNombreCursos((String) comboInstitutos.getSelectedItem());
				if(listaCursos.length!=0 && listaCursos!=null) {
					combocursos.setEnabled(true);
					combocursos.removeAllItems();
					for(String i : listaCursos){
						System.out.println(i);
						combocursos.addItem(i);					
					}
				}else {
					combocursos.addItem("Sin Datos");	
				}
				comboInstitutos.setEnabled(false);
			}
		});
		btnlistarcursos.setBounds(186, 45, 97, 21);
		getContentPane().add(btnlistarcursos);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(359, 102, 169, 198);
		getContentPane().add(textArea);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(35, 236, 83, 13);
		getContentPane().add(lblNewLabel);
		
		//combocurprog
		
		combocurprog.setEnabled(false);
		JButton btnListPoE = new JButton("Listar");
		btnListPoE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(rdbtnEdiciones.isSelected()) {
					lblNewLabel.setText("Ediciones");
					/////////////////////////////////////////////////////
					String[] listaediciones=iconIP.listarEdicionesdeCurso((String)combocursos.getSelectedItem());
					for(String s : listaediciones ) System.out.println(s);
						if(listaediciones.length!=0) {
							combocurprog.setEnabled(true);
							combocurprog.removeAllItems();
							for(String i : listaediciones){
								//System.out.println(i);
								combocurprog.addItem(i);					
							}
						}else {
							combocurprog.removeAllItems();
							combocurprog.addItem("Sin Datos");	
						}
				
					///////////////////////////////////////////////////////////
				}else {
					lblNewLabel.setText("Programas");
					//////////////////////////////////////////////
					String[] listaprogramas=iconIP.listarProgramasCurso((String)combocursos.getSelectedItem());
					//String[] listaprogramas=iconIP.listarProgramasCurso("*");
					for(String s : listaprogramas ) System.out.println(s);
					if(listaprogramas.length!=0) {
						combocurprog.setEnabled(true);
						combocurprog.removeAllItems();
						for(String i : listaprogramas){
							//System.out.println(i);
							combocurprog.addItem(i);					
						}
					}else {
						combocurprog.removeAllItems();
						combocurprog.addItem("Sin Datos");	
					}
					/////////////////////////////////////////////
				}
			}
		});
		btnListPoE.setBounds(134, 172, 85, 21);
		getContentPane().add(btnListPoE);
		
		JButton btnNewButton = new JButton("Ver Datos de Curso ->");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("Datos de Curso\n");
				DtCurso dt = iconIP.mostrarDatosdeCurso((String)combocursos.getSelectedItem());
				textArea.append("Nombre: " +dt.getNombre()+ "\n");
				textArea.append("Descripcion: " + dt.getDescripcion() + "\n");
				textArea.append("Duracion: " + dt.getDuracion() + "\n");
				textArea.append("Cantidad de Horas: " + String.valueOf(dt.getCantidadHoras())+ "\n");
				textArea.append("Creditos: " + String.valueOf(dt.getCantidadCreditos())+ "\n");
				textArea.append("Url: " + dt.getUrl()+ "\n");
				Date fechaAlta = dt.getFechaAlta();
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd"); //Quitar horas y segundos
				//dateFormat.format(date);
				textArea.append("Fecha Alta: " + dateFormat.format(fechaAlta) + "\n");
				textArea.append("Foto " + dt.getFoto()+"\n");
				String[] dprevias=dt.getDtprevias();
				for(String s:dprevias) {
					textArea.append("Previa: " + s +"\n" );
				}

			}
		});
		btnNewButton.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton.setBounds(157, 104, 165, 21);
		getContentPane().add(btnNewButton);
		
		JButton btnMostEdProg = new JButton("Mostrar datos -->");
		btnMostEdProg.addActionListener(new ActionListener() {
			@SuppressWarnings("unused")
			public void actionPerformed(ActionEvent e) {
				if(rdbtnEdiciones.isSelected()) {
					/////////////////////////////////////////////////////
						DtEdicion dte = iconIP.mostrarEdicion((String) combocurprog.getSelectedItem());
						textArea.setText("Datos de Edicion\n");
						textArea.append("Nombre: " +dte.getNombre()+ "\n");
						DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd"); //Quitar horas y segundos
						Calendar fechaI = dte.getFechaI();
						textArea.append("Fecha Inicio: " + dateFormat.format(fechaI)+"\n");
						
						Calendar fechaF = dte.getFechaF();
						textArea.append("Fecha Fin: " + dateFormat.format(fechaF)+"\n");
						textArea.append("Cupo: " + String.valueOf(dte.getCupo())+ "\n");
						Calendar fechePub = dte.getFechaPub();
						textArea.append("Publicacion: " + dateFormat.format(fechaF) + "\n");
						listaDocente.clear();
						listaDocente = dte.getListaDocentes();
						for(Docente d:listaDocente) {
							textArea.append("Docente: " + d.getNombre()+ "\n");
						}
						

				
					///////////////////////////////////////////////////////////
				}else {
					lblNewLabel.setText("Programas");
					DtPrograma dtp = iconIP.mostrarPrograma((String) combocurprog.getSelectedItem());
					//DtPrograma dtp = new DtPrograma("ProgEjemplo", "Descripp", new Date(), new Date());
					textArea.setText("Datos de Programa\n");
					textArea.append("Nombre: " +dtp.getNombre()+ "\n");
					textArea.append("Descripcion: " +dtp.getDescripcion()+ "\n");
					DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd"); //Quitar horas y segundos
					Calendar fechaI = dtp.getFechaI();
					textArea.append("Fecha Inicio: " + dateFormat.format(fechaI)+"\n");
					Calendar fechaF = dtp.getFechaF();
					textArea.append("Fecha Fin: " + dateFormat.format(fechaF)+"\n");
					textArea.append("\nTodavia no se pueden \n");
					textArea.append("agregar Ediciones asi que \n");
					textArea.append("listo to2 los Programas \n");


				}
			}
		});
		btnMostEdProg.setHorizontalAlignment(SwingConstants.LEFT);
		btnMostEdProg.setBounds(169, 257, 134, 21);
		getContentPane().add(btnMostEdProg);
		this.setTitle("Consulta de Curso");
		this.setClosable(true);
		
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				textArea.setText("");
				listaDocente.clear();
				combocurprog.removeAllItems();
				combocursos.removeAllItems();
				comboInstitutos.removeAllItems();
				
			}
		});

		
	}

}

